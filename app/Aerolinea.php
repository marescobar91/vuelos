<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aerolinea extends Model
{
    protected $table="aerolineas"; 

    protected $fillable = ['cod_aerolinea','pais_id','nombre_aerolinea','nombre_corto','nombre_responsable','apellido_responsable', 'pagina_web','facebook','twitter','correo','fecha_fundacion' ];

    public $timestamps=true;

	public function aviones(){
    	return $this->hasMany('App\Avion');
			}

	public function pais(){

				return $this->belongsTo('App\Pais');
			}

			
	public function vuelos(){
		return $this->hasMany('App\Vuelo');
	}

}

