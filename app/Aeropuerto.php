<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aeropuerto extends Model
{
    //
    protected $table = "aeropuertos";
    protected $fillable = ['nombre_aeropuerto', 'cod_aeropuerto', 'ciudad_id', 'telefono', 'nombre_responsable', 'apellido_responsable', 'numero_gateway', 'codigoarea_id'];

    public $timestamps = true;

    public function gateways()
    {
        return $this->hasMany('App\Gateway');
    }

    public function ciudad(){
    	return $this->belongsTo('App\Ciudad');
    }


    public function codigo(){
    	return $this->belongsTo('App\CodigoArea');
    }
}
