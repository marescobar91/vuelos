<?php

namespace App;
use Illuminate\Database\Query\Builder\withPivot;  
use Illuminate\Database\Eloquent\Model;

class Avion extends Model
{
     protected $table="aviones";

    protected $fillable = [ 'modelo_id','aerolinea_id','tipo_avion'];

    public $timestamps=true; 


    public function modelo(){ 
        return $this->belongsTo('App\Modelo', 'modelo_id');
    }

   /* public function clase(){ 
        return $this->belongsTo('App\Clase');
    }*/ 

    public function aerolinea(){ 
        return $this->belonsTo('App\Aerolinea');
    }

      public function clase(){
        //return $this->belongsToMany('App\Clase','avion_clase','avion_id','clase_id', 'numero_asiento');

      return $this->belongsToMany('App\Clase')->withPivot('id','fila', 'columna');
    }

    public function avionVuelo(){

      return $this->hasMany('App\VueloAvion');
    }
 
}
