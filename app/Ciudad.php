<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ciudad extends Model
{
    protected $table="ciudades";

    protected $fillable=['pais_id','nombre_ciudad'];
    public $timestamps=true;



    public function pais(){
    	return $this->belongsTo('App\Pais');
    }

    public function aeropuertos(){

		return $this->hasMany('App\Aeropuerto');
	}
	public function vueloOrigen(){
		return $this->hasOne('App\Vuelo', 'origen_id', 'id');
	}
	public function vueloDestino(){
		return $this->hasOne('App\Vuelo', 'destino_id', 'id');
	}

}