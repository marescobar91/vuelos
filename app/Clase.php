<?php

namespace App;
use App\Avion;
use Illuminate\Database\Query\Builder\withPivot;


use Illuminate\Database\Eloquent\Model;

class Clase extends Model
{
     protected $table="clases";

    protected $fillable = ['nombre_clase'];

    public $timestamps=true;


    public function aviones(){
        return $this->belongsToMany('App\Avion')->withPivot('id', 'fila', 'columna'); 
    }

    public function vuelos(){
 
     	return $this->belongsToMany('App\Vuelo', 'tarifas', 'clase_id', 'vuelo_id')->withPivot('id','precio', 'promocion');
    }
 


} 
