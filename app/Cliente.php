<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    //

     protected $table="clientes";

     protected $fillable = [ 'municipio_id','user_id','calle', 'pasaje', 'colonia', 'telefono_movil', 'telefono_fijo', 'num_viajeroFrec', 'milla_asignada' ];

    public $timestamps=true; 

    public function municipio(){ 
        return $this->belongsTo('App\Municipio');
    }

    public function user(){
    	return $this->belongsTo('App\User');
    }

    public function empresa(){
    	return $this->hasOne('App\Empresa'); 
    }

    public function natural(){
    	return $this->hasOne('App\Natural');
    }

    public function itinerario(){
        return $this->hasOne('App\Itinerario');
    }


     public function itinerarioCliente(){
        return $this->hasMany('App\IntinerarioCliente');
    }




}
