<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CodigoArea extends Model
{
    protected $table="codigosareas"; 

    protected $fillable =['pais_id','codigo'];

    public $timestamps=true;

    public function pais(){
    	return $this->belongsTo('App\Pais');
	}

	public function aeropuertos(){

		return $this->hasMany('App\Aeropuerto', 'codigoarea_id');
	}

	
}
