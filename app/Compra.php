<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Compra extends Model
{
    //   protected $table="ciudades";
	protected $table="compra";
    protected $fillable=['vuelo','itinerario', 'fecha_salida', 'fecha_llegada', 'tiempo_vuelo', 'nombre_corto', 'costo' ];
}
