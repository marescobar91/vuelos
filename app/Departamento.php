<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Departamento extends Model
{
    protected $table="departamentos"; 

    protected $fillable = ['pais_id','nombre_departamento'];

    public $timestamps=true;


	/**
     * pais
     */
    public function pais(){
    	return $this->belongsTo('App\Pais');
        }


    public function municipios(){
    	return $this->hasMany('App\Municipio');
    }





}