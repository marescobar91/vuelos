<?php

namespace App; 

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    protected $table="empresa";

     protected $fillable = [ 'cliente_id','nic','nit', 'nombre_empresa'];

    public $timestamps=true; 


    public function cliente(){
    	return $this->belongsTo('App\Cliente');
    }
}
