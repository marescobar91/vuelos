<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gateway extends Model
{
    
     protected $table ="gateways";
     protected $fillable =['aeropuerto_id','nombre_gateway'];
     public $timestamps=true;

public function aeropuerto(){ 
        return $this->belongsTo('App\Aeropuerto');

    }
    public function vuelos(){
		return $this->hasMany('App\Vuelo');
	}
}
