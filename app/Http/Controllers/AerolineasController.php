<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pais;
use App\Aerolinea;
use Laracasts\Flash\Flash;
class AerolineasController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('permission:aerolineas.create')->only(['create', 'store']);
        $this->middleware('permission:aerolineas.index')->only('index');
        $this->middleware('permission:aerolineas.edit')->only(['edit', 'update']);
        $this->middleware('permission:aerolineas.destroy')->only('destroy'); 
     }


    public  function index(){

        $aerolineas = Aerolinea::all();
        return view('aerolinea.index', compact('aerolineas'));
    }

     public function create(){
        
        $pais = Pais::get();
      
        return view('aerolinea.create')->with('pais', $pais);
    }

        public function store(Request $request){

        $this->validate($request, [
            'nombre_aerolinea' => 'required|max:50|unique:aerolineas,nombre_aerolinea',
            'nombre_corto' => 'required|max:25|unique:aerolineas,nombre_corto',
            'nombre_responsable' => 'required|max:25',
            'apellido_responsable' => 'required|max:25'
        ]);

        $aerolineas = new Aerolinea();
        $aerolineas->nombre_aerolinea = $request->nombre_aerolinea;
        $aerolineas->nombre_corto = $request->nombre_corto;
        $nom_aerolinea = $request->nombre_corto;

        $initials = substr($nom_aerolinea,0,4);
        //dd($initials);

        $aerolineas->cod_aerolinea = $initials;

        $aerolineas->nombre_responsable = $request->nombre_responsable;
        $aerolineas->apellido_responsable = $request->apellido_responsable;
        $aerolineas->pagina_web = $request->sitio_web;
        $aerolineas->facebook = $request->facebook;
        $aerolineas->twitter  = $request->twitter;
        $aerolineas->correo   = $request->correo_electronico;
        $aerolineas->fecha_fundacion =  $request->fecha_fundacion;
        $aerolineas->pais_id  = $request->pais;
        $aerolineas->save();

        flash("¡Se ha registrado la aerolinea " . $aerolineas->nombre_aerolinea . " de forma exitosa!")->success()->important();

        return redirect()->action('AerolineasController@index');

        }

        public function edit($id){

          $aerolinea = Aerolinea::find($id);
          $pais = Pais::get();

          return view('aerolinea.edit', compact('aerolinea', 'pais'));

        }

        public function update(Request $request, $id){
            $this->validate($request, [
            'nombre_aerolinea' => 'required|max:50|unique:aerolineas,nombre_aerolinea,'.$id,
            'nombre_corto' => 'required|max:25|unique:aerolineas,nombre_corto,'.$id,
            'nombre_responsable' => 'required|max:25',
            'apellido_responsable' => 'required|max:25'
        ]);

        $aerolineas = Aerolinea::find($id);
        $aerolineas->nombre_aerolinea = $request->nombre_aerolinea;
        $aerolineas->nombre_corto = $request->nombre_corto;
        $nom_aerolinea = $request->nombre_corto;

        $initials = substr($nom_aerolinea,0,4);
        //dd($initials);

        $aerolineas->cod_aerolinea = $initials;



        $aerolineas->nombre_responsable = $request->nombre_responsable;
        $aerolineas->apellido_responsable = $request->apellido_responsable;
        $aerolineas->pagina_web = $request->sitio_web;
        $aerolineas->facebook = $request->facebook;
        $aerolineas->twitter  = $request->twitter;
        $aerolineas->correo   = $request->correo_electronico;
        $aerolineas->fecha_fundacion =  $request->fecha_fundacion;
        $aerolineas->pais_id  = $request->pais;
        $aerolineas->update();

        flash("¡Se ha editado la aerolinea " . $aerolineas->nombre_aerolinea . " de forma exitosa!")->warning()->important();

        return redirect()->action('AerolineasController@index');

        }

        public function destroy($id){

          $aerolineas= Aerolinea::find($id);
          $aerolineas->delete();

          flash("¡Se ha eliminado la aerolinea " .$aerolineas->nombre_aerolinea. " de forma exitosa!")->error()->important();

          return redirect()->action('AerolineasController@index');
        }


}
