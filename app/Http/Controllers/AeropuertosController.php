<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ciudad;
use App\CodigoArea;
use App\Gateway;
use App\Aeropuerto;
use Laracasts\Flash\Flash;
use Illuminate\Support\Facades\DB;

class AeropuertosController extends Controller
{
    //

    function __construct()
    {
        $this->middleware('permission:aeropuertos.create')->only(['create', 'store']);
        $this->middleware('permission:aeropuertos.index')->only('index');
        $this->middleware('permission:aeropuertos.edit')->only(['edit', 'update']);
        $this->middleware('permission:aeropuertos.destroy')->only('destroy');
    }
    

    public function index(){

        $aeropuerto = Aeropuerto::all();
        $gateways = Gateway::all();
//            $aeropuerto = Aeropuerto::order_by('id','ASC')->get();
        return view('aeropuerto.index', compact('aeropuerto', 'gateways'));
    }

    public function create()
    {

        $ciudades = Ciudad::get();
        $codigos = CodigoArea::get();
        return view('aeropuerto.create',compact('codigos', 'ciudades'));
    }

    public function store(Request $request)
    {
         $this->validate($request, [
        'nombre_aeropuerto'      => 'required|max:50|unique:aeropuertos,nombre_aeropuerto',
        'ciudad'                 => 'required',
        'codigo_area'            => 'required',
        'telefono'               => 'required',
        'nombre_responsable'     => 'required|max:25',
        'apellido_responsable'   => 'required|max:25',
        'gateway'                => 'required'
        ]);

        $aeropuerto = new Aeropuerto();
        $aeropuerto->nombre_aeropuerto = $request->nombre_aeropuerto;
        $nom_aeropuerto = $request->nombre_aeropuerto;
        $aeropuerto->ciudad_id = $request->ciudad;
        $aeropuerto->codigoarea_id = $request->codigo_area;
        $aeropuerto->telefono = $request->telefono;
        $aeropuerto->nombre_responsable = $request->nombre_responsable;
        $aeropuerto->apellido_responsable = $request->apellido_responsable;
        $aeropuerto->numero_gateway = $request->gateway;
        $gate = $request->gateway;

        $words = explode(" ", $nom_aeropuerto);
        $initials = null;
        foreach ($words as $w) {
        $initials .= $w[0];
            }
        $aeropuerto->cod_aeropuerto = $initials;
        $aeropuerto->save();

        $ultimo_aeropuerto = DB::table('aeropuertos')->orderby('id', 'desc')->first();

        for ($i=1; $i<=$gate; $i++){

            $gateway = new Gateway();

            $gateway->aeropuerto_id = $ultimo_aeropuerto->id;
            $gateway->nombre_gateway = $initials."0".$i;
            $gateway->save();
        }

        flash("¡Se ha registrado el aeropuerto y gateways " .$aeropuerto->nombre_aeropuerto. " de forma exitosa!")->success()->important();

        return redirect()->action('AeropuertosController@index');

    }


    public function edit($id){

        $ciudades = Ciudad::all();
        $codigos = CodigoArea::all();
        $aeropuerto = Aeropuerto::find($id);
        return view('aeropuerto.edit', compact('aeropuerto', 'ciudades', 'codigos'));
    }

    public function update(Request $request, $id){
         $this->validate($request, [
       'nombre_aeropuerto'      => 'required|max:50|unique:aeropuertos,nombre_aeropuerto,' .$id,
        'ciudad'                 => 'required',
        'codigo_area'            => 'required',
        'telefono'               => 'required',
        'nombre_responsable'     => 'required|max:25',
        'apellido_responsable'   => 'required|max:25',
        'gateway'                => 'required'
        ]);

        $aeropuerto = Aeropuerto::find($id);
        $aeropuerto->nombre_aeropuerto = $request->nombre_aeropuerto;
        $nom_aeropuerto = $request->nombre_aeropuerto;
        $aeropuerto->ciudad_id = $request->ciudad;
        $aeropuerto->codigoarea_id = $request->codigo_area;
        $aeropuerto->telefono = $request->telefono;
        $aeropuerto->nombre_responsable = $request->nombre_responsable;
        $aeropuerto->apellido_responsable = $request->apellido_responsable;
        $aeropuerto->numero_gateway = $request->gateway;
        $gate = $request->gateway;

        $words = explode(" ", $nom_aeropuerto);
        $initials = null;
        foreach ($words as $w) {
        $initials .= $w[0];
            }
        $aeropuerto->cod_aeropuerto = $initials;
        $aeropuerto->update();

        DB::table('gateways')->where('aeropuerto_id', '=', $id)->delete();

        for ($i=1; $i<=$gate; $i++){

            $gateway = new Gateway();

            $gateway->aeropuerto_id = $id;
            $gateway->nombre_gateway = $initials."0".$i;
            $gateway->save();
        }

        flash("¡Se ha editado el aeropuerto y gateways " .$aeropuerto->nombre_aeropuerto. " de forma exitosa!")->warning()->important();

        return redirect()->action('AeropuertosController@index');

    }

    public function destroy($id){

        $aeropuerto = Aeropuerto::find($id);
        $aeropuerto->delete();

        DB::table('gateways')->where('aeropuerto_id', '=', $id)->delete();

        flash("¡Se ha eliminado el aeropuerto y gateways " .$aeropuerto->nombre_aeropuerto. " de forma exitosa!")->error()->important();

        return redirect()->action('AeropuertosController@index');
    }



}