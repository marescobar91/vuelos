<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use App\Avion;
use App\Modelo;
use App\Marca;
use App\Aerolinea; 
use App\Clase;

class AvionesController extends Controller
{
     public function __construct()
    {
        $this->middleware('permission:aviones.create')->only(['create', 'store']);
        $this->middleware('permission:aviones.index')->only('index');
        $this->middleware('permission:aviones.edit')->only(['edit', 'update']);
        $this->middleware('permission:aviones.destroy')->only('destroy'); 
     }


      public function index(Request $request){

        $aviones = Avion::all();
        $modelo = Modelo::all();
        $modelo=$request->modelo_id;

      return view('avion.index')
      ->with('aviones', $aviones)
      ->with('modelo', $modelo);
         
    }

   /* public function getModel(Request $request, $id){
      $model = Modelo::where('marca_id', $id)->pluc('nombre_modelo', 'id');
      return json_encode($model);
    }*/


     public function create(Request $request){
        //$marcas = Marca::all();
        $modelo = Modelo::all();
        $aerolinea = Aerolinea::all();
        $clases = Clase::all();
       return view('avion.create', compact('modelo', 'aerolinea', 'clases')); 
    }

   

  

    public function store(Request $request){
       $this->validate($request, [
        'modelos'      => 'required',
        'aerolinea'    => 'required',
        'clases'       => 'required',
        'tipo_avion'   => 'required|max:25|unique:aviones,tipo_avion'
        ]);
    // $marcas = Marca::find($request->marcas);
      // $modelo = Modelo::find($request->get('modelo_id'));
        $modelo = Modelo::find($request->modelos); 
        $aerolinea = Aerolinea::find($request->aerolinea);
        $clases = Clase::find($request->clases);


      $aviones = new Avion();
      $aviones->tipo_avion = $request->tipo_avion; 
      $aviones->modelo_id =$modelo->id;
      $aviones->aerolinea_id=$aerolinea->id;

      $fila= $request->get('fila');
      $columna = $request->get('columna');

     
      //dd($num);

      $aviones->save();

      foreach ($request->get('clases') as $key => $id) { 
      $aviones->clase()->attach([$id], ['fila' => array_get($fila, $key), 'columna'=>array_get($columna, $key)]); 
    
      $aviones->save();
      
      }
      flash("Se ha registrado ". $aviones->tipo_avion." con exito")->success()->important();
      return redirect()->action('AvionesController@index');
    }

    public function edit($id)
    {
      
      $modelos = Modelo::all();
      $aerolinea = Aerolinea::all();
      $clases = Clase::all();
      $avion = Avion::find($id);
      $class = $avion->clase->pluck('id')->ToArray();

      return view('avion.edit', compact('modelos', 'aerolinea', 'clases', 'avion', 'class'));
       

   
    }

    public function update(Request $request, $id){

        $this->validate($request, [
        'modelos'      => 'required' ,
        'aerolinea'    => 'required',
        'clases'       => 'required',
        'tipo_avion'   => 'required|max:25|unique:aviones,tipo_avion,' .$id
        ]);
      $aviones = Avion::find($id);
      $aviones->fill($request->all());
      $aviones->save();
      $aviones->clase()->sync($request->clase);
      $fila= $request->get('fila');
      $columna = $request->get('columna');
      foreach ($request->get('clases') as $key => $id) { 
     $aviones->clase()->attach([$id], ['fila' => array_get($fila, $key), 'columna'=>array_get($columna, $key)]); 
      $aviones->save();
      
      }

      flash("Se ha editado ".$aviones->tipo_avion." con exito")->warning()->important();
      return redirect()->action('AvionesController@index');
    }

   /* public function destroy($id){
      $avion = Avion::find($id);
      $avion->delete();
      //dd($avion);
      flash('El avion se ha eliminado correctamente')->error()->important();
      return redirect()->route('aviones.index');

    }*/


public function destroy($id){

  //dd($id);
   $avion = Avion::find($id);
    $avion->delete($id);
    flash("¡Se ha eliminado el ".$avion->tipo_avion." de forma exitosa!")->error()->important();
    return redirect()->route('aviones.index');

    }

}
