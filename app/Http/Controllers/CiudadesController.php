<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pais;
use App\Ciudad;
use Laracasts\Flash\Flash;
use Illuminate\Support\Facades\DB; 

class CiudadesController extends Controller
{
    //

    function __construct()
    {
        $this->middleware('permission:ciudades.create')->only(['create', 'store']);
        $this->middleware('permission:ciudades.index')->only('index');
        $this->middleware('permission:ciudades.edit')->only(['edit', 'update']);
        $this->middleware('permission:ciudades.destroy')->only('destroy');
    }


     public function index()
    {
      
      $ciudad = Ciudad::all();
      
      return view('geografia.ciudad.index')->with('ciudad', $ciudad);
      
    }
     public function  create()
      {
      
    
      $paises = Pais::all();
      return view('geografia.ciudad.create', compact('paises'));
      }

     public function store(Request $request){
       $this->validate($request, [
            'pais'   => 'required', 
            'ciudad'   => 'required'
        ]);
      
      $ciudades = new Ciudad();
      $ciudades->pais_id = $request->pais;
      $ciudades->nombre_ciudad = $request->ciudad;
    
      $ciudades->save();

      flash("¡Se ha registrado la ciudad " .$ciudades->nombre_ciudad. " de forma existosa!")->success()->important();
      return redirect()->action('CiudadesController@index');
    }


    public function edit($id){

      $ciudad = Ciudad::find($id);
      $paises = Pais::all();
      return view('geografia.ciudad.edit', compact('ciudad', 'paises'));


    }

    public function update(Request $request, $id){
        $this->validate($request, [
            'pais'        => 'required', 
            'ciudad'   => 'required|max:25'
        ]);
       $ciudad = Ciudad::find($id);
       $ciudad->pais_id = $request->pais;
       $ciudad->nombre_ciudad = $request->ciudad;
       $ciudad->update();

       flash("¡Se ha editado la ciudad " .$ciudad->nombre_ciudad. " de forma existosa!")->warning()->important();

       return redirect()->action('CiudadesController@index');

    }

    public function destroy($id){
      $ciudad = Ciudad::find($id);
      $ciudad->delete();

      flash("¡Se ha eliminado la ciudad " .$ciudad->nombre_ciudad. " de forma existosa!")->error()->important();
      return redirect()->action('CiudadesController@index');

    }

   

}
