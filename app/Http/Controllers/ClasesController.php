<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Clase;
use Illuminate\Support\Facades\DB;
use Laracasts\Flash\Flash;
//use Illuminate\Support\Facades\Redirect;

class ClasesController extends Controller
{
     public function __construct()
    {
        $this->middleware('permission:clases.create')->only(['create', 'store']);
        $this->middleware('permission:clases.index')->only('index');
        $this->middleware('permission:clases.edit')->only(['edit', 'update']);
        $this->middleware('permission:clases.destroy')->only('destroy');   
     }
 

      public function index(Request $request){
        $clases = Clase::orderBy('id', 'ASC')->get();
       return view('avion.clase.index')->with('clases', $clases); 
      }

     public function create(Request $request){
     return view('avion.clase.create'); 
    }

     public function store(Request $request){
         $this->validate($request, [
            'nombre_clase'   => 'required|max:25|unique:clases,nombre_clase'
        ]);  
      $limite  = DB::table('clases')->pluck('clases');
     // dd($limite);

          if ($limite->count() >=4){
              flash("¡ERROR! ¡No puede ingresar mas de 4 clases")->error()->important();
                    
                   return redirect()->action('ClasesController@index');

          }else {
              $clases = new Clase();
              $clases->nombre_clase = $request->nombre_clase;
              $clases->save();
              flash("¡Se ha registrado ". $clases->nombre_clase . " de forma exitosa")->success()->important();
              return redirect()->action('ClasesController@index');
          }//else

     
    }//funcion store


     public function edit($id) {
     $clase =Clase::find($id); 
    return view('avion.clase.edit_clase')->with('clase', $clase);
    }

    public function update(Request $request, $id){ 
        $this->validate($request, [
            'nombre_clase'   => 'required|max:25|unique:clases,nombre_clase,' .$id
        ]);  
        $clase= Clase::find($id);
        $clase->fill($request->all());
        $clase->save();
        flash("¡Se ha editado ". $clase->nombre_clase . " de forma exitosa")->warning()->important();

        return redirect()->action('ClasesController@index');
        
    }

    public function destroy($id){ 
        $clase = Clase::find($id);
        $clase->delete($id);
        flash("¡Se ha eliminado ". $clase->nombre_clase . " de forma exitosa")->error()->important();
        return redirect()->action('ClasesController@index');

    }
}
