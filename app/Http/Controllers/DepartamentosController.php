<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Departamento;
use App\Pais;
use Laracasts\Flash\Flash;

class DepartamentosController extends Controller
{
    //

      public function __construct()
    {
        $this->middleware('permission:departamentos.create')->only(['create', 'store']);
        $this->middleware('permission:departamentos.index')->only('index');
        $this->middleware('permission:departamentos.edit')->only(['edit', 'update']);
        $this->middleware('permission:departamentos.destroy')->only('destroy'); 
      }



     public function  index()
    {
      
      $departamentos = Departamento::get();

      return view('geografia.departamento.index', compact('departamentos'));
          }
     public function  create()
    {
      
          $paises = Pais::get();
          return view('geografia.departamento.create', compact('paises'));
          }


    public function store(Request $request){
       $this->validate($request, [
            'pais'           => 'required', 
            'departamento'   => 'required|max:25|unique:departamentos,nombre_departamento'
        ]);

      $departamentos = new Departamento();

      $departamentos->pais_id = $request->pais;
      $departamentos->nombre_departamento = $request->departamento;
      $departamentos->save();

      flash("¡Se ha registrado el departamento " . $departamentos->nombre_departamento . "de forma exitosa")->success()->important();

      return redirect()->action('DepartamentosController@index');
    }

    public function edit($id){

      $departamentos = Departamento::find($id);
      $paises = Pais::get();
      return view('geografia.departamento.edit', compact('departamentos', 'paises'));
    }

    public function update(Request $request, $id){

       $this->validate($request, [
            'pais'           => 'required', 
            'departamento'   => 'required|max:25|unique:departamentos,nombre_departamento,' .$id
        ]);

      $departamento = Departamento::find($id);
      $departamento->pais_id = $request->pais;
      $departamento->nombre_departamento = $request->departamento;
      $departamento->update();

      flash("¡Se ha editado el departamento " . $departamento->nombre_departamento . "de forma exitosa")->warning()->important();

      return redirect()->action('DepartamentosController@index');
    }

    public function destroy($id){

      $departamento = Departamento::find($id);
      $departamento->delete();
      flash("¡Se ha eliminado el departamento " . $departamentos->nombre_departamento . "de forma exitosa")->error()->important();
      return redirect()->action('DepartamentosController@index');
    }

    

}
