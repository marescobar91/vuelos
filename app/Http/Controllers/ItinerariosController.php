<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ciudad;
use App\Clase; 
use App\Vuelo;
use App\Aerolinea;
use App\Cliente;
use App\Empresa;
use App\VueloAvion;
use App\Natural;
use App\Municipio;
use App\Itinerario;
use App\ItinerarioCliente;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Laracasts\Flash\Flash;
use DateTime;
use \PDF;

class ItinerariosController extends Controller
{
   public function consulta(Request $request){
    $m="";

   	$ciudad = Ciudad::all();
   	$ciudadDes = Ciudad::all();
   	$clases = Clase::all();
    $aerolineas = Aerolinea::all();
     
   	return view('reservacion.view')
   	->with('ciudad', $ciudad)
   	->with('clases', $clases)
   	->with('ciudadDes', $ciudadDes)
    ->with('aerolineas', $aerolineas)
    ->with('m', $m);


   }

   public function busqueda(Request $request){

        $ciudad = Ciudad::all();
        $ciudadDes = Ciudad::all();
        $clases = Clase::all();
        $aerolineas = Aerolinea::all();
   	    Carbon::setLocale('es_SV');
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $date = $fecha->format('Y-m-d');
       // $anio = $fecha->format('Y');
        $ciudad = Ciudad::all();
        $origen = $request->origen;
        $destino = $request->destino;
        $fecha_salida = $request->input('fecha_salida');
        $fecha_llegada = $request->input('fecha_llegada');
        $clase = $request->clase;
        $ninos = $request->input('ninos');
        $adultos = $request->input('adultos');
        $ancianos = $request->input('ancianos');
        $suma = $ninos+ $adultos+ $ancianos;
        $vuelos = Vuelo::all();
        $f1 = explode(" ", $fecha_salida);
        $f2 = explode(" ", $fecha_llegada);
        $date=date_create($fecha_salida);
        $fecha1 = date_format($date,"Y-m-d H:i:s");
        $date1=date_create($fecha_llegada);
        $fecha2 = date_format($date1,"Y-m-d H:i:s");


        $date1 = Carbon::now();
         $date1 = $date1->format('Y-m-d');

    

 
 
    if($origen==null && $destino==null && $fecha_salida==null && $fecha_llegada==null){

            $m = "";
            return view('reservacion.view')
        ->with('ciudad', $ciudad)
        ->with('clases', $clases)
        ->with('ciudadDes', $ciudadDes)
        ->with('aerolineas', $aerolineas)
        ->with('m', $m);
      } 


        if ($suma > 5) {
          flash('Completo el maximo de acompañantes')->error()->important();
            return redirect()->action('ItinerariosController@consulta');
            
        }else{

    if ($origen!=null && $destino!=null && $fecha_salida!=null && $fecha_llegada!=null ) {

    
      $date1 = Carbon::now();

      $date1 = $date1->format('Y-m-d');


      if($fecha_salida < $date1 ){

            flash('¡ERROR! ¡La fecha de salida no puede ser menor a la fecha de actual!')->error()->important();
            return redirect()->action('ItinerariosController@consulta');
          }

      if($fecha_llegada < $date1 ){

      flash('¡ERROR! ¡La fecha de regreso no puede ser menor a la fecha de actual!')->error()->important();
      return redirect()->action('ItinerariosController@consulta');
    }

    if($fecha_salida > $fecha_llegada){
      flash('¡ERROR! ¡La fecha de salida no puede ser mayor a la fecha de regreso')->error()->important();
      return redirect()->action('ItinerariosController@consulta');}


      /**************VALIDANDO LAS FECHAS DE ELECCION*********************/
      $m = "Existe";

        	$vuelos1 = DB::table('vuelos as v')->leftJoin('aerolineas as a', 'v.aerolinea_id', '=', 'a.id')
        	->leftJoin('ciudades as origen',  'v.origen_id', '=', 'origen.id')->leftJoin('ciudades as destino', 'v.destino_id', '=','destino.id')->leftJoin('tarifas as ta', 'v.id', '=', 'ta.vuelo_id')->leftJoin('clases  as cla', 'cla.id', '=', 'ta.clase_id')
        	->select('v.id','v.tiempo_vuelo', 'v.fecha_salida', 'v.fecha_llegada', 'v.codigo_vuelo', 'v.costo', 'a.nombre_aerolinea', 'ta.precio', 'cla.nombre_clase')->where('cla.id', '=', $clase)->where('origen.id', '=', $origen)->where('destino.id', '=', $destino)->Orwhere('v.fecha_salida','=',$fecha1)->get();

          $vuelos2 = DB::table('vuelos as v')->leftJoin('aerolineas as a', 'v.aerolinea_id', '=', 'a.id')
          ->leftJoin('ciudades as origen',  'v.origen_id', '=', 'origen.id')->leftJoin('ciudades as destino', 'v.destino_id', '=','destino.id')->leftJoin('tarifas as ta', 'v.id', '=', 'ta.vuelo_id')->leftJoin('clases  as cla', 'cla.id', '=', 'ta.clase_id')
          ->select('v.id','v.tiempo_vuelo', 'v.fecha_salida', 'v.fecha_llegada', 'v.codigo_vuelo', 'v.costo', 'a.nombre_aerolinea', 'ta.precio', 'cla.nombre_clase')->where('cla.id', '=', $clase)->where('origen.id', '=', $destino)->where('destino.id', '=', $origen)->Orwhere('v.fecha_salida','=',$fecha2)->get();



          $novuelo1 = $vuelos1->isEmpty();
           $novuelo2 = $vuelos2->isEmpty();
          if($novuelo1 == true && $novuelo2 == true){
            flash('¡EL VUELO NO EXISTE! ¡Consulte de nuevo')->error()->important();
            return redirect()->action('ItinerariosController@consulta');
            
            }


           
         return view('reservacion.view')
          ->with('m', $m)
          ->with('suma', $suma)
          ->with('clase', $clase)
          ->with('vuelos1', $vuelos1)
          ->with('vuelos2', $vuelos2);
      }

      } 



   }

   public function guardar(Request $request){

    $var1= $request->ida;
    $var2 = $request->regreso;
    $clase = $request->clase;
    $sumar = $request->suma;

    $municipio = Municipio::all();
    //dd($var1, $var2, $sumar, $clase);
    return view('reservacion.cliente')->with('var1', $var1)->with('municipio', $municipio)->with('var2', $var2)->with('sumar', $sumar)->with('clase', $clase);
   }

   public function cliente(Request $request){
    //captura el id de vuelo de ida
    $var1 = $request->vuelo;
    //captura el id de vuelo de regreso
   $var2 = $request->vuelo2;
   //captura el id de la clase
    //dd($var2);
    $clase = $request->clase;
    $acompañantes = $request->sumar;

    $vuelos1 = Vuelo::where('id', '=', $var1)->get();
    $milla1 = 0;

      foreach($vuelos1 as $vuelo){
        $milla1 = $vuelo->millas_otorgar;
      }



      $vuelos2 = Vuelo::where('id', '=', $var2)->get();
        $milla2 = 0;

        foreach($vuelos2 as $vuelo){
          $milla2 = $vuelo->millas_otorgar;
        }

    $municipio = Municipio::all();

    $cliente = new Cliente();
    $cliente->nombre_cliente=$request->nombre_cliente;
    $cliente->apellido_cliente=$request->apellido_cliente;
    $cliente->municipio_id=$request->municipio;  
    $cliente->calle=$request->calle;
    $cliente->pasaje=$request->pasaje;
    $cliente->colonia=$request->colonia;
    $cliente->telefono_movil=$request->telefono_movil;
    $cliente->telefono_fijo=$request->telefono_fijo;
    $cliente->num_viajeroFrec=$request->num_viejaroFrec;
    $cliente->milla_asignada=$milla1;
    $cliente->vuelo_id=$var1;
    $cliente->save();
    //Valido para que me guarde si es natural o empresa

     $fecha= $request->fecha_nacimiento;

      $date2 = Carbon::now();

      $date2 = $date2->format('Y-m-d');


      if($fecha > $date2 ){

            flash('¡ERROR! ¡La fecha de nacimiento no puede ser mayor a la actual!')->error()->important();
            return redirect()->action('ItinerariosController@cliente');
          }

    if ($fecha != null)
     {

    
          $natural = new Natural();
          $natural->cliente_id = $cliente->id;
          $natural->fecha_nacimiento=$request->fecha_nacimiento;
          $natural->genero=$request->genero;
          $natural->estado_civil=$request->estado_civil;
          $natural->tipo_documento=$request->tipo_documento;
          $natural->numero_documento=$request->numero_documento;
          $natural->save();
 
     }else{

          $empresa = new Empresa();
          $empresa->cliente_id = $cliente->id;
          $empresa->nit=$request->nit;
          $empresa->nic=$request->nic;
          $empresa->nombre_empresa=$request->nombre_empresa;
          $empresa->save();

}

/*************HACER DOS VECES******************/

    //precios
    $tarifa1 = DB::table('tarifas')->where('tarifas.vuelo_id', '=', $var1)->where('tarifas.clase_id', '=', $clase)->get();

      $precio1 = 0;
      $promocion1=0;
      foreach ($tarifa1 as $tarifa) {
        $precio1 = $tarifa->precio;
        $promocion1 = $tarifa->promocion;

      }

      if($acompañantes >1){
        $total = ($precio1 + $promocion1)*$acompañantes;
      }else{

        $total = $precio1+$promocion1;
      }

      $itinerario = new Itinerario();
      $itinerario->costo =$total;
      $itinerario->numero_maletas=$request->maleta;
      if ($acompañantes >1) {
        $itinerario->cantidad=$acompañantes;
      }else{
        $itinerario->cantidad=0;
      }
      $itinerario->pago=1;
      $itinerario->cliente_id=$cliente->id;
      $itinerario->save();


    if ($acompañantes ==2) {
      
      $persona1 = new ItinerarioCliente();
      $persona1->cliente_id=$cliente->id;
      $persona1->itinerario_id=$itinerario->id;
      $persona1->nombre=$request->nombre1;
      $persona1->apellido=$request->apellido1;
      $persona1->save();

    }

    if ($acompañantes==3) {
      $persona1 = new ItinerarioCliente();
      $persona1->cliente_id=$cliente->id;
      $persona1->itinerario_id=$itinerario->id;
      $persona1->nombre=$request->nombre1;
      $persona1->apellido=$request->apellido1;
      $persona1->save();

      $persona2 = new ItinerarioCliente();
      $persona2->cliente_id=$cliente->id;
      $persona2->itinerario_id=$itinerario->id;
      $persona2->nombre=$request->nombre2;
      $persona2->apellido=$request->apellido2;
      $persona2->save();
    }

    if ($acompañantes==4) {
      $persona1 = new ItinerarioCliente();
      $persona1->cliente_id=$cliente->id;
      $persona1->itinerario_id=$itinerario->id;
      $persona1->nombre=$request->nombre1;
      $persona1->apellido=$request->apellido1;
      $persona1->save();

      $persona2 = new ItinerarioCliente();
      $persona2->cliente_id=$cliente->id;
      $persona2->itinerario_id=$itinerario->id;
      $persona2->nombre=$request->nombre2;
      $persona2->apellido=$request->apellido2;
      $persona2->save();

      $persona3 = new ItinerarioCliente();
      $persona3->cliente_id=$cliente->id;
      $persona3->itinerario_id=$itinerario->id;
      $persona3->nombre=$request->nombre3;
      $persona3->apellido=$request->apellido3;
      $persona3->save();
    }

    if ($acompañantes==5) {
      $persona1 = new ItinerarioCliente();
      $persona1->cliente_id=$cliente->id;
      $persona1->itinerario_id=$itinerario->id;
      $persona1->nombre=$request->nombre1;
      $persona1->apellido=$request->apellido1;
      $persona1->save();

      $persona2 = new ItinerarioCliente();
      $persona2->cliente_id=$cliente->id;
      $persona2->itinerario_id=$itinerario->id;
      $persona2->nombre=$request->nombre2;
      $persona2->apellido=$request->apellido2;
      $persona2->save();

      $persona3 = new ItinerarioCliente();
      $persona3->cliente_id=$cliente->id;
      $persona3->itinerario_id=$itinerario->id;
      $persona3->nombre=$request->nombre3;
      $persona3->apellido=$request->apellido3;
      $persona3->save();

      $persona4 = new ItinerarioCliente();
      $persona4->cliente_id=$cliente->id;
      $persona4->itinerario_id=$itinerario->id;
      $persona4->nombre=$request->nombre4;
      $persona4->apellido=$request->apellido4;
      $persona4->save();
    }


  /*************HACER DOS VECES******************/

    $cliente1 = new Cliente();
    $cliente1->nombre_cliente=$request->nombre_cliente;
    $cliente1->apellido_cliente=$request->apellido_cliente;
    $cliente1->municipio_id=$request->municipio;  
    $cliente1->calle=$request->calle;
    $cliente1->pasaje=$request->pasaje;
    $cliente1->colonia=$request->colonia;
    $cliente1->telefono_movil=$request->telefono_movil;
    $cliente1->telefono_fijo=$request->telefono_fijo;
    $cliente1->num_viajeroFrec=$request->num_viejaroFrec;
    $cliente1->milla_asignada=$milla2;
    $cliente1->vuelo_id=$var2;
    $cliente1->save();
    //Valido para que me guarde si es natural o empresa

     $fecha= $request->fecha_nacimiento;
    if ($fecha != null)
     {
          $natural1 = new Natural();
          $natural1->cliente_id = $cliente1->id;
          $natural1->fecha_nacimiento=$request->fecha_nacimiento;
          $natural1->genero=$request->genero;
          $natural1->estado_civil=$request->estado_civil;
          $natural1->tipo_documento=$request->tipo_documento;
          $natural1->numero_documento=$request->numero_documento;
          $natural1->save();
 
     }else{

          $empresa1 = new Empresa();
          $empresa1->cliente_id = $cliente1->id;
          $empresa1->nit=$request->nit;
          $empresa1->nic=$request->nic;
          $empresa1->nombre_empresa=$request->nombre_empresa;
          $empresa1->save();

        }

    //precios
      $tarifa2 = DB::table('tarifas')->where('tarifas.vuelo_id', '=', $var2)->where('tarifas.clase_id', '=', $clase)->get();

        $precio2 = 0;
        $promocion2=0;
        foreach ($tarifa2 as $tarifa) {
          $precio2 = $tarifa->precio;
          $promocion2 = $tarifa->promocion;

        }

      if($acompañantes >1){
        $total1 = ($precio2 + $promocion2)*$acompañantes;
      }else{

        $total1 = $precio2+$promocion2;
      }

      $itinerario1 = new Itinerario();
      $itinerario1->costo =$total1;
      $itinerario1->numero_maletas=$request->maleta;
      if ($acompañantes >1) {
        $itinerario1->cantidad=$acompañantes;
      }else{
        $itinerario1->cantidad=0;
      }
      $itinerario1->pago=1;
      $itinerario1->cliente_id=$cliente1->id;
      $itinerario1->save();

      if ($acompañantes ==2) {
        
        $per1 = new ItinerarioCliente();
        $per1->cliente_id=$cliente1->id;
        $per1->itinerario_id=$itinerario1->id;
        $per1->nombre=$request->nombre1;
        $per1->apellido=$request->apellido1;
        $per1->save();

      }

      if ($acompañantes==3) {
        $per1 = new ItinerarioCliente();
        $per1->cliente_id=$cliente1->id;
        $per1->itinerario_id=$itinerario1->id;
        $per1->nombre=$request->nombre1;
        $per1->apellido=$request->apellido1;
        $per1->save();

        $per2 = new ItinerarioCliente();
        $per2->cliente_id=$cliente1->id;
        $per2->itinerario_id=$itinerario1->id;
        $per2->nombre=$request->nombre2;
        $per2->apellido=$request->apellido2;
        $per2->save();
      }

      if ($acompañantes==4) {
        $per1 = new ItinerarioCliente();
        $per1->cliente_id=$cliente1->id;
        $per1->itinerario_id=$itinerario1->id;
        $per1->nombre=$request->nombre1;
        $per1->apellido=$request->apellido1;
        $per1->save();

        $per2 = new ItinerarioCliente();
        $per2->cliente_id=$cliente1->id;
        $per2->itinerario_id=$itinerario1->id;
        $per2->nombre=$request->nombre2;
        $per2->apellido=$request->apellido2;
        $per2->save();

        $per3 = new ItinerarioCliente();
        $per3->cliente_id=$cliente1->id;
        $per3->itinerario_id=$itinerario1->id;
        $per3->nombre=$request->nombre3;
        $per3->apellido=$request->apellido3;
        $per3->save();
      }

      if ($acompañantes==5) {
        $per1 = new ItinerarioCliente();
        $per1->cliente_id=$cliente1->id;
        $per1->itinerario_id=$itinerario1->id;
        $per1->nombre=$request->nombre1;
        $per1->apellido=$request->apellido1;
        $per1->save();

        $per2 = new ItinerarioCliente();
        $per2->cliente_id=$cliente1->id;
        $per2->itinerario_id=$itinerario1->id;
        $per2->nombre=$request->nombre2;
        $per2->apellido=$request->apellido2;
        $per2->save();

        $per3 = new ItinerarioCliente();
        $per3->cliente_id=$cliente1->id;
        $per3->itinerario_id=$itinerario1->id;
        $per3->nombre=$request->nombre3;
        $per3->apellido=$request->apellido3;
        $per3->save();

        $per4 = new ItinerarioCliente();
        $per4->cliente_id=$cliente1->id;
        $per4->itinerario_id=$itinerario1->id;
        $per4->nombre=$request->nombre4;
        $per4->apellido=$request->apellido4;
        $per4->save();
      }


        $iti1 = $itinerario->id;
        $iti2 = $itinerario1->id;

$compra1 = DB::table('compra')->where('vuelo', '=', $var1)->where('itinerario', '=', $iti1)->get();
//dd($compra1);

$compra2 = DB::table('compra')->where('vuelo', '=', $var2)->where('itinerario', '=', $iti2)->get();
$compra3 = DB::table('compra')->where('vuelo', '=', $var2)->where('itinerario', '=', $iti2)->get();
$clases = Clase::where('id', '=', $clase)->get();
$cla = Clase::where('id', '=', $clase)->get();
$sum1 = DB::table('compra')->where('compra.vuelo', '=', $var1)->where('itinerario', '=', $iti1)->sum('compra.costo');
$sum2 = DB::table('compra')->where('compra.vuelo', '=', $var2)->where('itinerario', '=', $iti2)->sum('compra.costo');
$total = $sum1 + $sum2;
//dd($compra1, $compra2)
//dd($total);



    return view('reservacion.tarifas')
    ->with('iti1', $iti1)
    ->with('iti2', $iti2)
    ->with('compra1', $compra1)
    ->with('compra2', $compra2)
    ->with('compra3', $compra3)
    ->with('var1', $var1)
    ->with('var2', $var2)
    ->with('total', $total)
    ->with('clases', $clases)
    ->with('cla', $cla)
    ->with('clase', $clase)
    ->with('acompañantes', $acompañantes);

   }


   public function mostrar(Request $request){

    $acompañantes=$request->sumar;
    $var1=$request->var1;
    
    $var2=$request->var2;
    

    $iti1 = $request->iti1;
    $iti2 = $request->iti2;
    $clases = $request->clase;


    $asiento1 = DB::table('vuelo_avion')->where('vuelo_avion.vuelo_id', '=', $var1)->where('vuelo_avion.clase_id', '=', $clases)->get();
   $asiento2 = DB::table('vuelo_avion')->where('vuelo_avion.vuelo_id', '=', $var2)->where('vuelo_avion.clase_id', '=', $clases)->where('vuelo_avion.disponible', '=', FALSE)->get();
   $cla = DB::table('clases')->where('clases.id', '=', $clases)->get();




    return view('reservacion.asiento')->with('asiento1', $asiento1)->with('asiento2', $asiento2)->with('iti1', $iti1)->with('iti2', $iti2)->with('clases', $clases)->with('cla', $cla);
   }

   public function asiento(Request $request){

    $itine=$request->iti1;
    $itine1=$request->iti2;

    $asi=$request->ida;
    $asi1=$request->regreso;

    $itinerario = Itinerario::find($itine);
    $itinerario1 = Itinerario::find($itine1);

    $itinerario->vueloAvion()->attach($request->ida);
    $itinerario1->vueloAvion()->attach($request->regreso);
    $itinerario->save();
    $itinerario1->save();
  
   foreach ($request->get('ida') as $key => $id) {
    $asiento1 = VueloAvion::find($id);
    $asiento1->disponible = 1;
    $asiento1->update();
   }
   foreach ($request->get('regreso') as $key => $id) {
    $asiento2 = VueloAvion::find($id);
    $asiento2->disponible = 1;
    $asiento2->update();
   }

   $boleto1 = DB::table('boletos')->select('itinerario','codigo_vuelo', 'nombre_gateway', 'nombre_aeropuerto', 'origen_nombre', 'destino_nombre', 'nombre_corto', 'nombre_cliente', 'apellido_cliente', 'nombre_empresa', 'num_viajeroFrec', 'pago', 'fecha_llegada', 'fecha_salida')->where('itinerario', '=', $itine)->get();

    $boleto2 = DB::table('boletos')->select('itinerario','codigo_vuelo', 'nombre_gateway', 'nombre_aeropuerto', 'origen_nombre', 'destino_nombre', 'nombre_corto', 'nombre_cliente', 'apellido_cliente', 'nombre_empresa', 'num_viajeroFrec', 'pago', 'fecha_llegada', 'fecha_salida')->where('itinerario', '=', $itine1)->get();
    $persona1 = DB::table('itinerario_cliente')->where('itinerario_cliente.itinerario_id', '=', $itine)->get();
    $persona2 = DB::table('itinerario_cliente')->where('itinerario_cliente.itinerario_id', '=', $itine1)->get();

    $silla1 = DB::table('asientos')->where('itinerario', '=', $itine)->get();
    $silla2 = DB::table('asientos')->where('itinerario', '=', $itine1)->get();

    $pdf = \PDF::loadView('reservacion.boleto', compact('itine', 'itine1', 'boleto1', 'boleto2', 'persona1', 'persona2', 'silla1', 'silla2'))->setPaper('A4', 'landscape')->setWarnings(false);
        return $pdf->stream('reporte_intenciones.pdf');

    //return view('reservacion.boleto')->with('itine', $itine)->with('itine1', $itine1)->with('boleto1', $boleto1)->with('boleto2', $boleto2)->with('persona1', $persona1)->with('persona2', $persona2)->with('silla1', $silla1)->with('silla2', $silla2);
    
   }




}
