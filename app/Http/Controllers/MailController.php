<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use Session;
use Redirect;
use Laracasts\Flash\Flash;

class MailController extends Controller
{
    

    public function store(Request $request){

    Mail::send('emails.contact', $request->all(), function($msj){

    		$msj->subject('Correo de Contacto');
    		$msj->to('fivewoman10@gmail.com');

    });

    	Flash('¡Tu correo se ha enviado exitosamente!')->success()->important();
    	return redirect()->route('show.contacto');


    }
}
