<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Marca;
use App\Modelo;
use Laracasts\Flash\Flash;  

class MarcasController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:marcas.create')->only(['create', 'store']);
        $this->middleware('permission:marcas.index')->only('index');
        $this->middleware('permission:marcas.edit')->only(['edit', 'update']);
        $this->middleware('permission:marcas.destroy')->only('destroy'); 
     }


      public function index()
 
    {
        

      $marcas = Marca::orderBy('id', 'ASC')->get();
       return view('avion.marca.index')->with('marcas', $marcas); 
    }

     public function create(Request $request)
 
    {
        
       return view('avion.marca.create'); 
    }

    public function store(Request $request){
         $this->validate($request, [
            'marca'   => 'required|max:25|unique:marcas,nombre_marca'
        ]);

      $marcas = new Marca();
      $marcas->nombre_marca = $request->marca;
      $marcas->save();

      flash("¡Se ha registrado la marca " . $marcas->nombre_marca . " de forma exitosa")->success()->important();

      return redirect()->action('MarcasController@index');
    }


     public function edit($id)
    {
     $marca =Marca::find($id); 
    return view('avion.marca.edit_marca')->with('marca', $marca);
    }

    public function update(Request $request, $id){
          $this->validate($request, [
            'nombre_marca'   => 'required|max:25|unique:marcas,nombre_marca,' .$id
        ]);     
        $marca= Marca::find($id);
        $marca->fill($request->all());
        $marca->save();
        flash("¡Se ha editado la marca " . $marca->nombre_marca . " de forma exitosa")->warning()->important();

        return redirect()->action('MarcasController@index');
     }

     public function destroy($id){ 
        $marca = Marca::find($id);
        $marca->delete($id);
        flash("¡Se ha eliminado la marca " . $marca->nombre_marca . " de forma exitosa")->error()->important();
        return redirect()->action('MarcasController@index');
      }

    

     

}


