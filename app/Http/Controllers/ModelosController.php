<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modelo;
use App\Marca;
use Laracasts\Flash\Flash;

class ModelosController extends Controller
{
     public function __construct()
    {
        $this->middleware('permission:modelos.create')->only(['create', 'store']);
        $this->middleware('permission:modelos.index')->only('index');
        $this->middleware('permission:modelos.edit')->only(['edit', 'update']);
        $this->middleware('permission:modelos.destroy')->only('destroy');
     }
 

      public function index(Request $request){
      $modelos = Modelo::orderBy('id', 'ASC')->get();
       return view('avion.modelo.index')->with('modelos', $modelos); 
      }

     public function create(){
      $marcas = Marca::all();   

       return view('avion.modelo.create', compact('marcas')); 
    }


     public function store(Request $request){
       $this->validate($request, [
            'marcas'        => 'required', 
            'modelo'   => 'required|max:25|unique:modelos,nombre_modelo'
        ]);
      $marcas = Marca::find($request->marcas);
      $modelos = new Modelo();
      $modelos->nombre_modelo = $request->modelo;
      $modelos->marca_id = $marcas->id;
     // $modelos->marca_id = $marcas->id_marca;
     // $miembro_parroquial->etapa_id=$etapa->id
      $modelos->save();
      flash("¡Se ha registrado el modelo " . $modelos->nombre_modelo . " de forma exitosa!")->success()->important();
      return redirect()->action('ModelosController@index');
    }

     public function edit($id) {
       $modelo =Modelo::find($id); 
       $marca =  Marca::all();
       $modelo->marcas;
          return view('avion.modelo.edit_modelo')
        ->with('modelo', $modelo)
        ->with('marca', $marca);  
    } 

    public function update(Request $request, $id) { 
           //
        $this->validate($request, [
            'marcas'   => 'required', 
            'nombre'   => 'required|max:25|unique:modelos,nombre_modelo,'.$id
        ]);
      $modelo= Modelo::find($id);
      $modelo->fill($request->all());
      $modelo->save();
      flash("¡Se ha editado el modelo " . $modelo->nombre_modelo . " de forma exitosa!")->warning()->important();
        return redirect()->action('ModelosController@index');
    }

    public function destroy($id){ 
        $modelo = Modelo::find($id);
        $modelo->delete($id);
        flash("¡Se ha eliminado el modelo " . $modelo->nombre_modelo . " de forma exitosa!")->error()->important();
        return redirect()->action('ModelosController@index');

    }




    
} 
