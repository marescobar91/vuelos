<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pais;
use App\Departamento;
use App\Municipio;
use Laracasts\Flash\Flash;
use Illuminate\Support\Facades\DB; 

class MunicipiosController extends Controller
{
    //
     public function  index()
    {
      
      //$paises = Pais::all();
      $municipios = Municipio::get();
      return view('geografia.municipio.index', compact('municipios'));
      }
      
     public function  create()
    {
      
      
      $departamentos = Departamento::all();
      //dd($paises);
      return view('geografia.municipio.create', compact('departamentos'));
          }


  public function store(Request $request){
     $this->validate($request, [
            'departamento'   => 'required', 
            'municipio'   => 'required|max:25'
        ]);

    $municipios= new Municipio();
    $municipios->departamento_id = $request->departamento;
    //$muni= $request->departamento;
    //dd($muni);
    $municipios->nombre_municipio = $request->municipio;
    $municipios->save();

    flash("¡Se ha registrado el municipio " . $municipios->nombre_municipio . " de forma exitosa!")->success()->important();

    return redirect()->action('MunicipiosController@index');

  }

  public function edit($id){

    $departamento= Departamento::all();
    $municipios=Municipio::find($id);
    return view('geografia.municipio.edit', compact('municipios','departamento'));

  }

  public function update(Request $request,$id){

     $this->validate($request, [
            'departamento'   => 'required', 
            'municipio'   => 'required|max:25'
        ]);
    $municipios=Municipio::find($id);
    $municipios->departamento_id = $request->departamento;
    //$muni= $request->departamento;
    //dd($muni);
    $municipios->nombre_municipio = $request->municipio;
    $municipios->update();
    flash("¡Se ha editado el municipio " . $municipios->nombre_municipio . " de forma exitosa!")->warning()->important();
     return redirect()->action('MunicipiosController@index');
  }


  public function destroy($id){
    $municipios=Municipio::find($id);
    $municipios->delete();
    flash("¡Se ha eliminado el municipio " . $municipios->nombre_municipio . " de forma exitosa!")->error()->important();
    return redirect()->action('MunicipiosController@index');
  }
  

}
