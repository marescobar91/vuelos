<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pais;
use Laracasts\Flash\Flash;
class PaisesController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:paises.create')->only(['create', 'store']);
        $this->middleware('permission:paises.index')->only('index');
        $this->middleware('permission:paises.edit')->only(['edit', 'update']);
        $this->middleware('permission:paises.destroy')->only('destroy');
      }

      public function  index()
    {
        $paises = Pais::get(); 
      return view('geografia.pais.index', compact('paises'));
      }
      public function  create()
    {
      

      return view('geografia.pais.create');
        
    }


    public function store(Request $request){
      $this->validate($request, [
            'pais' => 'required|max:25|unique:paises,nombre'
        ]);

    $paises = new Pais();
    $paises->nombre = $request->pais;
    $nombre_pais = $request->pais;

    $words = strtoupper(substr($nombre_pais,0,3));
    $paises->cod_pais = trim($words);
    $paises->save();
    flash("¡Se ha registrado el país " . $paises->nombre . " de forma exitosa!")->success()->important();

    return redirect()->action('PaisesController@index');
    }

    public function edit($id){

      $pais = Pais::find($id);

      return view('geografia.pais.edit')->with('pais', $pais);
    }

    public function update(Request $request, $id){
      $this->validate($request, [
            'codigo_pais'   => 'required|max:3|unique:paises,cod_pais,' .$id , 
            'pais'          => 'required|max:25|unique:paises,nombre,' .$id
        ]);


      $pais = Pais::find($id);
      $pais->cod_pais = $request->codigo_pais;
      $pais->nombre = $request->pais;
      $pais->update();
      Flash("¡Se ha editado el país " . $pais->nombre . " de forma exitosa!")->warning()->important();

      return redirect()->action('PaisesController@index');
    }


    public function destroy($id){

      $pais= Pais::find($id);
      $pais->delete();
      Flash("¡Se ha eliminado el país " . $pais->nombre . " de forma exitosa!")->error()->important();
      return redirect()->action('PaisesController@index');
    }
}
