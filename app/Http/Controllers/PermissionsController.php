<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use Caffeinated\Shinobi\Models\Permission;

class PermissionsController extends Controller
{
    
    public function __construct(){

        $this->middleware('permission:permisos.create')->only(['create', 'store']);
        $this->middleware('permission:permisos.index')->only('index');
        $this->middleware('permission:permisos.edit')->only(['edit', 'update']);
        $this->middleware('permission:permisos.destroy')->only('destroy');
        
    }



    public function index(){

    	$permisos = Permission::orderBy('id', 'DESC')->get();
    	return view('user.privilegio.index')->with('permisos', $permisos);

    }


   public function create(Request $request){

    	return view('user.privilegio.create');
    }

    public function store(Request $request){

        $this->validate($request, [
            'nombre' => 'required|max:255|unique:permissions,name',
            'url' => 'required|max:255|unique:permissions,slug',
            'descripcion' => 'required'
        ]);

    	$permisos = new Permission();
    	$permisos->name = $request->nombre;
    	$permisos->slug = $request->url;
    	$permisos->description =$request->descripcion;

    	$permisos->save();
    	
        Flash("¡Se ha registrado el privilegio ". $permisos->name . " de forma exitosa!")->success()->important();
        return redirect()->action('PermissionsController@index');

    }



   public function edit($id){

    	$permisos = Permission::find($id);

    	return view('user.privilegio.edit')->with('permisos', $permisos);
    }

    public function update(Request $request, $id){

        $this->validate($request, [
            'nombre' => 'required|max:255|unique:permissions,name,' .$id,
            'url' => 'required|max:255|unique:permissions,slug,' .$id,
            'descripcion' => 'required'
        ]);

    	$permisos = Permission::find($id);

    	$permisos->name= $request->nombre;
    	$permisos->slug = $request->url;
    	$permisos->description = $request->descripcion;
    	$permisos->update();

        Flash("¡Se ha editado el privilegio ". $permisos->name . " de forma exitosa!")->warning()->important();

    	return redirect()->route('permisos.index');
    }

    public function destroy($id){

   
		$permiso = Permission::find($id);
		$permiso->delete();
        Flash("¡Se ha eliminado el privilegio ". $permiso->name . " de forma exitosa!")->error()->important();
		return redirect()->route('permisos.index');

    }
}
