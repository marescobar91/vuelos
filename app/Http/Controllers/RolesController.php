<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Caffeinated\Shinobi\Models\Role;
use Caffeinated\Shinobi\Models\Permission;
use Laracasts\Flash\Flash;
class RolesController extends Controller
{
    

  public function __construct(){

       	$this->middleware('permission:roles.create')->only(['create', 'store']);
        $this->middleware('permission:roles.index')->only('index');
        $this->middleware('permission:roles.edit')->only(['edit', 'update']);
        $this->middleware('permission:roles.show')->only('show');
        $this->middleware('permission:roles.destroy')->only('destroy');
        
    }

    


    public function index(){

    	$roles = Role::orderBy('id', 'DESC')->get();
    	return view('user.rol.index')->with('roles', $roles);

    }

    public function create(){

    	$permisos = Permission::get();

    	return view('user.rol.create', compact('permisos'));
    }


    public function store(Request $request){
         $this->validate($request, [
            'nombre' => 'required|max:255|unique:roles,name',
            'url' => 'required|max:255|unique:roles,slug',
            'descripcion' => 'required',
            'permisos' => 'required'
        ]);

    	$role = new Role();
    	$role->name = $request->nombre;
    	$role->slug = $request->url;
    	$role->description = $request->descripcion;
        $role->special = $request->permiso_especial;
        $role->save();


        //actualizar role
        $role->permissions()->sync($request->get('permisos'));
        Flash("¡Se ha registrado el rol " . $role->name . " de forma exitosa!")->success()->important();

        return redirect()->action('RolesController@index');


    }

   public function edit($id){

        $roles = Role::find($id);
        $permisos = Permission::all();
        $permission= $roles->permissions->pluck('id')->ToArray();

       //dd($permission);

    	return view('user.rol.edit')->with('roles', $roles)->with('permisos', $permisos)->with('permission', $permission);
    }

    public function update(Request $request, $id){
          $this->validate($request, [
            'nombre' => 'required|max:255|unique:roles,name,' .$id,
            'url' => 'required|max:255|unique:roles,slug,' .$id,
            'descripcion' => 'required',
            'permisos' => 'required'
        ]);

    	//actualiza primero al usuario
    	//segundo se actualiza al rol

    	//actualiza el usuario
    	$role = Role::find($id);
    	$role->name = $request->nombre;
    	$role->slug = $request->url;
    	$role->description = $request->descripcion;
    	$role->update();

    	//actualizar role
    	$role->permissions()->sync($request->get('permissions'));
         Flash("¡Se ha editado el rol " . $role->name . " de forma exitosa!")->warning()->important();
    	return redirect()->route('roles.index');
    }

   

    public function destroy($id){

		$role = Role::find($id);
		$role->delete();
         Flash("¡Se ha eliminado el rol " . $role->name . " de forma exitosa!")->error()->important();
		return redirect()->route('roles.index');

    }

    public function show($id){

        $role = Role::find($id); 
      $permission = Permission::join("permission_role","permission_role.permission_id","=","permissions.id")
            ->where("permission_role.role_id",$id)
            ->get();
        return view('user.rol.show',compact('role','permission'));


    }
    
}
