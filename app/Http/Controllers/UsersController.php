<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Caffeinated\Shinobi\Models\Role;
use Validator;
use Auth;
use Hash;
use Laracasts\Flash\Flash;
class UsersController extends Controller
{
    //

	public function __construct(){

       	$this->middleware('permission:user.index')->only('index');
        $this->middleware('permission:user.edit')->only(['edit', 'update']);
        $this->middleware('permission:user.show')->only('show');
        $this->middleware('permission:user.destroy')->only('destroy');
        $this->middleware('permission:user.inactivo')->only('inactivo');
        $this->middleware('permission:user.habilitar')->only('habilitar');
        $this->middleware('permission:admin.password')->only(['password', 'changepassword']);

        
    }



    public function index(){

    	$user = User::orderBy('id', 'DESC')->get();
        $id = Auth::id();
    	return view('user.index')->with('user', $user)->with('id', $id);

    }

   public function edit($id){


   		$roles = Role::get();
    	$user = User::find($id);
        $rol= $user->roles->pluck('id')->ToArray();
        //dd($rol);

    	return view('user.edit', compact('user', 'roles', 'rol'));
    }

    public function update(Request $request, $id){
        $this->validate($request, [
            'nombre'  => 'required',
            'apellido'  => 'required',
            'usuario' => 'required|max:255',
            'roles'    => 'required'
        ]);
        
       
    	//actualiza primero al usuario
    	//segundo se actualiza al rol

    	//actualiza el usuario
    	$user = User::find($id);
    	$user->nombre = $request->nombre;
    	$user->apellido = $request->apellido;
    	$user->name = $request->usuario;
    	$user->update();

    	//actualizar role
    	$user->roles()->sync($request->get('roles'));
        flash("¡Se ha editado el usuario " . $user->name . " de forma exitosa")->warning()->important();
    	return redirect()->route('user.index');
    }

    public function show($id){

    	$user = User::find($id);

        $roles = Role::join("role_user","role_user.role_id","=","roles.id")
            ->where("role_user.user_id",$id)
            ->get();

    	return view('user.show', compact('user', 'roles'));
    }

    public function destroy($id){

   
		$user = User::find($id);
		$user->delete();
        flash("¡Se ha eliminado el usuario " . $user->name . " de forma exitosa")->error()->important();
		return redirect()->route('user.index');

    }


    public function inactivo(Request $request){

        $users = User::onlyTrashed()->get();
        return view('user.inactivo')->with('users', $users);

    }

    public function habilitar($id){

        $user = User::onlyTrashed()->where('id', $id)->restore();
        $user = User::find($id);
        return redirect()->route('user.index');
    }


    public function password($id){

        $user = User::find($id);
        return view('user.password')->with('user', $user);
    }

    public function changepassword(Request $request, $id){
        $this->validate($request, [
            'contraseña'           => 'required|string|min:8',
            'confirmar_contraseña' => 'required|same:contraseña|min:8',
        ]);
        $user = User::find($id);
        
        //dd($user);
        $user->password = bcrypt($request->contraseña);
        $user->save();
        flash("¡Se ha editado la contraseña " . $user->name . " de forma exitosa")->warning()->important();
        return redirect()->route('user.index');

    }

    public function passwordchange(){

        return view('user.newpassword');
    }


    public function updatepassword(Request $request){

        $this->validate($request, [
            'contraseña_actual'             => 'required',
            'contraseña_nueva'              => 'required|string|min:8',
            'confirmar_contraseña'          => 'required|same:contraseña_nueva|min:8',
        ]);
      

     if (Hash::check($request->contraseña_actual, Auth::user()->password)){
        $user = new User;
        $user->where('email', '=', Auth::user()->email)
             ->update(['password' => bcrypt($request->contraseña_nueva)]);
        flash("¡La contraseña ha sido modificada exitosamente!")->success()->important();
          Auth::logout();
        return redirect()->route('index');

        
        }
    else
        {
        
       
       flash('¡ERROR! ¡La contraseña ingresada no coincide con nuestros datos!')->error()->important();
        return back()->withInput();
        }

}
    
     
}
