<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vuelo;
use App\Aerolinea;
use App\Avion;
use App\Ciudad;
use App\Gateway;
use App\Clase;
use DateTime;
use Carbon\Carbon;
use App\VueloAvion;
use Laracasts\Flash\Flash;
use Illuminate\Support\Facades\DB; 

class VuelosController extends Controller
{
 
 	public function index(){

 		$vuelo= Vuelo::all();
 	
 		return view('vuelos.index')->with('vuelo', $vuelo);

 	}

 	public function create(){
 		$ciudades = Ciudad::get();
 		$gateway = Gateway::where('estado', '<>', 1)->get();
 		$aerolinea = Aerolinea::all();
 		$aviones = Avion::all();
 		
 		return view('vuelos.create',compact('aerolinea','gateway','aviones','ciudades'));
 	}

 	public function store(Request $request){
        $this->validate($request, [
            'origen'              => 'required',
            'destino'             => 'required|different:origen',
            'fecha_salida'        =>  'required',
            'fecha_llegada'       =>  'required',
            'gateway'             =>  'required',
            'aerolinea'           =>  'required',
            'distancia_recorrida' =>  'required',
            'costos'              =>  'required',
            'aviones'             =>  'required'
        ]);

 		$vuelos = new Vuelo();
 		$aero = Aerolinea::find($request->aerolinea);
 		$avion = Avion::find($request->aviones);
 		$vuelos->origen_id = $request->origen;
 		$vuelos->destino_id = $request->destino;
 		$vuelos->fecha_salida = $request->fecha_salida;
 		$vuelos->fecha_llegada = $request->fecha_llegada;
 		$gate = Gateway::find($request->gateway);
 		$vuelos->gateway_id = $gate->id;
 		$vuelos->aerolinea_id = $aero->id;
 		$id=$aero->id;
 		$id_avion = $avion->id;
 		$nombre = $aero->nombre_corto; 
 		$vuelos->kilometros_vuelo = $request->distancia_recorrida;
 		$km= $request->distancia_recorrida;
 		$vuelos->costo = $request->costos;
 		$vuelos->cantidad_millas = 1*$km;
 		$cantidad = 1*$km;
 		$vuelos->millas_otorgar = $cantidad/100;
 		$hsalida=$request->fecha_salida;
 		$hllegada=$request->fecha_llegada;
    Carbon::setLocale('es_SV');
    $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
    $date = $fecha->format('Y-m-d');
    if($hsalida <= $date){

      flash('¡ERROR! ¡La fecha de salida no puede ser menor a la fecha actual!')->error()->important();
      return redirect()->action('VuelosController@create');
    }
    if($hllegada <= $date ){
      flash('¡ERROR! ¡La fecha de llegada no puede ser menor a la fecha actual')->error()->important();
      return redirect()->action('VuelosController@create');
    }
    if($hsalida > $hllegada){
      flash('¡ERROR! ¡La fecha de salida no puede ser mayor a la fecha de llegada')->error()->important();
      return redirect()->action('VuelosController@create');
    }
        $resta = strtotime($hllegada) - strtotime($hsalida);
        $hora = round(($resta/3600),3);
        //dd($hora);
          //dd($vuelos);
        $vuelos->tiempo_vuelo=$hora;
        $words = substr($nombre,0,2);
        $ultimo_vuelo = DB::table('vuelos')->where('vuelos.aerolinea_id', '=', $id)->orderby('id', 'desc')->count('vuelos.id');
        //dd($ultimo_vuelo);
        $i =1;
        if($ultimo_vuelo ==0){
            $vuelos->codigo_vuelo = $words.$i;
        }else
        {
            $i = $i+$ultimo_vuelo;
            $vuelos->codigo_vuelo = $words.$i;
        }

    $vuelos->save();
    $gate->estado = 1;
    $gate->update();
    //dd($id_avion);


        //Traen el ultimo vuelo de la base.
  $ultimo = DB::table('vuelos')->orderby('id', 'desc')->first();


	$f1 = DB::table('avion_clase')->select('avion_clase.fila')->where('avion_clase.clase_id', '=', 1)->where('avion_clase.avion_id', '=', $id_avion)->get();
 	$c1= DB::table('avion_clase')->select('avion_clase.columna')->where('avion_clase.clase_id', '=', 1)->where('avion_clase.avion_id', '=', $id_avion)->get();

 //dd($f1);
 		$fi1=0;
 		$ci1=0;
 		$noF1 =$f1->isEmpty();
 		$noC1 =$c1->isEmpty();
        if($noF1!=true && $noC1 !=true){
 		foreach($f1 as $f){
 			$fi1 = $f->fila;
 		}
    	foreach($c1 as $c){
    		$ci1=$c->columna;
    	}
    	$contar = $ci1*$fi1;
    	for($i=1;$i<=$contar;$i++){

    		$v1 = new VueloAvion();
    		$v1->vuelo_id = $ultimo->id;
    		$v1->avion_id =$id_avion;
        $v1->clase_id =1;
    		$v1->asiento = 'A'.$i;
    		$v1->disponible = 0;
    		$v1->save();
    	}
    }

   $f2 = DB::table('avion_clase')->select('avion_clase.fila')->where('avion_clase.clase_id', '=', 2)->where('avion_clase.avion_id', '=', $id_avion)->get();
    $c2= DB::table('avion_clase')->select('avion_clase.columna')->where('avion_clase.clase_id', '=', 2)->where('avion_clase.avion_id', '=', $id_avion)->get();
        $fi2=0;
        $ci2=0;
       	$noF2 =$f2->isEmpty();
 		$noC2 =$c2->isEmpty();

        if($noF1!=true && $noC1 !=true){
        foreach($f2 as $f){
            $fi2 = $f->fila;
        }
    
        foreach($c2 as $c){
            $ci2=$c->columna;
        }
    
        $contar = $ci2*$fi2;
        for($i=1;$i<=$contar;$i++){

        $v2 = new VueloAvion();
    		$v2->vuelo_id = $ultimo->id;
    		$v2->avion_id =$id_avion;
        $v2->clase_id =2;
    		$v2->asiento = 'B'.$i;
    		$v2->disponible = 0;
    		$v2->save();
        }
    }

    
	$f3 = DB::table('avion_clase')->select('avion_clase.fila')->where('avion_clase.clase_id', '=', 3)->where('avion_clase.avion_id', '=', $id_avion)->get();
 	$c3= DB::table('avion_clase')->select('avion_clase.columna')->where('avion_clase.clase_id', '=', 3)->where('avion_clase.avion_id', '=', $id_avion)->get();
        $fi3=0;
        $ci3=0;
        $noF3 =$f3->isEmpty();
 		$noC3 =$c3->isEmpty();


   if($noF3!=true && $noC3 !=true){
        foreach($f3 as $f){
            $fi3 = $f->fila;
        }
        foreach($c3 as $c){
            $ci3=$c->columna;
        }
    $contar = $ci3*$fi3;
    for($i=1;$i<=$contar;$i++){

//desde aca para itinerario cliente
      $v3 = new VueloAvion();
    	$v3->vuelo_id = $ultimo->id;
    	$v3->avion_id =$id_avion;
      $v3->clase_id =3;
    	$v3->asiento = 'C'.$i;
    	$v3->disponible = 0;
    	$v3->save();
        }
    }

 	$f4 = DB::table('avion_clase')->select('avion_clase.fila')->where('avion_clase.clase_id', '=', 4)->where('avion_clase.avion_id', '=', $id_avion)->get();
  	$c4= DB::table('avion_clase')->select('avion_clase.columna')->where('avion_clase.clase_id', '=', 4)->where('avion_clase.avion_id', '=', $id_avion)->get();
  	//dd($f4);

    $fi4=0;
    $ci4=0;
    $noF4 =$f4->isEmpty();
 	$noC4 =$c4->isEmpty();

   if($noF4!=true && $noC4 !=true){
     foreach($f4 as $f){
            $fi4 = $f->fila;
        }
        foreach($c4 as $c){
            $ci1=$c->columna;
        }
        $contar = $ci4*$fi4;
        for($i=1;$i<=$contar;$i++){
        $v4 = new VueloAvion();
    	$v4->vuelo_id = $ultimo->id;
    	$v4->avion_id =$id_avion;
      $v4->clase_id =4;
    	$v4->asiento = 'D'.$i;
    	$v4->disponible = 0;
    	$v4->save();
        }
    }



// flash("Se ha guardado ".$vuelos->codigo_vuelo." con exito")->success()->important();
    return redirect()->action('VuelosController@tarifas');

   	
}
    public function tarifas(){

    	$vacio="";
    	$vuelo = Vuelo::orderBy('id', 'desc')->first();
    	
    	return view('vuelos.tarifa', compact('vacio', 'vuelo'));

    }

    public function asignacion(Request $request){

    	$vacio="Existe";

    	$vuelo = Vuelo::orderBy('id', 'desc')->first();
    	//dd($vuelo);
    	$id= $vuelo->id;
    	//dd($id);
    	$avion =DB::table('vuelo_avion')->select('vuelo_avion.avion_id')->where('vuelo_avion.vuelo_id', '=', $id )->distinct()->get();
    	//$avion = VueloAvion::where('vuelo_id', '=', $id)->get();
    	$id_avion =0;
    	foreach($avion as $avion){
    		$id_avion =$avion->avion_id;
    	}

    	$clases = DB::table('clases')->leftJoin('avion_clase', 'avion_clase.clase_id','=', 'clases.id')->select('clases.id','clases.nombre_clase')->where('avion_clase.avion_id','=', $id_avion)->get();
   
    	return view('vuelos.tarifa', compact('vacio', 'clases','vuelo'));

    }

    public function guardar(Request $request){

       $this->validate($request, [
            'precio'  =>  'required'
        ]);


    $vuelo = Vuelo::orderBy('id', 'desc')->first();
    $precio = $request->precio;
    $promocion = $request->promocion;
    $clase = $request->clase;

   // dd($clase);

    foreach ($request->get('clase') as $key => $id) { 
      $vuelo->clases()->attach([$id], ['precio' => array_get($precio, $key), 'promocion'=>array_get($promocion, $key)]); 
  }
    
   $vuelo->save()
   ;
    flash("¡Se ha registrado el vuelo " . $vuelo->codigo_vuelo . " de forma exitosa!")->success()->important();
   return redirect()->action('VuelosController@index');

   }

   public function destroy($id){

   	$vuelo = Vuelo::find($id);
   	$gate = $vuelo->gateway_id;
   	$gateway = Gateway::where('id', '=', $gate)->get();
   	foreach($gateway as $gateway)
   	{
   			$gateway->estado= 0;
   			$gateway->update();
   }
    $vuelo->delete($id);

  	DB::table('vuelo_avion')->where('vuelo_id', '=', $id)->delete();
    flash("¡Se ha eliminado el ".$vuelo->codigo_vuelo." de forma exitosa!")->error()->important();
    return redirect()->route('vuelos.index');
   }

   public function edit(Request $request, $id){

    $vuelos = Vuelo::find($id);
    $id=$vuelos->id;
   	$origen = Ciudad::get();
    $destino = Ciudad::get();
 		$gateway = Gateway::all();
 		$aerolinea = Aerolinea::all();
    $aviones = Avion::all();
    $vuelo = DB::table('vuelos')->leftJoin('vuelo_avion', 'vuelo_avion.vuelo_id', '=', 'vuelos.id')->select('vuelos.id', 'vuelo_avion.avion_id')->where('vuelo_avion.vuelo_id', '=', $id)->distinct()->get();
    //dd($vuelo);
    $avion=0;
    foreach($vuelo as $vuelo){
      $avion = $vuelo->avion_id;
    }

 		return view('vuelos.edit', compact('vuelos', 'origen', 'gateway', 'aerolinea', 'aviones', 'destino', 'avion'));

   }

   public function update(Request $request, $id){

     $this->validate($request, [
            'origen'              => 'required',
            'destino'             => 'required|different:origen',
            'fecha_salida'        =>  'required',
            'fecha_llegada'       =>  'required',
            'gateway'             =>  'required',
            'aerolinea'           =>  'required',
            'distancia_recorrida' =>  'required',
            'costos'              =>  'required',
            'precio'              =>  'required'
        ]);


    $vuelos = Vuelo::find($id);
    $aero = Aerolinea::find($request->aerolinea);
 		$vuelos->origen_id = $request->origen;
 		$vuelos->destino_id = $request->destino;
 		$vuelos->fecha_salida = $request->fecha_salida;
 		$vuelos->fecha_llegada = $request->fecha_llegada;
 		$gate = Gateway::find($request->gateway);
 		$vuelos->gateway_id = $gate->id;
 		$vuelos->aerolinea_id = $aero->id;
 		$id=$aero->id;
 		$nombre = $aero->nombre_corto;
 		$vuelos->kilometros_vuelo = $request->distancia_recorrida;
 		$km= $request->distancia_recorrida;
 		$vuelos->costo = $request->costos;
 		$vuelos->cantidad_millas = 1*$km;
 		$cantidad = 1*$km;
 		$vuelos->millas_otorgar = $cantidad/100;
 		$hsalida=$request->fecha_salida;
 		$hllegada=$request->fecha_llegada;
 		$h1 = new DateTime($hsalida);
 		$h2 = new DateTime($hllegada);
    $resta = strtotime($hllegada) - strtotime($hsalida);
    $hora = round(($resta/3600),3);
        //dd($hora);
          //dd($vuelos);
    $vuelos->tiempo_vuelo=$hora;
    $vuelos->save();
    $gate->estado = 1;
    $gate->update();

    $precio= $request->get('precio');
    $promocion = $request->get('promocion');

    
   foreach ($request->get('clase') as $key => $id) { 
      $vuelos->clases()->detach([$id], ['precio' => array_get($precio, $key), 'promocion'=>array_get($promocion, $key)]); 
      $vuelos->clases()->attach([$id], ['precio' => array_get($precio, $key), 'promocion'=>array_get($promocion, $key)]); 

   }
   $vuelos->save();
   flash("Se ha editado ".$vuelos->codigo_vuelo." con exito")->warning()->important();
      return redirect()->action('VuelosController@index');

  }

}
