<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CodigoArea;
use App\Pais;
use Validator;
use Laracasts\Flash\Flash;
class ZonasController extends Controller
{
    //
    public function __construct(){

        $this->middleware('permission:zonas.create')->only(['create', 'store']);
        $this->middleware('permission:zonas.index')->only('index');
        $this->middleware('permission:zonas.edit')->only(['edit', 'update']);
        $this->middleware('permission:zonas.destroy')->only('destroy');
        
    }

    public function index(){

    	$zonas = CodigoArea::orderBy('id', 'DESC')->get();
      	
       	return view('aeropuerto.zona.index')->with('zonas', $zonas);
      }

     public function create(){

     	$paises= Pais::get();
        
       return view('aeropuerto.zona.create', compact('paises')); 
    }


     public function store(Request $request){
        $this->validate($request, [
            'pais'  => 'required|unique:codigosareas,pais_id',
            'codigo_area'  => 'required|unique:codigosareas,codigo'
        ]);
        
   

     	$zonas = new CodigoArea();
     	$zonas->pais_id = $request->pais;
     	$zonas->codigo = $request->codigo_area;
     	$zonas->save();
        flash("¡Se ha registrado el codigo de area " . $zonas->codigo . " de forma existosa!")->success()->important();

     	return redirect()->action('ZonasController@index');
      
    }

     public function edit($id) {

     	$zona= CodigoArea::find($id);
     	$pais = Pais::all();

     	return view('aeropuerto.zona.edit')->with('zona', $zona)->with('pais', $pais);
         
    }

    public function update(Request $request, $id) { 

          $this->validate($request, [
            'pais'  => 'required|unique:codigosareas,pais_id,' .$id,
            'codigo_area'  => 'required|unique:codigosareas,codigo,' .$id
        ]);

         $zona = CodigoArea::find($id);
         $zona->pais_id=$request->pais;
         $zona->codigo=$request->codigo_area;
         $zona->update();
         flash("¡Se ha editado el codigo de area " . $zona->codigo . " de forma existosa!")->warning()->important();
         return redirect()->action('ZonasController@index');
      
    }

    public function destroy($id){ 
        
        $zona = CodigoArea::find($id);
        $zona->delete();
        flash("¡Se ha eliminado el codigo de area " . $zona->codigo . " de forma existosa!")->error()->important();
        return redirect()->action('ZonasController@index');
    
    }
}
