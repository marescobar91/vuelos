<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Itinerario extends Model
{
    protected $table="itinerarios";

    protected $fillable =['cliente_id', 'costo', 'numero_maletas', 'cantidad','pago'];

    public $timestamps=true; 

    public function cliente(){
    	return $this->belongsTo('App\Cliente', 'cliente_id');
    }

    public function vueloAvion(){
    	return $this->belongsToMany('App\VueloAvion', 'asignacion', 'itinerario_id', 'asiento_id');
    }


     public function clienteItinerario(){
        return $this->hasMany('App\ItinerarioCliente');
    }



}
