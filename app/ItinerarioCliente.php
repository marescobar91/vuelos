<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItinerarioCliente extends Model
{
     protected $table="itinerario_cliente";

     protected $fillable = [ 'cliente_id','itinerario_id','nombre', 'apellido' ];

    public $timestamps=true; 


    public function cliente(){ 
        return $this->belongsTo('App\Cliente');
    }
    
     public function itinerario(){ 
        return $this->belongsTo('App\Itinerario');
    }
}
