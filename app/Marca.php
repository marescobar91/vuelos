<?php

namespace App;
use App\Avion;
use App\Modelo;


use Illuminate\Database\Eloquent\Model;

class Marca extends Model
{
    protected $table="marcas";

    protected $fillable = [ 'nombre_marca'];

    public $timestamps=true;

    public function modelos(){ 
        return $this->hasMany('App\Modelo');
    }
}
