<?php

namespace App;
use App\Marca;
use App\Avion;

use Illuminate\Database\Eloquent\Model;
 
class Modelo extends Model
{
     protected $table="modelos"; 
     
 
    protected $fillable = ['marca_id', 'nombre_modelo',];

    public $timestamps=true;

	public function aviones(){ 
        return $this->hasMany('App\Avion');
    } 

    public function marca(){
    	return $this->belongsTo('App\Marca');
    }
 
  
}

 