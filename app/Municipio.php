<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Municipio extends Model
{
    protected $table="municipios";

    protected $fillable =[ 'departamento_id','nombre_municipio'];

    public $timestamps=true;


    public function departamento(){
    	return $this->belongsTo('App\Departamento');
	}


    public function municipio(){ 
        return $this->hasMany('App\Cliente');
    }

    
}