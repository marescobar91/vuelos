<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Natural extends Model
{
    //

    protected $table="natural";

     protected $fillable = [ 'cliente_id','fecha_nacimientio','genero', 'estado_civil', 'tipo_documento', 'numero_documento'];

    public $timestamps=true; 


    public function cliente(){
    	return $this->belongsTo('App\Cliente');
    }

}
