<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pais extends Model
{
    protected $table="paises"; 

    protected $fillable =['cod_pais','nombre'];

    public $timestamps=true;

    public function departametos(){
    	return $this->hasMany('App\Departamento');
	}

	public function aerolineas(){

		return $this->hasMany('App\Aerolinea');
	}

	public function codigos(){

		return $this->hasMany('App\CodigoArea');
	}

	public function ciudades(){

		return $this->hasMany('App/Ciudad');
	}

	
}
