<?php

namespace App;

use Caffeinated\Shinobi\Traits\ShinobiTrait;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;


class User extends Authenticatable
{
    use Notifiable;
    use ShinobiTrait { restore as private restoreA; }
    use SoftDeletes { restore as private restoreB; }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
       public function restore()
    {
        $this->restoreA();
        $this->restoreB();
    }
    protected $table = "users";

    protected $fillable = [
         'nombre', 'apellido', 'name', 'email', 'password'
    ];

    public $timestamps=true;
    protected $dates = ['deleted_at'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function user(){
        return $this->hasOne('App\Cliente');
    }


}
