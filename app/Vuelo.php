<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder\withPivot;

class Vuelo extends Model
{
    protected $table="vuelos";

    protected $fillable =['aerolinea_id','origen_id','destino_id','tiempo_vuelo','kilometros_vuelo','cantidad_millas','millas_otorgar','fecha_salida','fecha_llegada','costo','gateway_id', 'codigo_vuelo'];


    public function aerolineas(){
    	return $this->belongsTo('App\Aerolinea', 'aerolinea_id', 'id');	
    }
    public function ciudadOrigen(){
    	return $this->belongsTo('App\Ciudad','origen_id', 'id');
    }
    public function ciudadDestino(){
    	return $this->belongsTo('App\Ciudad','destino_id', 'id');
    }
    public function getaways(){
		return $this->belongsTo('App\Gateway', 'gateway_id', 'id');
    }
    public function vueloAvion(){
    	return $this->hasMany('App\VueloAvion');
    }

      public function clases(){
 
        return $this->belongsToMany('App\Clase', 'tarifas', 'vuelo_id', 'clase_id')->withPivot('id','precio', 'promocion');
    } 
}
