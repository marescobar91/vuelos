<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VueloAvion extends Model
{
    //
    protected $table="vuelo_avion";

    protected $fillable =['vuelo_id', 'avion_id', 'asiento', 'disponible'];


    public function vuelo(){
    	return $this->belongsTo('App\Vuelo');
    }

    public function avion(){

    	return $this->belongsTo('App\Avion');
    }

    public function itinerarios(){
    	return $this->belongsToMany('App\Itinerario', 'asignacion', 'asiento_id', 'itinerario_id');
    }
}
