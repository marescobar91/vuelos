--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.16
-- Dumped by pg_dump version 9.5.16

-- Started on 2019-06-20 14:56:40

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12355)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2451 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- TOC entry 244 (class 1255 OID 34260)
-- Name: insert_pais(character, character); Type: FUNCTION; Schema: public; Owner: fivewoman
--

CREATE FUNCTION public.insert_pais(cod_pais character, nombre character) RETURNS void
    LANGUAGE plpgsql
    AS $$ 
BEGIN 
	Insert Into public.paises(cod_pais,nombre)
	Values ('ARG','Argentina'); 
end;
$$;


ALTER FUNCTION public.insert_pais(cod_pais character, nombre character) OWNER TO fivewoman;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 181 (class 1259 OID 33790)
-- Name: aerolineas; Type: TABLE; Schema: public; Owner: fivewoman
--

CREATE TABLE public.aerolineas (
    id integer NOT NULL,
    cod_aerolinea character(4) NOT NULL,
    pais_id integer NOT NULL,
    nombre_aerolinea character varying(50) NOT NULL,
    nombre_corto character varying(25) NOT NULL,
    nombre_responsable character varying(25) NOT NULL,
    apellido_responsable character varying(25) NOT NULL,
    pagina_web character varying(30) NOT NULL,
    facebook character varying(25),
    twitter character varying(25),
    correo character varying(30),
    fecha_fundacion date NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.aerolineas OWNER TO fivewoman;

--
-- TOC entry 182 (class 1259 OID 33793)
-- Name: aerolineas_id_seq; Type: SEQUENCE; Schema: public; Owner: fivewoman
--

CREATE SEQUENCE public.aerolineas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.aerolineas_id_seq OWNER TO fivewoman;

--
-- TOC entry 2452 (class 0 OID 0)
-- Dependencies: 182
-- Name: aerolineas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fivewoman
--

ALTER SEQUENCE public.aerolineas_id_seq OWNED BY public.aerolineas.id;


--
-- TOC entry 183 (class 1259 OID 33795)
-- Name: aeropuertos; Type: TABLE; Schema: public; Owner: fivewoman
--

CREATE TABLE public.aeropuertos (
    id integer NOT NULL,
    cod_aeropuerto character(7) NOT NULL,
    ciudad_id integer NOT NULL,
    telefono integer NOT NULL,
    nombre_responsable character varying(25) NOT NULL,
    apellido_responsable character varying(25) NOT NULL,
    numero_gateway integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    nombre_aeropuerto character varying(255) NOT NULL,
    codigoarea_id integer NOT NULL
);


ALTER TABLE public.aeropuertos OWNER TO fivewoman;

--
-- TOC entry 184 (class 1259 OID 33798)
-- Name: aeropuertos_id_seq; Type: SEQUENCE; Schema: public; Owner: fivewoman
--

CREATE SEQUENCE public.aeropuertos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.aeropuertos_id_seq OWNER TO fivewoman;

--
-- TOC entry 2453 (class 0 OID 0)
-- Dependencies: 184
-- Name: aeropuertos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fivewoman
--

ALTER SEQUENCE public.aeropuertos_id_seq OWNED BY public.aeropuertos.id;


--
-- TOC entry 185 (class 1259 OID 33800)
-- Name: asignacion; Type: TABLE; Schema: public; Owner: fivewoman
--

CREATE TABLE public.asignacion (
    id integer NOT NULL,
    asiento_id integer NOT NULL,
    itinerario_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.asignacion OWNER TO fivewoman;

--
-- TOC entry 207 (class 1259 OID 33863)
-- Name: itinerarios; Type: TABLE; Schema: public; Owner: fivewoman
--

CREATE TABLE public.itinerarios (
    id integer NOT NULL,
    cliente_id integer NOT NULL,
    costo numeric(8,2) NOT NULL,
    numero_maletas integer NOT NULL,
    cantidad integer NOT NULL,
    pago boolean NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.itinerarios OWNER TO fivewoman;

--
-- TOC entry 236 (class 1259 OID 33950)
-- Name: vuelo_avion; Type: TABLE; Schema: public; Owner: fivewoman
--

CREATE TABLE public.vuelo_avion (
    id integer NOT NULL,
    vuelo_id integer NOT NULL,
    avion_id integer NOT NULL,
    asiento character(4) NOT NULL,
    disponible boolean NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    clase_id integer
);


ALTER TABLE public.vuelo_avion OWNER TO fivewoman;

--
-- TOC entry 243 (class 1259 OID 34266)
-- Name: asientos; Type: VIEW; Schema: public; Owner: fivewoman
--

CREATE VIEW public.asientos AS
 SELECT itinerarios.id AS itinerario,
    vuelo_avion.asiento
   FROM ((public.asignacion
     JOIN public.itinerarios ON ((itinerarios.id = asignacion.itinerario_id)))
     LEFT JOIN public.vuelo_avion ON ((asignacion.asiento_id = vuelo_avion.id)));


ALTER TABLE public.asientos OWNER TO fivewoman;

--
-- TOC entry 186 (class 1259 OID 33803)
-- Name: asignacion_id_seq; Type: SEQUENCE; Schema: public; Owner: fivewoman
--

CREATE SEQUENCE public.asignacion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.asignacion_id_seq OWNER TO fivewoman;

--
-- TOC entry 2454 (class 0 OID 0)
-- Dependencies: 186
-- Name: asignacion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fivewoman
--

ALTER SEQUENCE public.asignacion_id_seq OWNED BY public.asignacion.id;


--
-- TOC entry 187 (class 1259 OID 33805)
-- Name: avion_clase; Type: TABLE; Schema: public; Owner: fivewoman
--

CREATE TABLE public.avion_clase (
    id integer NOT NULL,
    avion_id integer NOT NULL,
    clase_id integer NOT NULL,
    fila integer NOT NULL,
    columna integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.avion_clase OWNER TO fivewoman;

--
-- TOC entry 188 (class 1259 OID 33808)
-- Name: avion_clase_id_seq; Type: SEQUENCE; Schema: public; Owner: fivewoman
--

CREATE SEQUENCE public.avion_clase_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.avion_clase_id_seq OWNER TO fivewoman;

--
-- TOC entry 2455 (class 0 OID 0)
-- Dependencies: 188
-- Name: avion_clase_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fivewoman
--

ALTER SEQUENCE public.avion_clase_id_seq OWNED BY public.avion_clase.id;


--
-- TOC entry 189 (class 1259 OID 33810)
-- Name: aviones; Type: TABLE; Schema: public; Owner: fivewoman
--

CREATE TABLE public.aviones (
    id integer NOT NULL,
    modelo_id integer NOT NULL,
    aerolinea_id integer NOT NULL,
    tipo_avion character varying(25) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.aviones OWNER TO fivewoman;

--
-- TOC entry 190 (class 1259 OID 33813)
-- Name: aviones_id_seq; Type: SEQUENCE; Schema: public; Owner: fivewoman
--

CREATE SEQUENCE public.aviones_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.aviones_id_seq OWNER TO fivewoman;

--
-- TOC entry 2456 (class 0 OID 0)
-- Dependencies: 190
-- Name: aviones_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fivewoman
--

ALTER SEQUENCE public.aviones_id_seq OWNED BY public.aviones.id;


--
-- TOC entry 191 (class 1259 OID 33815)
-- Name: ciudades; Type: TABLE; Schema: public; Owner: fivewoman
--

CREATE TABLE public.ciudades (
    id integer NOT NULL,
    pais_id integer NOT NULL,
    nombre_ciudad character varying(25) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.ciudades OWNER TO fivewoman;

--
-- TOC entry 195 (class 1259 OID 33825)
-- Name: clientes; Type: TABLE; Schema: public; Owner: fivewoman
--

CREATE TABLE public.clientes (
    id integer NOT NULL,
    municipio_id integer NOT NULL,
    user_id integer,
    calle character varying(25) NOT NULL,
    pasaje character varying(25),
    colonia character varying(25),
    telefono_movil integer,
    telefono_fijo integer,
    "num_viajeroFrec" character varying(10) NOT NULL,
    milla_asignada numeric(10,2) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    nombre_cliente character varying(255),
    apellido_cliente character varying(255),
    vuelo_id integer NOT NULL
);


ALTER TABLE public.clientes OWNER TO fivewoman;

--
-- TOC entry 201 (class 1259 OID 33843)
-- Name: empresa; Type: TABLE; Schema: public; Owner: fivewoman
--

CREATE TABLE public.empresa (
    id integer NOT NULL,
    cliente_id integer NOT NULL,
    nic character varying(25) NOT NULL,
    nit character varying(25) NOT NULL,
    nombre_empresa character varying(100) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.empresa OWNER TO fivewoman;

--
-- TOC entry 203 (class 1259 OID 33848)
-- Name: gateways; Type: TABLE; Schema: public; Owner: fivewoman
--

CREATE TABLE public.gateways (
    id integer NOT NULL,
    aeropuerto_id integer NOT NULL,
    nombre_gateway character(10) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    estado character varying(255) DEFAULT '0'::character varying NOT NULL,
    CONSTRAINT gateways_estado_check CHECK (((estado)::text = ANY (ARRAY[('0'::character varying)::text, ('1'::character varying)::text])))
);


ALTER TABLE public.gateways OWNER TO fivewoman;

--
-- TOC entry 217 (class 1259 OID 33888)
-- Name: natural; Type: TABLE; Schema: public; Owner: fivewoman
--

CREATE TABLE public."natural" (
    id integer NOT NULL,
    cliente_id integer NOT NULL,
    fecha_nacimiento date NOT NULL,
    genero character varying(255) NOT NULL,
    estado_civil character varying(25) NOT NULL,
    tipo_documento character varying(50) NOT NULL,
    numero_documento character varying(25) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    CONSTRAINT natural_genero_check CHECK (((genero)::text = ANY (ARRAY[('F'::character varying)::text, ('M'::character varying)::text])))
);


ALTER TABLE public."natural" OWNER TO fivewoman;

--
-- TOC entry 238 (class 1259 OID 33955)
-- Name: vuelos; Type: TABLE; Schema: public; Owner: fivewoman
--

CREATE TABLE public.vuelos (
    id integer NOT NULL,
    aerolinea_id integer NOT NULL,
    origen_id integer NOT NULL,
    destino_id integer NOT NULL,
    tiempo_vuelo numeric(10,3) NOT NULL,
    kilometros_vuelo numeric(10,2) NOT NULL,
    cantidad_millas numeric(10,2) NOT NULL,
    millas_otorgar numeric(10,2) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    fecha_salida timestamp(0) without time zone NOT NULL,
    fecha_llegada timestamp(0) without time zone NOT NULL,
    gateway_id integer NOT NULL,
    codigo_vuelo character(10) NOT NULL,
    costo numeric(8,2) NOT NULL,
    estado character varying(255) DEFAULT '0'::character varying NOT NULL,
    CONSTRAINT vuelos_estado_check CHECK (((estado)::text = ANY (ARRAY[('0'::character varying)::text, ('1'::character varying)::text])))
);


ALTER TABLE public.vuelos OWNER TO fivewoman;

--
-- TOC entry 242 (class 1259 OID 34261)
-- Name: boletos; Type: VIEW; Schema: public; Owner: fivewoman
--

CREATE VIEW public.boletos AS
 SELECT vuelos.id AS vuelo,
    clientes.id AS cliente,
    itinerarios.id AS itinerario,
    vuelos.codigo_vuelo,
    gateways.nombre_gateway,
    aeropuertos.nombre_aeropuerto,
    vuelos.*::public.vuelos AS vuelos,
    o.id AS origen,
    d.id AS destino,
    o.nombre_ciudad AS origen_nombre,
    d.nombre_ciudad AS destino_nombre,
    aerolineas.nombre_corto,
    clientes.nombre_cliente,
    clientes.apellido_cliente,
    empresa.nombre_empresa,
    clientes."num_viajeroFrec",
    itinerarios.costo AS pago,
    vuelos.fecha_llegada,
    vuelos.fecha_salida
   FROM (((((((((public.itinerarios
     JOIN public.clientes ON ((clientes.id = itinerarios.cliente_id)))
     LEFT JOIN public.vuelos ON ((vuelos.id = clientes.vuelo_id)))
     LEFT JOIN public.aerolineas ON ((aerolineas.id = vuelos.aerolinea_id)))
     LEFT JOIN public.gateways ON ((gateways.id = vuelos.gateway_id)))
     LEFT JOIN public.empresa ON ((empresa.cliente_id = clientes.id)))
     LEFT JOIN public."natural" ON (("natural".cliente_id = clientes.id)))
     LEFT JOIN public.aeropuertos ON ((aeropuertos.id = gateways.aeropuerto_id)))
     LEFT JOIN public.ciudades o ON ((vuelos.origen_id = o.id)))
     LEFT JOIN public.ciudades d ON ((vuelos.destino_id = d.id)));


ALTER TABLE public.boletos OWNER TO fivewoman;

--
-- TOC entry 193 (class 1259 OID 33820)
-- Name: clases; Type: TABLE; Schema: public; Owner: fivewoman
--

CREATE TABLE public.clases (
    id integer NOT NULL,
    nombre_clase character varying(25) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.clases OWNER TO fivewoman;

--
-- TOC entry 232 (class 1259 OID 33937)
-- Name: tarifas; Type: TABLE; Schema: public; Owner: fivewoman
--

CREATE TABLE public.tarifas (
    id integer NOT NULL,
    precio numeric(8,2) NOT NULL,
    promocion numeric(2,2),
    vuelo_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    clase_id integer NOT NULL
);


ALTER TABLE public.tarifas OWNER TO fivewoman;

--
-- TOC entry 241 (class 1259 OID 34255)
-- Name: busqueda_vuelos; Type: VIEW; Schema: public; Owner: fivewoman
--

CREATE VIEW public.busqueda_vuelos AS
 SELECT vuelos.id AS vuelo,
    vuelos.tiempo_vuelo,
    vuelos.fecha_salida,
    vuelos.fecha_llegada,
    vuelos.codigo_vuelo,
    vuelos.costo,
    aerolineas.nombre_aerolinea,
    tarifas.precio,
    clases.nombre_clase,
    clases.id AS clases,
    o.id AS origen,
    d.id AS destino
   FROM (((((public.vuelos
     JOIN public.aerolineas ON ((vuelos.aerolinea_id = aerolineas.id)))
     LEFT JOIN public.ciudades o ON ((vuelos.origen_id = o.id)))
     LEFT JOIN public.ciudades d ON ((vuelos.destino_id = d.id)))
     LEFT JOIN public.tarifas ON ((vuelos.id = tarifas.vuelo_id)))
     LEFT JOIN public.clases ON ((clases.id = tarifas.clase_id)));


ALTER TABLE public.busqueda_vuelos OWNER TO fivewoman;

--
-- TOC entry 192 (class 1259 OID 33818)
-- Name: ciudades_id_seq; Type: SEQUENCE; Schema: public; Owner: fivewoman
--

CREATE SEQUENCE public.ciudades_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ciudades_id_seq OWNER TO fivewoman;

--
-- TOC entry 2457 (class 0 OID 0)
-- Dependencies: 192
-- Name: ciudades_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fivewoman
--

ALTER SEQUENCE public.ciudades_id_seq OWNED BY public.ciudades.id;


--
-- TOC entry 194 (class 1259 OID 33823)
-- Name: clases_id_seq; Type: SEQUENCE; Schema: public; Owner: fivewoman
--

CREATE SEQUENCE public.clases_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.clases_id_seq OWNER TO fivewoman;

--
-- TOC entry 2458 (class 0 OID 0)
-- Dependencies: 194
-- Name: clases_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fivewoman
--

ALTER SEQUENCE public.clases_id_seq OWNED BY public.clases.id;


--
-- TOC entry 196 (class 1259 OID 33831)
-- Name: clientes_id_seq; Type: SEQUENCE; Schema: public; Owner: fivewoman
--

CREATE SEQUENCE public.clientes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.clientes_id_seq OWNER TO fivewoman;

--
-- TOC entry 2459 (class 0 OID 0)
-- Dependencies: 196
-- Name: clientes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fivewoman
--

ALTER SEQUENCE public.clientes_id_seq OWNED BY public.clientes.id;


--
-- TOC entry 197 (class 1259 OID 33833)
-- Name: codigosareas; Type: TABLE; Schema: public; Owner: fivewoman
--

CREATE TABLE public.codigosareas (
    id integer NOT NULL,
    pais_id integer NOT NULL,
    codigo character(4) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.codigosareas OWNER TO fivewoman;

--
-- TOC entry 198 (class 1259 OID 33836)
-- Name: codigosareas_id_seq; Type: SEQUENCE; Schema: public; Owner: fivewoman
--

CREATE SEQUENCE public.codigosareas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.codigosareas_id_seq OWNER TO fivewoman;

--
-- TOC entry 2460 (class 0 OID 0)
-- Dependencies: 198
-- Name: codigosareas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fivewoman
--

ALTER SEQUENCE public.codigosareas_id_seq OWNED BY public.codigosareas.id;


--
-- TOC entry 240 (class 1259 OID 34250)
-- Name: compra; Type: VIEW; Schema: public; Owner: fivewoman
--

CREATE VIEW public.compra AS
 SELECT vuelos.id AS vuelo,
    itinerarios.id AS itinerario,
    clientes.id AS cliente,
    vuelos.fecha_salida,
    vuelos.fecha_llegada,
    vuelos.tiempo_vuelo,
    aerolineas.nombre_corto,
    itinerarios.costo
   FROM (((public.vuelos
     LEFT JOIN public.aerolineas ON ((aerolineas.id = vuelos.aerolinea_id)))
     LEFT JOIN public.clientes ON ((vuelos.id = clientes.vuelo_id)))
     LEFT JOIN public.itinerarios ON ((clientes.id = itinerarios.cliente_id)));


ALTER TABLE public.compra OWNER TO fivewoman;

--
-- TOC entry 199 (class 1259 OID 33838)
-- Name: departamentos; Type: TABLE; Schema: public; Owner: fivewoman
--

CREATE TABLE public.departamentos (
    id integer NOT NULL,
    pais_id integer NOT NULL,
    nombre_departamento character varying(25) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.departamentos OWNER TO fivewoman;

--
-- TOC entry 200 (class 1259 OID 33841)
-- Name: departamentos_id_seq; Type: SEQUENCE; Schema: public; Owner: fivewoman
--

CREATE SEQUENCE public.departamentos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.departamentos_id_seq OWNER TO fivewoman;

--
-- TOC entry 2461 (class 0 OID 0)
-- Dependencies: 200
-- Name: departamentos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fivewoman
--

ALTER SEQUENCE public.departamentos_id_seq OWNED BY public.departamentos.id;


--
-- TOC entry 202 (class 1259 OID 33846)
-- Name: empresa_id_seq; Type: SEQUENCE; Schema: public; Owner: fivewoman
--

CREATE SEQUENCE public.empresa_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.empresa_id_seq OWNER TO fivewoman;

--
-- TOC entry 2462 (class 0 OID 0)
-- Dependencies: 202
-- Name: empresa_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fivewoman
--

ALTER SEQUENCE public.empresa_id_seq OWNED BY public.empresa.id;


--
-- TOC entry 204 (class 1259 OID 33853)
-- Name: gateways_id_seq; Type: SEQUENCE; Schema: public; Owner: fivewoman
--

CREATE SEQUENCE public.gateways_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gateways_id_seq OWNER TO fivewoman;

--
-- TOC entry 2463 (class 0 OID 0)
-- Dependencies: 204
-- Name: gateways_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fivewoman
--

ALTER SEQUENCE public.gateways_id_seq OWNED BY public.gateways.id;


--
-- TOC entry 205 (class 1259 OID 33855)
-- Name: itinerario_cliente; Type: TABLE; Schema: public; Owner: fivewoman
--

CREATE TABLE public.itinerario_cliente (
    id integer NOT NULL,
    cliente_id integer NOT NULL,
    itinerario_id integer NOT NULL,
    nombre character varying(255) NOT NULL,
    apellido character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.itinerario_cliente OWNER TO fivewoman;

--
-- TOC entry 206 (class 1259 OID 33861)
-- Name: itinerario_cliente_id_seq; Type: SEQUENCE; Schema: public; Owner: fivewoman
--

CREATE SEQUENCE public.itinerario_cliente_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.itinerario_cliente_id_seq OWNER TO fivewoman;

--
-- TOC entry 2464 (class 0 OID 0)
-- Dependencies: 206
-- Name: itinerario_cliente_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fivewoman
--

ALTER SEQUENCE public.itinerario_cliente_id_seq OWNED BY public.itinerario_cliente.id;


--
-- TOC entry 208 (class 1259 OID 33866)
-- Name: itinerarios_id_seq; Type: SEQUENCE; Schema: public; Owner: fivewoman
--

CREATE SEQUENCE public.itinerarios_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.itinerarios_id_seq OWNER TO fivewoman;

--
-- TOC entry 2465 (class 0 OID 0)
-- Dependencies: 208
-- Name: itinerarios_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fivewoman
--

ALTER SEQUENCE public.itinerarios_id_seq OWNED BY public.itinerarios.id;


--
-- TOC entry 209 (class 1259 OID 33868)
-- Name: marcas; Type: TABLE; Schema: public; Owner: fivewoman
--

CREATE TABLE public.marcas (
    id integer NOT NULL,
    nombre_marca character varying(100) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.marcas OWNER TO fivewoman;

--
-- TOC entry 210 (class 1259 OID 33871)
-- Name: marcas_id_seq; Type: SEQUENCE; Schema: public; Owner: fivewoman
--

CREATE SEQUENCE public.marcas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.marcas_id_seq OWNER TO fivewoman;

--
-- TOC entry 2466 (class 0 OID 0)
-- Dependencies: 210
-- Name: marcas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fivewoman
--

ALTER SEQUENCE public.marcas_id_seq OWNED BY public.marcas.id;


--
-- TOC entry 211 (class 1259 OID 33873)
-- Name: migrations; Type: TABLE; Schema: public; Owner: fivewoman
--

CREATE TABLE public.migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);


ALTER TABLE public.migrations OWNER TO fivewoman;

--
-- TOC entry 212 (class 1259 OID 33876)
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: fivewoman
--

CREATE SEQUENCE public.migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.migrations_id_seq OWNER TO fivewoman;

--
-- TOC entry 2467 (class 0 OID 0)
-- Dependencies: 212
-- Name: migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fivewoman
--

ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;


--
-- TOC entry 213 (class 1259 OID 33878)
-- Name: modelos; Type: TABLE; Schema: public; Owner: fivewoman
--

CREATE TABLE public.modelos (
    id integer NOT NULL,
    marca_id integer NOT NULL,
    nombre_modelo character(30) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.modelos OWNER TO fivewoman;

--
-- TOC entry 214 (class 1259 OID 33881)
-- Name: modelos_id_seq; Type: SEQUENCE; Schema: public; Owner: fivewoman
--

CREATE SEQUENCE public.modelos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.modelos_id_seq OWNER TO fivewoman;

--
-- TOC entry 2468 (class 0 OID 0)
-- Dependencies: 214
-- Name: modelos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fivewoman
--

ALTER SEQUENCE public.modelos_id_seq OWNED BY public.modelos.id;


--
-- TOC entry 215 (class 1259 OID 33883)
-- Name: municipios; Type: TABLE; Schema: public; Owner: fivewoman
--

CREATE TABLE public.municipios (
    id integer NOT NULL,
    departamento_id integer NOT NULL,
    nombre_municipio character varying(25) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.municipios OWNER TO fivewoman;

--
-- TOC entry 216 (class 1259 OID 33886)
-- Name: municipios_id_seq; Type: SEQUENCE; Schema: public; Owner: fivewoman
--

CREATE SEQUENCE public.municipios_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.municipios_id_seq OWNER TO fivewoman;

--
-- TOC entry 2469 (class 0 OID 0)
-- Dependencies: 216
-- Name: municipios_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fivewoman
--

ALTER SEQUENCE public.municipios_id_seq OWNED BY public.municipios.id;


--
-- TOC entry 218 (class 1259 OID 33892)
-- Name: natural_id_seq; Type: SEQUENCE; Schema: public; Owner: fivewoman
--

CREATE SEQUENCE public.natural_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.natural_id_seq OWNER TO fivewoman;

--
-- TOC entry 2470 (class 0 OID 0)
-- Dependencies: 218
-- Name: natural_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fivewoman
--

ALTER SEQUENCE public.natural_id_seq OWNED BY public."natural".id;


--
-- TOC entry 219 (class 1259 OID 33894)
-- Name: paises; Type: TABLE; Schema: public; Owner: fivewoman
--

CREATE TABLE public.paises (
    id integer NOT NULL,
    cod_pais character(3) NOT NULL,
    nombre character varying(25) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.paises OWNER TO fivewoman;

--
-- TOC entry 220 (class 1259 OID 33897)
-- Name: paises_id_seq; Type: SEQUENCE; Schema: public; Owner: fivewoman
--

CREATE SEQUENCE public.paises_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.paises_id_seq OWNER TO fivewoman;

--
-- TOC entry 2471 (class 0 OID 0)
-- Dependencies: 220
-- Name: paises_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fivewoman
--

ALTER SEQUENCE public.paises_id_seq OWNED BY public.paises.id;


--
-- TOC entry 221 (class 1259 OID 33899)
-- Name: password_resets; Type: TABLE; Schema: public; Owner: fivewoman
--

CREATE TABLE public.password_resets (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone
);


ALTER TABLE public.password_resets OWNER TO fivewoman;

--
-- TOC entry 222 (class 1259 OID 33905)
-- Name: permission_role; Type: TABLE; Schema: public; Owner: fivewoman
--

CREATE TABLE public.permission_role (
    id bigint NOT NULL,
    permission_id bigint NOT NULL,
    role_id bigint NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.permission_role OWNER TO fivewoman;

--
-- TOC entry 223 (class 1259 OID 33908)
-- Name: permission_role_id_seq; Type: SEQUENCE; Schema: public; Owner: fivewoman
--

CREATE SEQUENCE public.permission_role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.permission_role_id_seq OWNER TO fivewoman;

--
-- TOC entry 2472 (class 0 OID 0)
-- Dependencies: 223
-- Name: permission_role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fivewoman
--

ALTER SEQUENCE public.permission_role_id_seq OWNED BY public.permission_role.id;


--
-- TOC entry 224 (class 1259 OID 33910)
-- Name: permission_user; Type: TABLE; Schema: public; Owner: fivewoman
--

CREATE TABLE public.permission_user (
    id bigint NOT NULL,
    permission_id bigint NOT NULL,
    user_id bigint NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.permission_user OWNER TO fivewoman;

--
-- TOC entry 225 (class 1259 OID 33913)
-- Name: permission_user_id_seq; Type: SEQUENCE; Schema: public; Owner: fivewoman
--

CREATE SEQUENCE public.permission_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.permission_user_id_seq OWNER TO fivewoman;

--
-- TOC entry 2473 (class 0 OID 0)
-- Dependencies: 225
-- Name: permission_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fivewoman
--

ALTER SEQUENCE public.permission_user_id_seq OWNED BY public.permission_user.id;


--
-- TOC entry 226 (class 1259 OID 33915)
-- Name: permissions; Type: TABLE; Schema: public; Owner: fivewoman
--

CREATE TABLE public.permissions (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    slug character varying(255) NOT NULL,
    description text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.permissions OWNER TO fivewoman;

--
-- TOC entry 227 (class 1259 OID 33921)
-- Name: permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: fivewoman
--

CREATE SEQUENCE public.permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.permissions_id_seq OWNER TO fivewoman;

--
-- TOC entry 2474 (class 0 OID 0)
-- Dependencies: 227
-- Name: permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fivewoman
--

ALTER SEQUENCE public.permissions_id_seq OWNED BY public.permissions.id;


--
-- TOC entry 228 (class 1259 OID 33923)
-- Name: role_user; Type: TABLE; Schema: public; Owner: fivewoman
--

CREATE TABLE public.role_user (
    id bigint NOT NULL,
    role_id bigint NOT NULL,
    user_id bigint NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.role_user OWNER TO fivewoman;

--
-- TOC entry 229 (class 1259 OID 33926)
-- Name: role_user_id_seq; Type: SEQUENCE; Schema: public; Owner: fivewoman
--

CREATE SEQUENCE public.role_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.role_user_id_seq OWNER TO fivewoman;

--
-- TOC entry 2475 (class 0 OID 0)
-- Dependencies: 229
-- Name: role_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fivewoman
--

ALTER SEQUENCE public.role_user_id_seq OWNED BY public.role_user.id;


--
-- TOC entry 230 (class 1259 OID 33928)
-- Name: roles; Type: TABLE; Schema: public; Owner: fivewoman
--

CREATE TABLE public.roles (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    slug character varying(255) NOT NULL,
    description text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    special character varying(255),
    CONSTRAINT roles_special_check CHECK (((special)::text = ANY (ARRAY[('all-access'::character varying)::text, ('no-access'::character varying)::text])))
);


ALTER TABLE public.roles OWNER TO fivewoman;

--
-- TOC entry 231 (class 1259 OID 33935)
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: fivewoman
--

CREATE SEQUENCE public.roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.roles_id_seq OWNER TO fivewoman;

--
-- TOC entry 2476 (class 0 OID 0)
-- Dependencies: 231
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fivewoman
--

ALTER SEQUENCE public.roles_id_seq OWNED BY public.roles.id;


--
-- TOC entry 233 (class 1259 OID 33940)
-- Name: tarifas_id_seq; Type: SEQUENCE; Schema: public; Owner: fivewoman
--

CREATE SEQUENCE public.tarifas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tarifas_id_seq OWNER TO fivewoman;

--
-- TOC entry 2477 (class 0 OID 0)
-- Dependencies: 233
-- Name: tarifas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fivewoman
--

ALTER SEQUENCE public.tarifas_id_seq OWNED BY public.tarifas.id;


--
-- TOC entry 234 (class 1259 OID 33942)
-- Name: users; Type: TABLE; Schema: public; Owner: fivewoman
--

CREATE TABLE public.users (
    id integer NOT NULL,
    nombre character varying(25) NOT NULL,
    apellido character varying(25) NOT NULL,
    name character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    remember_token character varying(100),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE public.users OWNER TO fivewoman;

--
-- TOC entry 235 (class 1259 OID 33948)
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: fivewoman
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO fivewoman;

--
-- TOC entry 2478 (class 0 OID 0)
-- Dependencies: 235
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fivewoman
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- TOC entry 237 (class 1259 OID 33953)
-- Name: vuelo_avion_id_seq; Type: SEQUENCE; Schema: public; Owner: fivewoman
--

CREATE SEQUENCE public.vuelo_avion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vuelo_avion_id_seq OWNER TO fivewoman;

--
-- TOC entry 2479 (class 0 OID 0)
-- Dependencies: 237
-- Name: vuelo_avion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fivewoman
--

ALTER SEQUENCE public.vuelo_avion_id_seq OWNED BY public.vuelo_avion.id;


--
-- TOC entry 239 (class 1259 OID 33960)
-- Name: vuelos_id_seq; Type: SEQUENCE; Schema: public; Owner: fivewoman
--

CREATE SEQUENCE public.vuelos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vuelos_id_seq OWNER TO fivewoman;

--
-- TOC entry 2480 (class 0 OID 0)
-- Dependencies: 239
-- Name: vuelos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fivewoman
--

ALTER SEQUENCE public.vuelos_id_seq OWNED BY public.vuelos.id;


--
-- TOC entry 2176 (class 2604 OID 33962)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.aerolineas ALTER COLUMN id SET DEFAULT nextval('public.aerolineas_id_seq'::regclass);


--
-- TOC entry 2177 (class 2604 OID 33963)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.aeropuertos ALTER COLUMN id SET DEFAULT nextval('public.aeropuertos_id_seq'::regclass);


--
-- TOC entry 2178 (class 2604 OID 33964)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.asignacion ALTER COLUMN id SET DEFAULT nextval('public.asignacion_id_seq'::regclass);


--
-- TOC entry 2179 (class 2604 OID 33965)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.avion_clase ALTER COLUMN id SET DEFAULT nextval('public.avion_clase_id_seq'::regclass);


--
-- TOC entry 2180 (class 2604 OID 33966)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.aviones ALTER COLUMN id SET DEFAULT nextval('public.aviones_id_seq'::regclass);


--
-- TOC entry 2181 (class 2604 OID 33967)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.ciudades ALTER COLUMN id SET DEFAULT nextval('public.ciudades_id_seq'::regclass);


--
-- TOC entry 2182 (class 2604 OID 33968)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.clases ALTER COLUMN id SET DEFAULT nextval('public.clases_id_seq'::regclass);


--
-- TOC entry 2183 (class 2604 OID 33969)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.clientes ALTER COLUMN id SET DEFAULT nextval('public.clientes_id_seq'::regclass);


--
-- TOC entry 2184 (class 2604 OID 33970)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.codigosareas ALTER COLUMN id SET DEFAULT nextval('public.codigosareas_id_seq'::regclass);


--
-- TOC entry 2185 (class 2604 OID 33971)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.departamentos ALTER COLUMN id SET DEFAULT nextval('public.departamentos_id_seq'::regclass);


--
-- TOC entry 2186 (class 2604 OID 33972)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.empresa ALTER COLUMN id SET DEFAULT nextval('public.empresa_id_seq'::regclass);


--
-- TOC entry 2188 (class 2604 OID 33973)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.gateways ALTER COLUMN id SET DEFAULT nextval('public.gateways_id_seq'::regclass);


--
-- TOC entry 2190 (class 2604 OID 33974)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.itinerario_cliente ALTER COLUMN id SET DEFAULT nextval('public.itinerario_cliente_id_seq'::regclass);


--
-- TOC entry 2191 (class 2604 OID 33975)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.itinerarios ALTER COLUMN id SET DEFAULT nextval('public.itinerarios_id_seq'::regclass);


--
-- TOC entry 2192 (class 2604 OID 33976)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.marcas ALTER COLUMN id SET DEFAULT nextval('public.marcas_id_seq'::regclass);


--
-- TOC entry 2193 (class 2604 OID 33977)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);


--
-- TOC entry 2194 (class 2604 OID 33978)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.modelos ALTER COLUMN id SET DEFAULT nextval('public.modelos_id_seq'::regclass);


--
-- TOC entry 2195 (class 2604 OID 33979)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.municipios ALTER COLUMN id SET DEFAULT nextval('public.municipios_id_seq'::regclass);


--
-- TOC entry 2196 (class 2604 OID 33980)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public."natural" ALTER COLUMN id SET DEFAULT nextval('public.natural_id_seq'::regclass);


--
-- TOC entry 2198 (class 2604 OID 33981)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.paises ALTER COLUMN id SET DEFAULT nextval('public.paises_id_seq'::regclass);


--
-- TOC entry 2199 (class 2604 OID 33982)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.permission_role ALTER COLUMN id SET DEFAULT nextval('public.permission_role_id_seq'::regclass);


--
-- TOC entry 2200 (class 2604 OID 33983)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.permission_user ALTER COLUMN id SET DEFAULT nextval('public.permission_user_id_seq'::regclass);


--
-- TOC entry 2201 (class 2604 OID 33984)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.permissions ALTER COLUMN id SET DEFAULT nextval('public.permissions_id_seq'::regclass);


--
-- TOC entry 2202 (class 2604 OID 33985)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.role_user ALTER COLUMN id SET DEFAULT nextval('public.role_user_id_seq'::regclass);


--
-- TOC entry 2203 (class 2604 OID 33986)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.roles ALTER COLUMN id SET DEFAULT nextval('public.roles_id_seq'::regclass);


--
-- TOC entry 2205 (class 2604 OID 33987)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.tarifas ALTER COLUMN id SET DEFAULT nextval('public.tarifas_id_seq'::regclass);


--
-- TOC entry 2206 (class 2604 OID 33988)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- TOC entry 2207 (class 2604 OID 33989)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.vuelo_avion ALTER COLUMN id SET DEFAULT nextval('public.vuelo_avion_id_seq'::regclass);


--
-- TOC entry 2209 (class 2604 OID 33990)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.vuelos ALTER COLUMN id SET DEFAULT nextval('public.vuelos_id_seq'::regclass);


--
-- TOC entry 2212 (class 2606 OID 33992)
-- Name: aerolineas_pkey; Type: CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.aerolineas
    ADD CONSTRAINT aerolineas_pkey PRIMARY KEY (id);


--
-- TOC entry 2214 (class 2606 OID 33994)
-- Name: aeropuertos_pkey; Type: CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.aeropuertos
    ADD CONSTRAINT aeropuertos_pkey PRIMARY KEY (id);


--
-- TOC entry 2216 (class 2606 OID 33996)
-- Name: asignacion_pkey; Type: CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.asignacion
    ADD CONSTRAINT asignacion_pkey PRIMARY KEY (id);


--
-- TOC entry 2218 (class 2606 OID 33998)
-- Name: avion_clase_pkey; Type: CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.avion_clase
    ADD CONSTRAINT avion_clase_pkey PRIMARY KEY (id);


--
-- TOC entry 2220 (class 2606 OID 34000)
-- Name: aviones_pkey; Type: CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.aviones
    ADD CONSTRAINT aviones_pkey PRIMARY KEY (id);


--
-- TOC entry 2222 (class 2606 OID 34002)
-- Name: ciudades_pkey; Type: CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.ciudades
    ADD CONSTRAINT ciudades_pkey PRIMARY KEY (id);


--
-- TOC entry 2224 (class 2606 OID 34004)
-- Name: clases_pkey; Type: CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.clases
    ADD CONSTRAINT clases_pkey PRIMARY KEY (id);


--
-- TOC entry 2226 (class 2606 OID 34006)
-- Name: clientes_pkey; Type: CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.clientes
    ADD CONSTRAINT clientes_pkey PRIMARY KEY (id);


--
-- TOC entry 2228 (class 2606 OID 34008)
-- Name: codigosareas_pkey; Type: CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.codigosareas
    ADD CONSTRAINT codigosareas_pkey PRIMARY KEY (id);


--
-- TOC entry 2230 (class 2606 OID 34010)
-- Name: departamentos_pkey; Type: CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.departamentos
    ADD CONSTRAINT departamentos_pkey PRIMARY KEY (id);


--
-- TOC entry 2232 (class 2606 OID 34012)
-- Name: empresa_pkey; Type: CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.empresa
    ADD CONSTRAINT empresa_pkey PRIMARY KEY (id);


--
-- TOC entry 2234 (class 2606 OID 34014)
-- Name: gateways_pkey; Type: CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.gateways
    ADD CONSTRAINT gateways_pkey PRIMARY KEY (id);


--
-- TOC entry 2236 (class 2606 OID 34016)
-- Name: itinerario_cliente_pkey; Type: CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.itinerario_cliente
    ADD CONSTRAINT itinerario_cliente_pkey PRIMARY KEY (id);


--
-- TOC entry 2238 (class 2606 OID 34018)
-- Name: itinerarios_pkey; Type: CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.itinerarios
    ADD CONSTRAINT itinerarios_pkey PRIMARY KEY (id);


--
-- TOC entry 2240 (class 2606 OID 34020)
-- Name: marcas_pkey; Type: CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.marcas
    ADD CONSTRAINT marcas_pkey PRIMARY KEY (id);


--
-- TOC entry 2242 (class 2606 OID 34022)
-- Name: migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);


--
-- TOC entry 2244 (class 2606 OID 34024)
-- Name: modelos_pkey; Type: CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.modelos
    ADD CONSTRAINT modelos_pkey PRIMARY KEY (id);


--
-- TOC entry 2246 (class 2606 OID 34026)
-- Name: municipios_pkey; Type: CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.municipios
    ADD CONSTRAINT municipios_pkey PRIMARY KEY (id);


--
-- TOC entry 2248 (class 2606 OID 34028)
-- Name: natural_pkey; Type: CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public."natural"
    ADD CONSTRAINT natural_pkey PRIMARY KEY (id);


--
-- TOC entry 2250 (class 2606 OID 34030)
-- Name: paises_cod_pais_unique; Type: CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.paises
    ADD CONSTRAINT paises_cod_pais_unique UNIQUE (cod_pais);


--
-- TOC entry 2252 (class 2606 OID 34032)
-- Name: paises_nombre_unique; Type: CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.paises
    ADD CONSTRAINT paises_nombre_unique UNIQUE (nombre);


--
-- TOC entry 2254 (class 2606 OID 34034)
-- Name: paises_pkey; Type: CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.paises
    ADD CONSTRAINT paises_pkey PRIMARY KEY (id);


--
-- TOC entry 2259 (class 2606 OID 34036)
-- Name: permission_role_pkey; Type: CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.permission_role
    ADD CONSTRAINT permission_role_pkey PRIMARY KEY (id);


--
-- TOC entry 2263 (class 2606 OID 34038)
-- Name: permission_user_pkey; Type: CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.permission_user
    ADD CONSTRAINT permission_user_pkey PRIMARY KEY (id);


--
-- TOC entry 2266 (class 2606 OID 34040)
-- Name: permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.permissions
    ADD CONSTRAINT permissions_pkey PRIMARY KEY (id);


--
-- TOC entry 2268 (class 2606 OID 34042)
-- Name: permissions_slug_unique; Type: CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.permissions
    ADD CONSTRAINT permissions_slug_unique UNIQUE (slug);


--
-- TOC entry 2270 (class 2606 OID 34044)
-- Name: role_user_pkey; Type: CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.role_user
    ADD CONSTRAINT role_user_pkey PRIMARY KEY (id);


--
-- TOC entry 2274 (class 2606 OID 34046)
-- Name: roles_name_unique; Type: CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_name_unique UNIQUE (name);


--
-- TOC entry 2276 (class 2606 OID 34048)
-- Name: roles_pkey; Type: CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- TOC entry 2278 (class 2606 OID 34050)
-- Name: roles_slug_unique; Type: CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_slug_unique UNIQUE (slug);


--
-- TOC entry 2280 (class 2606 OID 34052)
-- Name: tarifas_pkey; Type: CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.tarifas
    ADD CONSTRAINT tarifas_pkey PRIMARY KEY (id);


--
-- TOC entry 2282 (class 2606 OID 34054)
-- Name: users_email_unique; Type: CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_email_unique UNIQUE (email);


--
-- TOC entry 2284 (class 2606 OID 34056)
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- TOC entry 2286 (class 2606 OID 34058)
-- Name: vuelo_avion_pkey; Type: CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.vuelo_avion
    ADD CONSTRAINT vuelo_avion_pkey PRIMARY KEY (id);


--
-- TOC entry 2288 (class 2606 OID 34060)
-- Name: vuelos_pkey; Type: CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.vuelos
    ADD CONSTRAINT vuelos_pkey PRIMARY KEY (id);


--
-- TOC entry 2255 (class 1259 OID 34061)
-- Name: password_resets_email_index; Type: INDEX; Schema: public; Owner: fivewoman
--

CREATE INDEX password_resets_email_index ON public.password_resets USING btree (email);


--
-- TOC entry 2256 (class 1259 OID 34062)
-- Name: password_resets_token_index; Type: INDEX; Schema: public; Owner: fivewoman
--

CREATE INDEX password_resets_token_index ON public.password_resets USING btree (token);


--
-- TOC entry 2257 (class 1259 OID 34063)
-- Name: permission_role_permission_id_index; Type: INDEX; Schema: public; Owner: fivewoman
--

CREATE INDEX permission_role_permission_id_index ON public.permission_role USING btree (permission_id);


--
-- TOC entry 2260 (class 1259 OID 34064)
-- Name: permission_role_role_id_index; Type: INDEX; Schema: public; Owner: fivewoman
--

CREATE INDEX permission_role_role_id_index ON public.permission_role USING btree (role_id);


--
-- TOC entry 2261 (class 1259 OID 34065)
-- Name: permission_user_permission_id_index; Type: INDEX; Schema: public; Owner: fivewoman
--

CREATE INDEX permission_user_permission_id_index ON public.permission_user USING btree (permission_id);


--
-- TOC entry 2264 (class 1259 OID 34066)
-- Name: permission_user_user_id_index; Type: INDEX; Schema: public; Owner: fivewoman
--

CREATE INDEX permission_user_user_id_index ON public.permission_user USING btree (user_id);


--
-- TOC entry 2271 (class 1259 OID 34067)
-- Name: role_user_role_id_index; Type: INDEX; Schema: public; Owner: fivewoman
--

CREATE INDEX role_user_role_id_index ON public.role_user USING btree (role_id);


--
-- TOC entry 2272 (class 1259 OID 34068)
-- Name: role_user_user_id_index; Type: INDEX; Schema: public; Owner: fivewoman
--

CREATE INDEX role_user_user_id_index ON public.role_user USING btree (user_id);


--
-- TOC entry 2289 (class 2606 OID 34069)
-- Name: aerolineas_pais_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.aerolineas
    ADD CONSTRAINT aerolineas_pais_id_foreign FOREIGN KEY (pais_id) REFERENCES public.paises(id) ON DELETE CASCADE;


--
-- TOC entry 2290 (class 2606 OID 34074)
-- Name: aeropuertos_ciudad_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.aeropuertos
    ADD CONSTRAINT aeropuertos_ciudad_id_foreign FOREIGN KEY (ciudad_id) REFERENCES public.ciudades(id) ON DELETE CASCADE;


--
-- TOC entry 2291 (class 2606 OID 34079)
-- Name: aeropuertos_codigoarea_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.aeropuertos
    ADD CONSTRAINT aeropuertos_codigoarea_id_foreign FOREIGN KEY (codigoarea_id) REFERENCES public.codigosareas(id) ON DELETE CASCADE;


--
-- TOC entry 2292 (class 2606 OID 34084)
-- Name: asignacion_asiento_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.asignacion
    ADD CONSTRAINT asignacion_asiento_id_foreign FOREIGN KEY (asiento_id) REFERENCES public.vuelo_avion(id) ON DELETE CASCADE;


--
-- TOC entry 2293 (class 2606 OID 34089)
-- Name: asignacion_itinerario_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.asignacion
    ADD CONSTRAINT asignacion_itinerario_id_foreign FOREIGN KEY (itinerario_id) REFERENCES public.itinerarios(id) ON DELETE CASCADE;


--
-- TOC entry 2294 (class 2606 OID 34094)
-- Name: avion_clase_avion_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.avion_clase
    ADD CONSTRAINT avion_clase_avion_id_foreign FOREIGN KEY (avion_id) REFERENCES public.aviones(id) ON DELETE CASCADE;


--
-- TOC entry 2295 (class 2606 OID 34099)
-- Name: avion_clase_clase_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.avion_clase
    ADD CONSTRAINT avion_clase_clase_id_foreign FOREIGN KEY (clase_id) REFERENCES public.clases(id) ON DELETE CASCADE;


--
-- TOC entry 2296 (class 2606 OID 34104)
-- Name: aviones_aerolinea_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.aviones
    ADD CONSTRAINT aviones_aerolinea_id_foreign FOREIGN KEY (aerolinea_id) REFERENCES public.aerolineas(id) ON DELETE CASCADE;


--
-- TOC entry 2297 (class 2606 OID 34109)
-- Name: aviones_modelo_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.aviones
    ADD CONSTRAINT aviones_modelo_id_foreign FOREIGN KEY (modelo_id) REFERENCES public.modelos(id) ON DELETE CASCADE;


--
-- TOC entry 2298 (class 2606 OID 34114)
-- Name: ciudades_pais_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.ciudades
    ADD CONSTRAINT ciudades_pais_id_foreign FOREIGN KEY (pais_id) REFERENCES public.paises(id) ON DELETE CASCADE;


--
-- TOC entry 2299 (class 2606 OID 34119)
-- Name: clientes_municipio_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.clientes
    ADD CONSTRAINT clientes_municipio_id_foreign FOREIGN KEY (municipio_id) REFERENCES public.municipios(id) ON DELETE CASCADE;


--
-- TOC entry 2300 (class 2606 OID 34124)
-- Name: clientes_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.clientes
    ADD CONSTRAINT clientes_user_id_foreign FOREIGN KEY (user_id) REFERENCES public.users(id) ON DELETE CASCADE;


--
-- TOC entry 2301 (class 2606 OID 34129)
-- Name: clientes_vuelo_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.clientes
    ADD CONSTRAINT clientes_vuelo_id_foreign FOREIGN KEY (vuelo_id) REFERENCES public.vuelos(id) ON DELETE CASCADE;


--
-- TOC entry 2302 (class 2606 OID 34134)
-- Name: codigosareas_pais_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.codigosareas
    ADD CONSTRAINT codigosareas_pais_id_foreign FOREIGN KEY (pais_id) REFERENCES public.paises(id) ON DELETE CASCADE;


--
-- TOC entry 2303 (class 2606 OID 34139)
-- Name: departamentos_pais_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.departamentos
    ADD CONSTRAINT departamentos_pais_id_foreign FOREIGN KEY (pais_id) REFERENCES public.paises(id) ON DELETE CASCADE;


--
-- TOC entry 2304 (class 2606 OID 34144)
-- Name: empresa_cliente_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.empresa
    ADD CONSTRAINT empresa_cliente_id_foreign FOREIGN KEY (cliente_id) REFERENCES public.clientes(id) ON DELETE CASCADE;


--
-- TOC entry 2305 (class 2606 OID 34149)
-- Name: gateways_aeropuerto_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.gateways
    ADD CONSTRAINT gateways_aeropuerto_id_foreign FOREIGN KEY (aeropuerto_id) REFERENCES public.aeropuertos(id) ON DELETE CASCADE;


--
-- TOC entry 2306 (class 2606 OID 34154)
-- Name: itinerario_cliente_cliente_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.itinerario_cliente
    ADD CONSTRAINT itinerario_cliente_cliente_id_foreign FOREIGN KEY (cliente_id) REFERENCES public.clientes(id) ON DELETE CASCADE;


--
-- TOC entry 2307 (class 2606 OID 34159)
-- Name: itinerario_cliente_itinerario_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.itinerario_cliente
    ADD CONSTRAINT itinerario_cliente_itinerario_id_foreign FOREIGN KEY (itinerario_id) REFERENCES public.itinerarios(id) ON DELETE CASCADE;


--
-- TOC entry 2308 (class 2606 OID 34164)
-- Name: itinerarios_cliente_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.itinerarios
    ADD CONSTRAINT itinerarios_cliente_id_foreign FOREIGN KEY (cliente_id) REFERENCES public.clientes(id) ON DELETE CASCADE;


--
-- TOC entry 2309 (class 2606 OID 34169)
-- Name: modelos_marca_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.modelos
    ADD CONSTRAINT modelos_marca_id_foreign FOREIGN KEY (marca_id) REFERENCES public.marcas(id) ON DELETE CASCADE;


--
-- TOC entry 2310 (class 2606 OID 34174)
-- Name: municipios_departamento_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.municipios
    ADD CONSTRAINT municipios_departamento_id_foreign FOREIGN KEY (departamento_id) REFERENCES public.departamentos(id) ON DELETE CASCADE;


--
-- TOC entry 2311 (class 2606 OID 34179)
-- Name: natural_cliente_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public."natural"
    ADD CONSTRAINT natural_cliente_id_foreign FOREIGN KEY (cliente_id) REFERENCES public.clientes(id) ON DELETE CASCADE;


--
-- TOC entry 2312 (class 2606 OID 34184)
-- Name: permission_role_permission_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.permission_role
    ADD CONSTRAINT permission_role_permission_id_foreign FOREIGN KEY (permission_id) REFERENCES public.permissions(id) ON DELETE CASCADE;


--
-- TOC entry 2313 (class 2606 OID 34189)
-- Name: permission_role_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.permission_role
    ADD CONSTRAINT permission_role_role_id_foreign FOREIGN KEY (role_id) REFERENCES public.roles(id) ON DELETE CASCADE;


--
-- TOC entry 2314 (class 2606 OID 34194)
-- Name: permission_user_permission_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.permission_user
    ADD CONSTRAINT permission_user_permission_id_foreign FOREIGN KEY (permission_id) REFERENCES public.permissions(id) ON DELETE CASCADE;


--
-- TOC entry 2315 (class 2606 OID 34199)
-- Name: permission_user_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.permission_user
    ADD CONSTRAINT permission_user_user_id_foreign FOREIGN KEY (user_id) REFERENCES public.users(id) ON DELETE CASCADE;


--
-- TOC entry 2316 (class 2606 OID 34204)
-- Name: role_user_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.role_user
    ADD CONSTRAINT role_user_role_id_foreign FOREIGN KEY (role_id) REFERENCES public.roles(id) ON DELETE CASCADE;


--
-- TOC entry 2317 (class 2606 OID 34209)
-- Name: role_user_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.role_user
    ADD CONSTRAINT role_user_user_id_foreign FOREIGN KEY (user_id) REFERENCES public.users(id) ON DELETE CASCADE;


--
-- TOC entry 2318 (class 2606 OID 34214)
-- Name: tarifas_clase_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.tarifas
    ADD CONSTRAINT tarifas_clase_id_foreign FOREIGN KEY (clase_id) REFERENCES public.clases(id) ON DELETE CASCADE;


--
-- TOC entry 2319 (class 2606 OID 34219)
-- Name: tarifas_vuelo_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.tarifas
    ADD CONSTRAINT tarifas_vuelo_id_foreign FOREIGN KEY (vuelo_id) REFERENCES public.vuelos(id) ON DELETE CASCADE;


--
-- TOC entry 2320 (class 2606 OID 34224)
-- Name: vuelo_avion_clase_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.vuelo_avion
    ADD CONSTRAINT vuelo_avion_clase_id_foreign FOREIGN KEY (clase_id) REFERENCES public.clases(id) ON DELETE CASCADE;


--
-- TOC entry 2321 (class 2606 OID 34229)
-- Name: vuelos_aerolinea_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.vuelos
    ADD CONSTRAINT vuelos_aerolinea_id_foreign FOREIGN KEY (aerolinea_id) REFERENCES public.aerolineas(id) ON DELETE CASCADE;


--
-- TOC entry 2322 (class 2606 OID 34234)
-- Name: vuelos_destino_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.vuelos
    ADD CONSTRAINT vuelos_destino_id_foreign FOREIGN KEY (destino_id) REFERENCES public.ciudades(id) ON DELETE CASCADE;


--
-- TOC entry 2323 (class 2606 OID 34239)
-- Name: vuelos_gateway_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.vuelos
    ADD CONSTRAINT vuelos_gateway_id_foreign FOREIGN KEY (gateway_id) REFERENCES public.gateways(id) ON DELETE CASCADE;


--
-- TOC entry 2324 (class 2606 OID 34244)
-- Name: vuelos_origen_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: fivewoman
--

ALTER TABLE ONLY public.vuelos
    ADD CONSTRAINT vuelos_origen_id_foreign FOREIGN KEY (origen_id) REFERENCES public.ciudades(id) ON DELETE CASCADE;


--
-- TOC entry 2450 (class 0 OID 0)
-- Dependencies: 7
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2019-06-20 14:56:40

--
-- PostgreSQL database dump complete
--

