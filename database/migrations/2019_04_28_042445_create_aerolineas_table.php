<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAerolineasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aerolineas', function (Blueprint $table) {
            $table->increments('id');
            $table->char('cod_aerolinea', 4);
            $table->integer('pais_id')->unsigned();
            $table->string('nombre_aerolinea', 50);
            $table->string('nombre_corto',25);
            $table->string('nombre_responsable', 25);
            $table->string('apellido_responsable', 25);
            $table->string('pagina_web', 30);
            $table->string('facebook', 25)->nullable();
            $table->string('twitter', 25)->nullable();
            $table->string('correo', 30)->nullable();
            $table->date('fecha_fundacion');
            $table->timestamps();

            $table->foreign('pais_id')->references('id')->on('paises')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aerolineas');
    }
}
