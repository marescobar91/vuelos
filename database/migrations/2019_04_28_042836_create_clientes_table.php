<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('municipio_id')->unsigned();
            $table->integer('user_id')->unsigned()->nullable();
            $table->string('calle',25);
            $table->string('pasaje', 25)->nullable();
            $table->string('colonia', 25)->nullable();
            $table->integer('telefono_movil')->nullable();
            $table->integer('telefono_fijo')->nullable();
            $table->string('num_viajeroFrec', 10);
            $table->decimal('milla_asignada', 10,2);
            $table->foreign('municipio_id')->references('id')->on('municipios')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
}
