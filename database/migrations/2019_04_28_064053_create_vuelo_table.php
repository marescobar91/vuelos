<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVueloTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vuelos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('aerolinea_id')->unsigned();
            $table->integer('origen_id')->unsigned();
            $table->integer('destino_id')->unsigned();
            $table->decimal('tiempo_vuelo',10,3);
            $table->decimal('kilometros_vuelo', 10,2);
            $table->decimal('cantidad_millas', 10,2);
            $table->decimal('millas_otorgar', 10,2);
            $table->timestamps();
            $table->foreign('aerolinea_id')->references('id')->on('aerolineas')->onDelete('cascade');
            $table->foreign('origen_id')->references('id')->on('ciudades')->onDelete('cascade');
            $table->foreign('destino_id')->references('id')->on('ciudades')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vuelos');
    }
}
