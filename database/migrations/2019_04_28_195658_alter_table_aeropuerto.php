<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableAeropuerto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('aeropuertos', function (Blueprint $table) {
            //
            $table->string('nombre_aeropuerto')->after('cod_aeropuerto');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('aeropuertos', function (Blueprint $table) {
            //
        $table->dropColumn('nombre_aeropuerto');
        });
    }
}
