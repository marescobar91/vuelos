<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterForeignCodigoAreaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('aeropuertos', function (Blueprint $table) {
            //
            $table->integer('codigoarea_id')->unsigned();
            $table->foreign('codigoarea_id')
                 ->references('id')->on('codigosareas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('aeropuertos', function (Blueprint $table) {
            //
            $table->dropColumn('codigoarea_id');
        });
    }
}
