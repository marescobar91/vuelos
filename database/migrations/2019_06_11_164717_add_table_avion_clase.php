<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableAvionClase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('avion_clase', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('avion_id')->unsigned();
            $table->integer('clase_id')->unsigned();
            $table->integer('fila');
            $table->integer('columna');
            $table->foreign('avion_id')->references('id')->on('aviones')->onDelete('cascade');
            $table->foreign('clase_id')->references('id')->on('clases')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('avion_clase');
    }
}
