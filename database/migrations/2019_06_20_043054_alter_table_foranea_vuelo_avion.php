<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableForaneaVueloAvion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vuelo_avion', function (Blueprint $table) {
             $table->integer('clase_id')->unsigned()->nullable();
            $table->foreign('clase_id')->references('id')->on('clases')->onDelete('cascade');
        });
    } 

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vuelo_avion', function (Blueprint $table) {
            //
        });
    }
}
