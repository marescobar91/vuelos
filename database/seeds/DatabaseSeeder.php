<?php

use Illuminate\Database\Seeder;
use App\Boleto;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     //   $this->call(UsersTableSeeder::class);
        Boleto::create([
        	'tipo' => 'Ninos'
        	]);
        Boleto::create([
        	'tipo' => 'Adulto'
        	]);
        Boleto::create([
        	'tipo' => 'Anciano'
        	]);
        
    }
}
