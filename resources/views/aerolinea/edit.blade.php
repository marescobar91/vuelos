@extends('layouts.template')
@section('content')
<div class="span12">
  <!-- Despliega mensajes de error -->
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                              <button type="button" class="close" data-dismiss="alert">
                                &times;</button>
                                <strong>¡ERROR!</strong> Hay errores en la información ingresada.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
@endif
<h1 style="align-content: left">Editar Aerolinea</h1>
<form action="{{ route('aerolineas.update', $aerolinea->id) }}" method="post" role="form" class="contactForm">
    {{csrf_field()}}
    {{ method_field('PUT') }}
              <div class="row">
                <div class="span8 form-group {{ $errors->has('nombre_aerolinea') ? ' has-error' : '' }}">
                  <input type="text" name="nombre_aerolinea" class="input-block-level" id="nombre_aerolinea" value="{{ $aerolinea->nombre_aerolinea }}" required />
                  <div class="validation">
                    @if ($errors->has('nombre_aerolinea'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('nombre_aerolinea') }}</strong>
                                    </span>
                    @endif
                  </div>
                </div>
                <div class="span4 form-group {{ $errors->has('nombre_corto') ? ' has-error' : '' }}">
                  <input type="text" name="nombre_corto" class="input-block-level" id="nombre_corto" value="{{ $aerolinea->nombre_corto }}" required />
                   <div class="validation">
                    @if ($errors->has('nombre_corto'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('nombre_corto') }}</strong>
                                    </span>
                    @endif
                  </div>
                </div>                
                <div class="span6 form-group {{ $errors->has('nombre_responsable') ? ' has-error' : '' }}">
                  <input required type="text" name="nombre_responsable" class="input-block-level" id="nombre_responsable" value="{{ $aerolinea->nombre_responsable }}" required />
                   <div class="validation">
                     @if ($errors->has('nombre_responsable'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('nombre_responsable') }}</strong>
                                    </span>
                    @endif
                  </div>
                </div>
                <div class="span6 form-group {{ $errors->has('nombre_responsable') ? ' has-error' : '' }}">
                  <input required type="text" name="apellido_responsable" class="input-block-level" id="apellido_responsable" value="{{ $aerolinea->apellido_responsable }}" required />
                 <div class="validation">
                        @if ($errors->has('apellido_responsable'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('apellido_responsable') }}</strong>
                                    </span>
                    @endif
                  </div>
                </div>
                <div class="span3 form-group">
                  <input required type="text" name="sitio_web" class="input-block-level" id="sitio_web" value="{{ $aerolinea->pagina_web }}" required />
                  <div class="validation"></div>
                </div>
                <div class="span3 form-group">
                  <input type="text" name="facebook" class="input-block-level" id="facebook" value="{{ $aerolinea->facebook }}" placeholder="Facebook" />
                  <div class="validation"></div>
                </div>
                <div class="span3 form-group">
                  <input type="text" name="twitter" class="input-block-level" id="twitter" value="{{ $aerolinea->twitter }}"  placeholder="Twitter"/>
                  <div class="validation"></div>
                </div>
                <div class="span3 form-group">
                  <input type="email" name="correo_electronico" class="input-block-level" id="correo_electronico" value="{{ $aerolinea->correo }}" placeholder="Correo Electronico" />
                  <div class="validation"></div>
                </div>
                <div class="span2 form-group">
                  <label style="float: left;">Fecha de Fundación</label></div>
                  <div class="span4 form-group">  
                  <input required type="date" class="input-block-level" name="fecha_fundacion" id="fecha_fundacion" value="{{ $aerolinea->fecha_fundacion }}" />
                  <div class="validation"></div>
                </div>
                <div class="span6">
                    <select name="pais" class="form-group" required>
                       <option value="">Seleccion Pais</option>
                       @foreach($pais as $pais)
                          <option value="{{$pais->id}}" @if($pais->id==$aerolinea->pais_id)
                            selected='selected' @endif>{{$pais->nombre}} </option>
                      @endforeach
                    </select>
                  </div>
                <div class="span12 form-group">
                  <div class="text-center" style="padding-top: 1%">
                    <button class="btn btn-primary" type="submit">Editar</button>
                    <a href="{{route('aerolineas.index')}}" class="btn btn-secondary" style="float:right 5%;"> <span>
                            <b>Cancelar</b></span></a>
                  </div>
                </div>
              </div>
            </form>
        </div>
@endsection