@extends('layouts.template')
@section('content')
<div class="span10" style="margin-left: 10%">
     <!-- Despliega mensajes de error -->
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                              <button type="button" class="close" data-dismiss="alert">
                                &times;</button>
                                <strong>¡ERROR!</strong> Hay errores en la información ingresada.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
@endif
<h1>Crear Aeropuerto</h1>
	<form action="{{ route('aeropuertos.store') }}" method="post" role="form" class="contactForm">
    {{csrf_field()}}
              <div class="row">
                <div class="span10 form-group {{ $errors->has('nombre_aeropuerto') ? ' has-error' : '' }}">
                  <input type="text" name="nombre_aeropuerto" class="input-block-level" id="nombre_aeropuerto" placeholder="Nombre del Aeropuerto" required />
                     <div class="validation">
                    @if ($errors->has('nombre_aeropuerto'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('nombre_aeropuerto') }}</strong>
                                    </span>
                    @endif
                  </div>
                </div>
                <div class="span3 {{ $errors->has('ciudad') ? ' has-error' : '' }}">
                    <select name="ciudad" class="form-group" required>
                       <option value="">Seleccion Ciudad</option>
                       @foreach($ciudades as $ciudades)
                       <option value="{{ $ciudades->id }}">{{ $ciudades->nombre_ciudad }}</option>
                       @endforeach
                    </select>
                       <div class="validation">
                    @if ($errors->has('ciudad'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('ciudad') }}</strong>
                                    </span>
                    @endif
                  </div>
                  </div>
                <div class="span2 form-group {{ $errors->has('codigo_area') ? ' has-error' : '' }}">
                  <select name="codigo_area" class="form-group" required>
                    <option>Codigo de Area</option>
                    @foreach($codigos as $codigo)
                    <option value="{{ $codigo->id }}">{{ $codigo->codigo }}</option>
                    @endforeach
                  </select>
                    <div class="validation">
                    @if ($errors->has('codigo_area'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('codigo_area') }}</strong>
                                    </span>
                    @endif
                  </div>

                </div>
                  <div class="span3 form-group {{ $errors->has('telefono') ? ' has-error' : '' }}" style="padding-left: 10%">
                  <input type="number" name="telefono" class="input-block-level" id="telefono" placeholder="Telefono" required maxlength="7" oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" />
                <div class="validation">
                    @if ($errors->has('telefono'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('telefono') }}</strong>
                                    </span>
                    @endif
                  </div>
                </div>
                <div class="span5 form-group {{ $errors->has('nombre_responsable') ? ' has-error' : '' }}">
                  <input type="text" name="nombre_responsable" class="input-block-level" id="nombre_responsable" placeholder="Nombre Responsable" required />
                  <div class="validation">
                    @if ($errors->has('nombre_responsable'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('nombre_responsable') }}</strong>
                                    </span>
                    @endif
                  </div>
                </div>
                <div class="span5 form-group {{ $errors->has('apellido_responsable') ? ' has-error' : '' }}">
                  <input type="text" name="apellido_responsable" class="input-block-level" id="apellido_responsable" placeholder="Apellido Responsable" required />
                 <div class="validation">
                    @if ($errors->has('apellido_responsable'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('apellido_responsable') }}</strong>
                                    </span>
                    @endif
                  </div>
                </div>
                <div class="span3 form-group">
                  <label>Numero de Gateway</label>
                </div>
                <div class="span3 form-group {{ $errors->has('gateway') ? ' has-error' : '' }}">
                  <input type="number" name="gateway" class="input-block-level" id="gateway" placeholder="###" required />
                  <div class="validation">
                    @if ($errors->has('gateway'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('gateway') }}</strong>
                                    </span>
                    @endif
                  </div>
                </div>
                
                <div class="span10 form-group">
                  <div class="text-center" style="padding-top: 1%">
                    <button class="btn btn-primary" type="submit">Guardar</button>
                    <a href="{{route('aeropuertos.index')}}" class="btn btn-secondary" style="float:right 5%;"> <span>
                            <b>Cancelar</b></span></a>
                  </div>
                </div>
              </div>
            </form>
        </div>
@endsection