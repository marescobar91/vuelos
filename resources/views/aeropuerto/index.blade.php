@extends('layouts.template')
@section('content')
<div class="container" style="margin-top: 3%">
        <div class="col-md-9 col-md-offset-1">
            <div class="table-title" style=" margin-bottom: 1%">
                <div class="row">
                    <div class="col-sm-5" style="margin-left: 2%">
                        <h1 style="align-content: left">Gestion de Aeropuerto</h1>
                        @can('aeropuertos.create')
                        <a href="{{ route('aeropuertos.create') }}" class="btn btn-primary" style="float:right 5%; "> <span><b>Agregar Aeropuerto</b></span>
                            @endcan
                        </a>
                        @can('zonas.create')
                         <a href="{{ route('zonas.create') }}" class="btn btn-primary" style="float:right 5%;"> <span><b>Agregar Codigo de Area</b></span>
                         </a>
                         @endcan

                    </div>

                </div>
            </div>
            <table class="table table-striped table-hover" id="myTable" style="background: #fff;width: 100%">
                <thead>
                <tr>
                    <th>Codigo</th>
                    <th>Nombre Aeropuerto</th>
                    <th>Encargado</th>
                    <th> Gateways</th>
                    <th width="23%">Acciones</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($aeropuerto as $aeropuerto)
                <tr>
                    <td>{{ $aeropuerto->cod_aeropuerto }}</td>
                     <td>{{ $aeropuerto->nombre_aeropuerto }}</td>
                     <td>{{ $aeropuerto->nombre_responsable }} {{ $aeropuerto->apellido_responsable }}</td>
                     <td> <ul>
                    @foreach($aeropuerto->gateways as $gate)
                        <li>
                            {{$gate->nombre_gateway}}
                        </li>
                      @endforeach
                    </ul></td>
                        <td>
                            @can('aeropuertos.edit')
                        <a href="{{ route('aeropuertos.edit', $aeropuerto->id) }}" class="btn btn-warning"><span class=" icon-pencil"></span> </a>
                        @endcan
                        @can('aeropuertos.destroy')
                        <a style="margin-right: 3%" class="btn btn-danger" onclick="eliminar('{{ $aeropuerto->id }}')"><span class="icon-trash"></span></a>
                        @endcan
                      </td>
                </tr>
                @endforeach
                </tbody>
            </table>
            <!--Modal-->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-body">
                    <p> ¿Desea Eliminar? </p>
                    <input type="hidden">
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <a class="btn btn-danger" id="aceptar">Sí</a>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
<script>
    
    function eliminar(id){
        var iddentified=id;
        
        $('#exampleModal').modal();
        document.getElementById("aceptar").addEventListener("click", function() {
        var url ='{{ route("aeropuertos.destroy", ":id") }}';
        url=url.replace(":id",iddentified);
        window.location.href=url;
        }, false);

    }
    </script>
@endsection
