@extends('layouts.template')
@section('content')
<div class="span6" style="margin-left: 25%">
<!-- Despliega mensajes de error -->
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                              <button type="button" class="close" data-dismiss="alert">
                                &times;</button>
                                <strong>¡ERROR!</strong> Hay errores en la información ingresada.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
@endif
<h1>Agregar Clase</h1>
	<form action="{{route('clases.store')}}" method="post" role="form" class="contactForm">
       {{csrf_field()}}
              <div class="row">
                <div class="span6 form-group {{ $errors->has('nombre_clase') ? ' has-error' : '' }}">
                  <input type="text" class="input-block-level" name="nombre_clase" id="nombre_clase" placeholder="Nombre de Clase" data-rule="email" data-msg="Please enter a valid email" required />
                <div class="validation">
                    @if ($errors->has('nombre_clase'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('nombre_clase') }}</strong>
                                    </span>
                    @endif
                  </div>
                </div>
                <div class="span6 form-group">
                  <div class="text-center">
                    <button class="btn btn-primary" type="submit">Guardar</button>
                     <a href="{{route('clases.index')}}" class="btn btn-secondary" style="float:right 5%;"> <span>
                            <b>Cancelar</b></span></a>
                  </div>
                </div>
              </div>
            </form>
        </div>
              
@endsection