@extends('layouts.template')
@section('content')
<div class="span12">
       <!-- Despliega mensajes de error -->
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                              <button type="button" class="close" data-dismiss="alert">
                                &times;</button>
                                <strong>¡ERROR!</strong> Hay errores en la información ingresada.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
@endif
<h1>Agregar Avion</h1>

  <form  name="asientos" action="{{route('aviones.store')}}" method="post" role="form" class="contactForm">
    {{csrf_field()}}

    <div class="span4 form-group {{ $errors->has('tipo_avion') ? ' has-error' : '' }}">
                  <input type="text" name="tipo_avion" class="input-block-level" id="tipo_avion" placeholder="Tipo de Avion" data-rule="minlen:4" data-msg="Please enter at least 4 chars" required />
                    <div class="validation">
                    @if ($errors->has('tipo_avion'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('tipo_avion') }}</strong>
                                    </span>
                    @endif
                  </div>

                </div> 
             <div class="span3 {{ $errors->has('modelos') ? ' has-error' : '' }}">
                    <select name="modelos" id="modelos" class="form-group" required>
                       <option value="">Seleccion Modelo</option>
                        @foreach($modelo as $model)
                          <option value="{{$model->id}}">{{$model->nombre_modelo}} </option>
                      @endforeach
                    </select>
                      <div class="validation">
                    @if ($errors->has('modelos'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('modelos') }}</strong>
                                    </span>
                    @endif
                  </div>
             </div>
               <div class="span3 {{ $errors->has('aerolinea') ? ' has-error' : '' }}">
                    <select name="aerolinea" class="form-group" required>
                       <option value="">Seleccion Aerolinea</option>
                       @foreach($aerolinea as $aerolineas)
                          <option value="{{$aerolineas->id}}">{{$aerolineas->nombre_aerolinea}} </option>
                      @endforeach
                    </select>
                    <div class="validation">
                    @if ($errors->has('aerolinea'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('aerolinea') }}</strong>
                                    </span>
                    @endif
                  </div>
             </div>
                <div class="span11 form-group">
                  <label><h5>Clases</h5></label>
                </div>
                     <div class="span11 form-group {{ $errors->has('clases') ? ' has-error' : '' }}">
                                    @foreach ($clases as $clase)
                              
                        <input class="in-line" type="checkbox" id="clases" value="{{$clase->id}}" name="clases[]" /> <strong>{{$clase->nombre_clase}}</strong>                         
                                    @endforeach 
                                      <div class="validation">
                    @if ($errors->has('clases'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('clases') }}</strong>
                                    </span>
                    @endif
                  </div>                         
                      </div>
             <div class="span4 form-group" style="margin-left: 32%">

              @foreach($clases as $clase)
                  <label>Asiento de {{ $clase->nombre_clase}}</label>
                  <input type="number" name="fila[]" value="" class="input-block-level" placeholder="Numero de Filas"  />
                  <input type="number" name="columna[]" value="" class="input-block-level" placeholder="Numero de Columna"  />

                  @endforeach
                                </div> 

              <div class="row">
                <div class="span12 form-group">
                  <div class="text-center">
                    <button class="btn btn-primary" type="submit">Guardar</button>
                     <a href="{{route('aviones.index')}}" class="btn btn-secondary" style="float:right 5%;"> <span>
                            <b>Cancelar</b></span></a>
                  </div>
                </div>
              </div>            
            </form>
        </div>
              
@endsection





   
