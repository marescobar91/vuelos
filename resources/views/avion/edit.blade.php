@extends('layouts.template')
@section('content')
<div class="span12">
  <div class="span12">
       <!-- Despliega mensajes de error -->
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                              <button type="button" class="close" data-dismiss="alert">
                                &times;</button>
                                <strong>¡ERROR!</strong> Hay errores en la información ingresada.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
@endif
<h1>Editar Avion</h1>

  <form  name="asientos" action="{{route('aviones.update', $avion->id)}}" method="post" role="form" class="contactForm">
    {{csrf_field()}}
    {{ method_field('PUT') }}


    <div class="span4 form-group {{ $errors->has('tipo_avion') ? ' has-error' : '' }}">
                  <input type="text" name="tipo_avion" class="input-block-level" id="name" placeholder="Tipo de Avion" data-rule="minlen:4" value="{{$avion->tipo_avion}}" data-msg="Please enter at least 4 chars" />
                  <div class="validation">
                    @if ($errors->has('tipo_avion'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('tipo_avion') }}</strong>
                                    </span>
                    @endif
                  </div>

                </div> 

            

             <div class="span3 {{ $errors->has('tipo_avion') ? ' has-error' : '' }}">
                    <select name="modelos" id="modelos" class="form-group" >
                       <option value="">Seleccion Modelo</option>
                        @foreach($modelos as $model)
                          <option value="{{$model->id}}" @if($model->id==$avion->modelo_id) selected ='selected' @endif >{{$model->nombre_modelo}}</option>
                      @endforeach
                    </select>
                    <div class="validation">
                    @if ($errors->has('tipo_avion'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('tipo_avion') }}</strong>
                                    </span>
                    @endif
                  </div>

             </div>

        

               <div class="span3 {{ $errors->has('aerolinea') ? ' has-error' : '' }}">
                    <select name="aerolinea" class="form-group" >
                       <option value="">Seleccion Aerolinea</option>
                       @foreach($aerolinea as $aerolineas)
                          <option value="{{$aerolineas->id}}" @if($aerolineas->id==$avion->aerolinea_id) selected = 'selected' @endif>{{$aerolineas->nombre_aerolinea}} </option>
                      @endforeach
                    </select>
                     <div class="validation">
                    @if ($errors->has('aerolinea'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('aerolinea') }}</strong>
                                    </span>
                    @endif
                  </div>
             </div>

             
                  <div class="span11 form-group">
                  <label><h5>Clases</h5></label>
                </div>
                     <div class="span11 form-group {{ $errors->has('clases') ? ' has-error' : '' }}">
                        @foreach ($clases as $clase)
                              
                        <input class="in-line" type="checkbox" id="clases" value="{{$clase->id}}" {{in_array($clase->id, $class) ? "checked" : null}} name="clases[]" /> <strong>{{$clase->nombre_clase}}</strong>
                                    @endforeach                          
                      </div>
                      <div class="validation">
                    @if ($errors->has('clases'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('clases') }}</strong>
                                    </span>
                    @endif
                  </div>
             </dvi>
            <div class="span4 form-group" style="margin-left: 32%">

              @foreach($avion->clase as $clase)
                  <label>Asiento de {{ $clase->nombre_clase}} </label>
                    <h4>Fila</h4>
                  <input type="number" name="fila[]" value="{{ $clase->pivot->fila }}" class="input-block-level" placeholder="Numero de Filas"  />
                  <h4>Columna</h4>
                  <input type="number" name="columna[]" value="{{ $clase->pivot->columna }}" class="input-block-level" placeholder="Numero de Columna"  />
                  @endforeach
                  <div class="validation"></div>
                </div> 

              <div class="row">
                <div class="span12 form-group">
                  <div class="text-center">
                    <button class="btn btn-primary" type="submit">Guardar</button>
                     <a href="{{route('aviones.index')}}" class="btn btn-secondary" style="float:right 5%;"> <span>
                            <b>Cancelar</b></span></a>
                  </div>
                </div>
              </div>            
            </form>
        </div>
              
@endsection