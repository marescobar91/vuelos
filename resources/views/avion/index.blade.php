@extends('layouts.template')
@section('content')
 @extends('layouts.errores')
<div class="container" style="margin-top: 3%">
        <div class="col-md-9 col-md-offset-1">
            <div class="table-title" style=" margin-bottom: 1%">
                <div class="row">
                    <div class="col-sm-5" style="margin-left: 2%">
                        <h1 style="align-content: left">Gestion de Avion</h1>
                        @can('aviones.create')
                        <a href="{{route('aviones.create')}}" class="btn btn-primary" style="float:right 5%;"> <span><b>Agregar Avion</b></span></a>
                        @endcan
                    </div>
                </div>
            </div> 
            <div>
            <table class="table table-striped table-hover" id="myTable" style="width: 100%">
                <thead>
                <tr>
                    <th>Tipo de Avion</th>
                    <th>Modelo</th>
                    <th>Clase</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody>
                @foreach($aviones as $avion)
                <tr>
                <td>{{$avion->tipo_avion}} </td>  
                <td>{{$avion->modelo->nombre_modelo}} </td>  
                <td> <ul>
                    @foreach($avion->clase as $clases)
                        <li>
                            {{$clases->nombre_clase}}
                        </li>
                      @endforeach
                    </ul>
                </td>
                  
                        <td>
                            @can('aviones.edit')
                        <a href="{{route('aviones.edit', $avion->id)}}" class="btn btn-warning"><span class=" icon-pencil"></span> </a>
                        @endcan
                        @can('aviones.destroy')
                        <a class="btn btn-danger" onclick="eliminar('{{ $avion->id }}')"><span class="icon-trash"></span>
                        </a>
                        @endcan
                      </td>
                </tr>
                 @endforeach
                </tbody>
            </table>
        </div>
         <!--Modal-->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-body">
                    <p> ¿Desea Eliminar? </p>
                    <input type="hidden">
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <a class="btn btn-danger" id="aceptar">Sí</a>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
<script>
    
    function eliminar(id){
        var iddentified=id;
        
        $('#exampleModal').modal();
        document.getElementById("aceptar").addEventListener("click", function() {
        var url ='{{ route("aviones.destroy", ":id") }}';
        url=url.replace(":id",iddentified);
        window.location.href=url;
        }, false);

    }
    </script>
@endsection
