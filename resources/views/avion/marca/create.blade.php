@extends('layouts.template')
@section('content')
<div class="span6" style="margin-left: 25%">
    <!-- Despliega mensajes de error -->
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                              <button type="button" class="close" data-dismiss="alert">
                                &times;</button>
                                <strong>¡ERROR!</strong> Hay errores en la información ingresada.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
@endif
<h1>Crear Marca</h1>
	<form action="{{ route('marcas.store') }}" method="post" role="form" class="contactForm">
    {{csrf_field()}}
              <div class="row">
                <div class="span6 form-group {{ $errors->has('marca') ? ' has-error' : '' }}">
                  <input type="text" name="marca" class="input-block-level" id="marca" placeholder="Nombre" data-rule="minlen:4" data-msg="Please enter at least 4 chars" required />
                 <div class="validation">
                    @if ($errors->has('marca'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('marca') }}</strong>
                                    </span>
                    @endif
                  </div>
                </div>
                <div class="span6 form-group">
                  <div class="text-center">
                    <button class="btn btn-primary" type="submit">Guardar</button>
                     <a href="{{route('marcas.index')}}" class="btn btn-secondary" style="float:right 5%;"> <span>
                            <b>Cancelar</b></span></a>
                  </div>
                </div>
              </div>
            </form>
        </div>
              
@endsection