@extends('layouts.template')
@section('content')
<div class="span6" style="margin-left: 25%">
      <!-- Despliega mensajes de error -->
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                              <button type="button" class="close" data-dismiss="alert">
                                &times;</button>
                                <strong>¡ERROR!</strong> Hay errores en la información ingresada.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
@endif
<h1 >Crear Marca</h1>
  <form action="{{ route('marcas.update', $marca->id) }}" method="post" role="form" class="contactForm">
    {{csrf_field()}}
    {{ method_field('PUT') }}

              <div class="row">
                <div class="span6 form-group {{ $errors->has('nombre_marca') ? ' has-error' : '' }}">
                  <input type="text" name="nombre_marca" class="input-block-level" id="nombre_marca" placeholder="Nombre" data-rule="minlen:4" value="{{$marca->nombre_marca}}" required />
                 <div class="validation">
                    @if ($errors->has('nombre_marca'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('nombre_marca') }}</strong>
                                    </span>
                    @endif
                  </div>
                </div>
                <div class="span6 form-group">
                  <div class="text-center">
                    <button class="btn btn-primary" type="submit">Guardar</button>
                     <a href="{{route('marcas.index')}}" class="btn btn-secondary" style="float:right 5%;"> <span>
                            <b>Cancelar</b></span></a>
                  </div>
                </div>
              </div>
            </form>
        </div>
             
@endsection