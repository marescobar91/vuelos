@extends('layouts.template')
@section('content')
<div class="span6" style="margin-left: 25%">
  <!-- Despliega mensajes de error -->
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                              <button type="button" class="close" data-dismiss="alert">
                                &times;</button>
                                <strong>¡ERROR!</strong> Hay errores en la información ingresada.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
@endif
<h1>Agregar Modelo</h1>
	<form action="{{ route('modelos.update', $modelo->id) }}" method="post" role="form" class="contactForm">
     {{csrf_field()}}
     {{ method_field('PUT') }}
              <div class="span5 {{ $errors->has('marcas') ? ' has-error' : '' }}">
                    <select name="marcas" class="form-group" required>
                       <option value="">Seleccion Marca</option>
                       @foreach($marca as $marca)
                          <option value="{{$marca->id}}" @if($marca->id==$modelo->marca_id)
                            selected='selected' @endif>{{$marca->nombre_marca}} </option>
                      @endforeach
                    </select>
                    <div class="validation">
                    @if ($errors->has('marcas'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('marcas') }}</strong>
                                    </span>
                    @endif
                  </div>
             </div>
             
              <div class="row">
                <div class="span6 form-group {{ $errors->has('nombre') ? ' has-error' : '' }}">
                  <input type="text" name="nombre" class="input-block-level" id="nombre" placeholder="Nombre" data-rule="minlen:4"  value="{{$modelo->nombre_modelo}}" required />
                   <div class="validation">
                    @if ($errors->has('nombre'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('nombre') }}</strong>
                                    </span>
                    @endif
                  </div>
                </div>
                <div class="span6 form-group">
                  <div class="text-center">
                    <button class="btn btn-primary" type="submit">Guardar</button>
                    <!--<button class="btn btn-secondary" type="reset">Cancelar</button>-->
                    <a href="{{route('modelos.index')}}" class="btn btn-secondary" style="float:right 5%;"> <span>
                            <b>Cancelar</b></span></a>
                  </div>
                </div>
              </div>
            </form>
        </div>
              
@endsection




                              