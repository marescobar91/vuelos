@extends('layouts.template')
@section('content')
<div class="span6" style="margin-left: 25%">
     <!-- Despliega mensajes de error -->
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                              <button type="button" class="close" data-dismiss="alert">
                                &times;</button>
                                <strong>¡ERROR!</strong> Hay errores en la información ingresada.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
@endif
<h1>Editar Ciudad</h1>
  <form action="{{ route('ciudades.update', $ciudad->id) }}" method="post" role="form" class="contactForm">
  {{csrf_field()}}
    {{ method_field('PUT') }}
             <div class="span5 {{ $errors->has('pais') ? ' has-error' : '' }}">
                    <select name="pais" class="form-group" required>
                       <option value="">Seleccion Pais</option>
                       @foreach($paises as $pais)
                       <option value="{{$pais->id}}" @if($pais->id==$ciudad->pais_id)
                            selected='selected' @endif>{{$pais->nombre}} </option>
                       @endforeach
                    </select>
                      <div class="validation">
                    @if ($errors->has('pais'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('pais') }}</strong>
                                    </span>
                    @endif
                  </div>
             </div>
              <div class="row">
                <div class="span6 form-group {{ $errors->has('ciudad') ? ' has-error' : '' }}">
                  <input type="text" name="ciudad" class="input-block-level" id="ciudad" value="{{ $ciudad->nombre_ciudad }}" data-rule="minlen:4" data-msg="Please enter at least 4 chars" required />
                    <div class="validation">
                    @if ($errors->has('ciudad'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('ciudad') }}</strong>
                                    </span>
                    @endif
                  </div>
                </div>
                <div class="span6 form-group">
                  <div class="text-center">
                    <button class="btn btn-primary" type="submit">Editar</button>
                    <a href="{{route('ciudades.index')}}" class="btn btn-secondary" style="float:right 5%;"> <span>
                            <b>Cancelar</b></span></a>
                  </div>
                </div>
              </div>
            </form>
        </div>
              
@endsection