@extends('layouts.template')
@section('content')
<div class="container" style="margin-top: 3%">
        <div class="col-md-9 col-md-offset-1">
            <div class="table-title" style=" margin-bottom: 1%">
                <div class="row">
                    <div class="col-sm-5" style="margin-left: 2%">
                        <h1 style="align-content: left">Gestion de Ciudad</h1>
                        @can('ciudades.create')
                        <a href="{{route('ciudades.create')}}" class="btn btn-primary" style="float:right 5%;"> <span><b>Agregar Ciudad</b></span></a>
                        @endcan
                    </div>
                </div>
            </div>
            <table class="table table-striped table-hover" id="myTable" style="background: #fff;width: 100%">
                <thead>
                <tr>
                    <th>N°</th>
                    <th>Municipio</th>
                    <th>Ciudad</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody>
                @foreach($ciudad as $ciudad)
                <tr>
                    <td>{{ $ciudad->id }} </td> 
                     <td> {{ $ciudad->pais->nombre }}</td> 
                     <td> {{ $ciudad->nombre_ciudad }}</td> 
                        <td>
                            @can('ciudades.edit')
                        <a href="{{ route('ciudades.edit', $ciudad->id) }}" class="btn btn-warning"><span class=" icon-pencil"></span> </a>
                        @endcan
                        @can('ciudades.destroy')
                        <a style="margin-right: 3%" class="btn btn-danger" onclick="eliminar('{{ $ciudad->id }}')"><span class="icon-trash"></span>
                            @endcan
                    </a>
                      </td>
                </tr>
                @endforeach
                </tbody>
            </table>
            <!--Modal-->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-body">
                    <p> ¿Desea Eliminar? </p>
                    <input type="hidden">
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <a class="btn btn-danger" id="aceptar">Sí</a>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
<script>
    
    function eliminar(id){
        var iddentified=id;
        
        $('#exampleModal').modal();
        document.getElementById("aceptar").addEventListener("click", function() {
        var url ='{{ route("ciudades.destroy", ":id") }}';
        url=url.replace(":id",iddentified);
        window.location.href=url;
        }, false);

    }
    </script>
@endsection
