@extends('layouts.template')
@section('content')
<div class="span6" style="margin-left: 25%">
   <!-- Despliega mensajes de error -->
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                              <button type="button" class="close" data-dismiss="alert">
                                &times;</button>
                                <strong>¡ERROR!</strong> Hay errores en la información ingresada.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
@endif
<h1 style="align-content: left">Agregar Departamento</h1>
<form action="{{ route('departamentos.store') }}" method="post" role="form" class="contactForm">
    {{csrf_field()}}
              <div class="span5 {{ $errors->has('pais') ? ' has-error' : '' }}">
                    <select name="pais" class="form-group" required>
                       <option value="">Seleccion Pais</option>
                       @foreach($paises as $paises)
                       <option value="{{ $paises->id }}"> {{ $paises->nombre}}</option>
                       @endforeach
                    </select>
                    <div class="validation">
                    @if ($errors->has('pais'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('pais') }}</strong>
                                    </span>
                    @endif
                  </div>
             </div>
              <div class="row">
                <div class="span6 form-group {{ $errors->has('departamento') ? ' has-error' : '' }}">
                  <input type="text" name="departamento" class="input-block-level" id="departamento" placeholder="Nombre del Departamento" data-rule="minlen:4" data-msg="Please enter at least 4 chars" required />
                   <div class="validation">
                    @if ($errors->has('departamento'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('departamento') }}</strong>
                                    </span>
                    @endif
                  </div>
                </div>
                <div class="span6 form-group">
                  <div class="text-center">
                    <button class="btn btn-primary" type="submit">Guardar</button>
                  <a href="{{route('departamentos.index')}}" class="btn btn-secondary" style="float:right 5%;"> <span>
                            <b>Cancelar</b></span></a>
                  </div>
                </div>
              </div>
            </form>
        </div>
              
@endsection