@extends('layouts.template')
@section('content')
<div class="span6" style="margin-left: 25%">
<div class="span6" style="margin-left: 25%">
   <!-- Despliega mensajes de error -->
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                              <button type="button" class="close" data-dismiss="alert">
                                &times;</button>
                                <strong>¡ERROR!</strong> Hay errores en la información ingresada.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
@endif
<h1 style="align-content: left">Editar Departamento</h1>
<form action="{{ route('departamentos.update', $departamentos->id)}}" method="post" role="form" class="contactForm">
     {{csrf_field()}}
    {{ method_field('PUT') }}
              <div class="span5 {{ $errors->has('pais') ? ' has-error' : '' }}">
                    <select name="pais" class="form-group" required>
                       <option value="">Seleccion Pais</option>
                       @foreach($paises as $paises)
                       <option value="{{$paises->id}}" @if($paises->id==$departamentos->pais_id)
                            selected='selected' @endif>{{$paises->nombre}} </option>
                       @endforeach
                    </select>
                    <div class="validation">
                    @if ($errors->has('pais'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('pais') }}</strong>
                                    </span>
                    @endif
                  </div>
             </div>
              <div class="row">
                <div class="span6 form-group">
                  <input type="text" name="departamento" class="input-block-level" id="departamento" value="{{ $departamentos->nombre_departamento }}" data-rule="minlen:4" data-msg="Please enter at least 4 chars" required />
                </div>
                <div class="validation">
                    @if ($errors->has('departamento'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('departamento') }}</strong>
                                    </span>
                    @endif
                  </div>
                <div class="span6 form-group">
                  <div class="text-center">
                    <button class="btn btn-primary" type="submit">Editar</button>
                  <a href="{{route('departamentos.index')}}" class="btn btn-secondary" style="float:right 5%;"> <span>
                            <b>Cancelar</b></span></a>
                  </div>
                </div>
              </div>
            </form>
        </div>
              
@endsection