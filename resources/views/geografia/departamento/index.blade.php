@extends('layouts.template')
@section('content')
<div class="container" style="margin-top: 3%">
        <div class="col-md-9 col-md-offset-1">
            <div class="table-title" style=" margin-bottom: 1%">
                <div class="row">
                    <div class="col-sm-5" style="margin-left: 2%">
                        <h1 style="align-content: left">Gestion de Departamento</h1>
                        @can('departamentos.create')
                        <a href="{{route('departamentos.create')}}" class="btn btn-primary" style="float:right 5%;"> <span><b>Agregar Departamento</b></span></a>
                        @endcan
                    </div>
                </div>
            </div>
            <table class="table table-striped table-hover" id="myTable" style="background: #fff;width: 100%">
                <thead>
                <tr>
                    <th>N°</th>
                    <th>Departamento</th>
                    <th>Pais</th>
                    <th width="23%">Acciones</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($departamentos as $departamentos)
                <tr>
                    <td>{{ $departamentos->id }}</td> 
                    <td>{{ $departamentos->nombre_departamento }}</td>
                    <td>{{ $departamentos->pais->nombre }}</td>
                        <td>
                            @can('departamentos.edit')
                        <a href="{{ route('departamentos.edit', $departamentos->id) }}" class="btn btn-warning"><span class=" icon-pencil"></span> </a>
                        @endcan
                        @can('departamentos.destroy')
                        <a style="margin-right: 3%" class="btn btn-danger" onclick="eliminar('{{ $departamentos->id }}')"><span class="icon-trash"></span>
                    </a>
                    @endcan
                      </td>
                </tr>
                @endforeach
                </tbody>
            </table>
            <!--Modal-->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-body">
                    <p> ¿Desea Eliminar? </p>
                    <input type="hidden">
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <a class="btn btn-danger" id="aceptar">Sí</a>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
<script>
    
    function eliminar(id){
        var iddentified=id;
        
        $('#exampleModal').modal();
        document.getElementById("aceptar").addEventListener("click", function() {
        var url ='{{ route("departamentos.destroy", ":id") }}';
        url=url.replace(":id",iddentified);
        window.location.href=url;
        }, false);

    }
    </script>
@endsection

