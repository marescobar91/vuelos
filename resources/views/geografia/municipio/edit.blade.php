@extends('layouts.template')
@section('content')
<div class="span6" style="margin-left: 25%">
  <div class="span6" style="margin-left: 25%">
     <!-- Despliega mensajes de error -->
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                              <button type="button" class="close" data-dismiss="alert">
                                &times;</button>
                                <strong>¡ERROR!</strong> Hay errores en la información ingresada.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
@endif
<h1 >Editar Municipio</h1>
	<form action="{{ route('municipios.update', $municipios->id)}}" method="post" role="form" class="contactForm">
     {{csrf_field()}}
    {{ method_field('PUT') }}
              <div class="span5 {{ $errors->has('departamento') ? ' has-error' : '' }}">
               <select name="departamento" class="form-group" required>
                       <option value="">Seleccion Departamento</option>
                       @foreach($departamento as $departamento)
                       <option value="{{$departamento->id}}" @if($departamento->id==$municipios->departamento_id)
                            selected='selected' @endif>{{$departamento->nombre_departamento}} </option>
                       @endforeach
                    </select>
                       <div class="validation">
                    @if ($errors->has('departamento'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('departamento') }}</strong>
                                    </span>
                    @endif
                  </div>
            
            
             </div>
              <div class="row">
                <div class="span6 form-group {{ $errors->has('municipio') ? ' has-error' : '' }}">
                  <input type="text" name="municipio" class="input-block-level" id="municipio" value="{{$municipios->nombre_municipio}}" data-rule="minlen:4" data-msg="Please enter at least 4 chars" required />
                     <div class="validation">
                    @if ($errors->has('municipio'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('municipio') }}</strong>
                                    </span>
                    @endif
                  </div>
                </div>
                <div class="span6 form-group">
                  <div class="text-center">
                    <button class="btn btn-primary" type="submit">Editar</button>
                   <a href="{{route('municipios.index')}}" class="btn btn-secondary" style="float:right 5%;"> <span>
                            <b>Cancelar</b></span></a>
                  </div>
                </div>
              </div>
            </form>
        </div>
              
@endsection



