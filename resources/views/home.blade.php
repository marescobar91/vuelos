@extends('layouts.template')

@section('slider')
<section id="intro">

      <div class="container">
        <div class="row">
          <div class="span12">
            <!-- Place somewhere in the <body> of your page -->
            <div id="mainslider" class="flexslider">
              <ul class="slides">
                <li data-thumb="img/slides/flexslider/img5.jpg">
                  <img src="img/slides/flexslider/img5.jpg" alt="" />
                  <div class="flex-caption primary">
                    <h2>Lago de Coatepeque</h2>
                    <p>Santa Ana, El Salvador. ¡Visitalo!</p>
                  </div>
                </li>
                <li data-thumb="img/slides/flexslider/img6.jpg">
                  <img src="img/slides/flexslider/img6.jpg" alt="" />
                  <div class="flex-caption warning">
                    <h2>Machu Picchu</h2>
                    <p>Elevacion de  2,430m, Perú. ¡Visitalo!</p>
                  </div>
                </li>
                <li data-thumb="img/slides/flexslider/img7.jpg">
                  <img src="img/slides/flexslider/img7.jpg" alt="" />
                  <div class="flex-caption success">
                    <h2>Cataratas de Iguazu</h2>
                    <p>Su atractivo es la Garganta del Diablo, Argentina ¡Visitalo! </p>
                  </div>
                </li>
                <li data-thumb="img/slides/flexslider/img9.jpg">
                  <img src="img/slides/flexslider/img9.jpg" alt="" />
                  <div class="flex-caption primary">
                    <h2>Solar de Uyuni </h2>
                    <p>El mas grande desierto de sal, Bolivia ¡Visitalo!</p>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
