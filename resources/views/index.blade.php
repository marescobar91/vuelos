<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>Reserva de Vuelos</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <!-- styles -->
  <link href="css/bootstrap.css" rel="stylesheet">
  <link href="css/bootstrap-responsive.css" rel="stylesheet">
  <link href="css/prettyPhoto.css" rel="stylesheet">
  <link href="js/google-code-prettify/prettify.css" rel="stylesheet">
  <link href="css/flexslider.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet">
  <link href="color/default.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Droid+Serif:400,600,400italic|Open+Sans:400,600,700" rel="stylesheet">

  <!-- fav and touch icons -->
  <link rel="shortcut icon" href="ico/favicon.ico">
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">

  <!-- =======================================================
    Theme Name: Sistema de Vuelos Lumia 
    Author: Five Woman
  ======================================================= -->
</head>


<body>
  <div id="wrapper">
    <header>
      <!-- Navbar
    ================================================== -->
      <div class="navbar navbar-static-top">
        <div class="navbar-inner">
          <div class="container">
            <!-- logo -->
            <div class="logo">
              <a  href="login" class="span5"><img src="img/logo.png" alt="" /></a>
              <h1 class="span8"><strong>Reserva de Vuelos</strong> </h1>
               <div style="float: right; padding-top: 1%;"><label> {{ \Carbon\Carbon::now(new DateTimeZone('America/El_Salvador'))->format('d/m/Y H:i:s')}}</label></div> <!--obtener fecha actual-->
            </div>
            <!-- end logo -->

          </div>
        </div>
      </div>
    </header>
    <section id="intro">

      <div class="container">
        <div class="row">
          <div class="span12">
            <!-- Place somewhere in the <body> of your page -->
            <div id="mainslider" class="flexslider">
              <ul class="slides">
                <li data-thumb="img/slides/flexslider/img5.jpg">
                  <img src="img/slides/flexslider/img5.jpg" alt="" />
                  <div class="flex-caption primary">
                    <h2>Lago de Coatepeque</h2>
                    <p>Santa Ana, El Salvador. ¡Visitalo!</p>
                  </div>
                </li>
                <li data-thumb="img/slides/flexslider/img6.jpg">
                  <img src="img/slides/flexslider/img6.jpg" alt="" />
                  <div class="flex-caption warning">
                    <h2>Machu Picchu</h2>
                    <p>Elevacion de  2,430m, Perú. ¡Visitalo!</p>
                  </div>
                </li>
                <li data-thumb="img/slides/flexslider/img7.jpg">
                  <img src="img/slides/flexslider/img7.jpg" alt="" />
                  <div class="flex-caption success">
                    <h2>Cataratas de Iguazu</h2>
                    <p>Su atractivo es la Garganta del Diablo, Argentina ¡Visitalo! </p>
                  </div>
                </li>
                <li data-thumb="img/slides/flexslider/img9.jpg">
                  <img src="img/slides/flexslider/img9.jpg" alt="" />
                  <div class="flex-caption primary">
                    <h2>Solar de Uyuni </h2>
                    <p>El mas grande desierto de sal, Bolivia ¡Visitalo!</p>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>

    </section>
    <section id="maincontent">
      <div class="container">

        <div class="row">
          <div class="span12">
            <div class="call-action">

              <div class="text">
                <h2>Adquiere tu boleto con <strong>¡DESCUENTO!</strong></h2>
                <p>
                  No te pierdas la oportunidad de viajar y disfrutar 
                </p>
              </div>
              <div class="cta">
                <a class="btn btn-large btn-theme" href="{{route('itinerario.filtro')}}">
							<i class="icon-plane icon-white"></i> ¡Compra ya! </a>
              </div>

            </div>
            <!-- end tagline -->
          </div>
        </div>

        <div class="row">
          <div class="span3 features e_pulse">
            <img src="img/dummies/img11.jpg" alt="" />
            <div class="box">
              <div class="divcenter">
                <a href="{{route('top10')}}"><i class="icon-circled icon-48 icon-globe active icon"></i></a>
                <h4> Top 10  <br />Sitios Turisticos</h4>
              </div>
            </div>
          </div>

          <div class="span3 features e_pulse">
            <img src="img/dummies/img9.jpg" alt="" />
            <div class="box">
              <div class="divcenter">
                <a href="#"><i class="icon-circled icon-48 icon-camera icon"></i></a>
                <h4>Isla Margarita<br />La perla del Caribe</h4>
              </div>
            </div>
          </div>

          <div class="span3 features e_pulse">
            <img src="img/dummies/img12.jpg" alt="" />
            <div class="box">
              <div class="divcenter">
                <a href="#"><i class="icon-circled icon-48 icon-star icon"></i></a>
                <h4>Ciudades<br />Nocturnas </h4>
              </div>
            </div>
          </div>

          <div class="span3 features e_pulse">
            <img src="img/dummies/img10.jpg" alt="" />
            <div class="box">
              <div class="divcenter">
                <a href="#"><i class="icon-circled icon-48 icon-tint icon"></i></a>
                <h4>Playas para <br />Surfing </h4>
              </div>
            </div>
          </div>
        </div>

   <!--     <div class="row">
          <div class="span12">
            <h4 class="rheading">Recent works<span></span></h4>
            <div class="row">
              <div class="span3">

                <p class="hidden-phone">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut in lacus rhoncus elit egestas luctus. Nullam at lectus augue.
                </p>

                <a href="#" class="btn btn-theme">Read More</a>
              </div>

              <div class="span9">
                <div id="latest-work" class="carousel btleft">
                  <div class="carousel-wrapper">
                    <ul class="da-thumbs">

                      <li>
                        <img src="dummies/work1.jpg" alt="" />
                        <article class="da-animate da-slideFromRight">
                          <a class="zoom" data-pretty="prettyPhoto" href="img/dummies/big1.jpg">
														<i class="icon-zoom-in icon-rounded icon-48 active"></i>
													</a>
                          <a href="portfolio-detail.html">
														<i class="icon-link icon-rounded icon-48 active"></i>
													</a>
                          <div class="hidden-tablet">
                            <p>Serenity theme</p>
                          </div>
                        </article>
                      </li>

                      <li>
                        <img src="img/dummies/work2.jpg" alt="" />
                        <article class="da-animate da-slideFromRight">
                          <a class="zoom" data-pretty="prettyPhoto" href="img/dummies/big1.jpg">
														<i class="icon-zoom-in icon-rounded icon-48 active"></i>
													</a>
                          <a href="portfolio-detail.html">
														<i class="icon-link icon-rounded icon-48 active"></i>
													</a>
                          <div class="hidden-tablet">
                            <p>Dark apps</p>
                          </div>
                        </article>
                      </li>

                      <li>
                        <img src="img/dummies/work3.jpg" alt="" />
                        <article class="da-animate da-slideFromRight">
                          <a class="zoom" data-pretty="prettyPhoto" href="img/dummies/big1.jpg">
														<i class="icon-zoom-in icon-rounded icon-48 active"></i>
													</a>
                          <a href="portfolio-detail.html">
														<i class="icon-link icon-rounded icon-48 active"></i>
													</a>
                          <div class="hidden-tablet">
                            <p>Mobile apps</p>
                          </div>
                        </article>
                      </li>

                      <li>
                        <img src="img/dummies/work4.jpg" alt="" />
                        <article class="da-animate da-slideFromRight">
                          <a class="zoom" data-pretty="prettyPhoto" href="img/dummies/big1.jpg">
														<i class="icon-zoom-in icon-rounded icon-48 active"></i>
													</a>
                          <a href="portfolio-detail.html">
														<i class="icon-link icon-rounded icon-48 active"></i>
													</a>
                          <div class="hidden-tablet">
                            <p>Mobile template</p>
                          </div>
                        </article>
                      </li>

                      <li>
                        <img src="img/dummies/work5.jpg" alt="" />
                        <article class="da-animate da-slideFromRight">
                          <a class="zoom" data-pretty="prettyPhoto" href="img/dummies/big1.jpg">
														<i class="icon-zoom-in icon-rounded icon-48 active"></i>
													</a>
                          <a href="portfolio-detail.html">
														<i class="icon-link icon-rounded icon-48 active"></i>
													</a>
                          <div class="hidden-tablet">
                            <p>Business theme</p>
                          </div>
                        </article>
                      </li>

                      <li>
                        <img src="img/dummies/work6.jpg" alt="" />
                        <article class="da-animate da-slideFromRight">
                          <a class="zoom" data-pretty="prettyPhoto" href="img/dummies/big1.jpg">
														<i class="icon-zoom-in icon-rounded icon-48 active"></i>
													</a>
                          <a href="portfolio-detail.html">
														<i class="icon-link icon-rounded icon-48 active"></i>
													</a>
                          <div class="hidden-tablet">
                            <p>Portfolio graphic</p>
                          </div>
                        </article>
                      </li>

                    </ul>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>-->

      </div>
    </section>
    <!-- Footer
 ================================================== -->
    <footer class="footer">
      <div class="container">
        <div class="row">
          <div class="span2"></div>
          <div class="span6">
            <div class="widget">
            <h5 align="center">Aerolineas</h5>
           <!--     <div class="flickr-badge">
                <script type="text/javascript" src="http://www.flickr.com/badge_code_v2.gne?count=9&amp;display=random&amp;size=s&amp;layout=x&amp;source=user&amp;user=34178660@N03"></script>

              </div>-->
              <table cellpadding="10%">
              <tr>
                <td class="span2"><a href="https://aeromexico.com/"><img src="{{asset('img\brands\img1.png')}}"  style="background-color: white;" /></a></td>
                <td class="span3"><a href="https://www.united.com/"><img src="{{asset('img\brands\img2.png')}}"  style="background-color: white;" /></a></td>
              </tr>
              <tr>
                <td class="span2"><a href="https://www.aireuropa.com/"><img src="{{asset('img\brands\img6.png')}}"  style="background-color: white;" /></a></td>
                <td class="span3"><a href="https://www.copaair.com/"><img src="{{asset('img\brands\img3.png')}}"  style="background-color: white;" /></a></td>
              </tr>
              <tr>
               <td class="span2"><a href="https://www.volaris.com/"><img src="{{asset('img\brands\img7.png')}}"  style="background-color: white;" /></a></td>
                <td class="span3"><a href="https://www.avianca.com/"><img src="{{asset('img\brands\img5.png')}}"  style="background-color: white;" /></a></td>
              </tr>
            </table>
              <div class="clear"></div>
            </div>
          </div>
          <div class="span4">
            <div class="widget">
              <a href="{{asset('img\fivewoman.png')}}"><img src="{{asset('img\fivewoman.png')}}" style="width: 25%; padding-left: 10%"></a>
              <h5>Encuentranos en: </h5>
              <address>
                <i class="icon-home"></i> <strong>FiveWoman Company, inc</strong><br>
                Residencial Miralvalle, Avenida Eat<br>
                Milan, Italy
                </address>
              <p>
                <i class="icon-phone"></i> (123) 456-7890 - (123) 555-8890<br>
                <i class="icon-envelope-alt"></i> bussines@fivewoman.com
              </p>
            </div>
            <div class="widget">
              <ul class="social">
                <li><a href="#" data-placement="bottom" title="Twitter"><i class="icon-twitter icon-square icon-32"></i></a></li>
                <li><a href="#" data-placement="bottom" title="Facebook"><i class="icon-facebook icon-square icon-32"></i></a></li>
                <li><a href="#" data-placement="bottom" title="Linkedin"><i class="icon-linkedin icon-square icon-32"></i></a></li>
                <li><a href="#" data-placement="bottom" title="Pinterest"><i class="icon-pinterest icon-square icon-32"></i></a></li>
                <li><a href="#" data-placement="bottom" title="Google plus"><i class="icon-google-plus icon-square icon-32"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="verybottom">
        <div class="container">
          <div class="row">
            <div class="span6">
              <p>
                &copy; FiveWoman - Derechos Reservados
              </p>
            </div>
            <div class="span6">
              <div class="pull-right">
                <div class="credits">
                  Created by <a href="#">FiveWoman</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>


  </div>
  <!-- end wrapper -->
  <a href="#" class="scrollup"><i class="icon-chevron-up icon-square icon-48 active"></i></a>
  <script src="js/jquery-1.8.2.min.js"></script>
  <script src="js/jquery.js"></script>
  <script src="js/raphael-min.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/bootstrap.js"></script>
  <script src="js/google-code-prettify/prettify.js"></script>
  <script src="js/jquery.elastislide.js"></script>
  <script src="js/jquery.prettyPhoto.js"></script>
  <script src="js/jquery.flexslider.js"></script>
  <script src="js/jquery-hover-effect.js"></script>
  <script src="js/animate.js"></script>

  <!-- Template Custom JavaScript File -->
  <script src="js/custom.js"></script>

</body>
</html>
