@extends('layouts.template')
@section('content')
<div class="container">
        <div class="row">
          <div class="span4">
            <aside>
              <div class="widget">
                <h4 class="rheading">Contactenos<span></span></h4>
                <ul>
                  <li><label><strong>FiveWoman Company, Inc : </strong></label>
                    <p>
                      (123) 456-7890 - (123) 555-8890
                    </p>
                  </li>
                  <li><label><strong>Email : </strong></label>
                    <p>
                      bussines@fivewoman.com
                    </p>
                  </li>
                  <li><label><strong>Adress : </strong></label>
                    <p>
                      Residencial Miralvalle, Avenida Eat<br>
                Milan, Italy
                    </p>
                  </li>
                </ul>
              </div>
              <div class="widget">
                <h4 class="rheading">Redes Sociales<span></span></h4>
                <ul class="social-links">
                  <li><a href="#" title="Twitter"><i class="icon-square icon-32 icon-twitter"></i></a></li>
                  <li><a href="#" title="Facebook"><i class="icon-square icon-32 icon-facebook"></i></a></li>
                  <li><a href="#" title="Google plus"><i class="icon-square icon-32 icon-google-plus"></i></a></li>
                  <li><a href="#" title="Linkedin"><i class="icon-square icon-32 icon-linkedin"></i></a></li>
                  <li><a href="#" title="Pinterest"><i class="icon-square icon-32 icon-pinterest"></i></a></li>
                </ul>
              </div>
            </aside>
          </div>
          <div class="span8">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d41065.07280806567!2d9.178994348786274!3d45.45232342647063!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4786c1493f1275e7%3A0x3cffcd13c6740e8d!2zTWlsw6FuLCBJdGFsaWE!5e0!3m2!1ses-419!2ssv!4v1561090214065!5m2!1ses-419!2ssv" width="100%" height="380" frameborder="0" style="border:0" allowfullscreen></iframe>

            <div class="spacer30">
            </div>

            <div id="sendmessage">Tu mensaje ha sido enviado. ¡Gracias!</div>
            <div id="errormessage"></div>
            <form action="{{ route('mail.store') }}" method="post" role="form" class="contactForm">
                 {{csrf_field()}}
              <div class="row">
                <div class="span4 form-group">
                  <input type="text" name="nombre" class="input-block-level" id="nombre" placeholder="Tu Nombre" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                  <div class="validation"></div>
                </div>

                <div class="span4 form-group">
                  <input type="email" class="input-block-level" name="email" id="email" placeholder="Tu correo" data-rule="email" data-msg="Por favor, ingresa un correo valido" />
                  <div class="validation"></div>
                </div>
                <div class="span8 form-group">
                  <input type="text" class="input-block-level" name="asunto" id="subject" placeholder="Asunto" data-rule="minlen:4" data-msg="Por favor, ingresa al menos 8 caracteres en el asunto" />
                  <div class="validation"></div>
                </div>
                <div class="span8 form-group">
                  <textarea class="input-block-level" name="mensaje" rows="5" data-rule="required" data-msg="Por favor, escribenos algo..." placeholder="Mensaje del Correo"></textarea>
                  <div class="validation"></div>
                  <div class="text-center">
                    <button class="btn btn-theme" type="submit">Enviar mensaje</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
@endsection