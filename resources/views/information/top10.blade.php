@extends('layouts.template')
@section('content')
      <div class="container">
        <div class="row">
        <h1>Sitios Turisticos</h1>
            <h2>Top 10</h2>
            <div align="left" style="padding-left: 10%">
            <h4 style="align-content: left" class="span12">Para el 2017, el sitio de noticias e informacion BBC.
                <br>Indico cuales eran las ciudad mas visitadas de America Latina.
                <br> Entre las que se destacan: Playas, Montañas y Sitios Historicos o Precolombinos
                <br> Aca te mostramos cuales son el Top 10.</h4>
                <div style="padding-left: 10%">
                    <ol>Cancún, México</ol> 
                    <ol>Punta Cana, República Dominicana</ol>
                    <ol>Ciudad de México,  México</ol>
                    <ol>Buenos Aires, Argentina</ol>
                    <ol>Lima, Perú</ol>
                    <ol>Santiago, Chile</ol>
                    <ol>Río de Janeiro, Brasil</ol>
                    <ol>Puerto Vallarta, México</ol>
                    <ol>Cataratas del Iguazú, Argentina</ol>
                    <ol>Valparaíso y Viña del Mar, Chile</ol>
                </div>
            </div>
        </div>
        <div class="row">
          <ul class="portfolio-area da-thumbs">
            <li class="portfolio-item" data-id="id-0" data-type="web">
              <div class="span6">
                <div class="thumbnail">
                  <div class="image-wrapp">
                    <img src="{{asset('img/dummies/sites/100.jpg')}}" alt="Portfolio name" title="" />
                    <article class="da-animate da-slideFromRight">
                      <a class="zoom" data-pretty="prettyPhoto" href="{{asset('img/dummies/sites/100.jpg')}}">
                            <i class="icon-zoom-in icon-rounded icon-48 active"></i>
                            </a>
                      <div class="hidden-tablet">
                        <p>
                          Cancún, México
                        </p>
                      </div>
                    </article>
                  </div>
                </div>
              </div>
            </li>
            <li class="portfolio-item" data-id="id-0" data-type="web">
              <div class="span6">
                <div class="thumbnail">
                  <div class="image-wrapp">
                    <img src="{{asset('img/dummies/sites/101.jpg')}}" alt="Portfolio name" title="" />
                    <article class="da-animate da-slideFromRight">
                      <a class="zoom" data-pretty="prettyPhoto" href="{{asset('img/dummies/sites/101.jpg')}}">
                            <i class="icon-zoom-in icon-rounded icon-48 active"></i>
                            </a>
                      <div class="hidden-tablet">
                        <p>
                          Punta Cana, Republica Dominicana
                        </p>
                      </div>
                    </article>
                  </div>
                </div>
              </div>
            </li>
            <li class="portfolio-item" data-id="id-0" data-type="web">
              <div class="span6">
                <div class="thumbnail">
                  <div class="image-wrapp">
                    <img src="{{asset('img/dummies/sites/102.jpg')}}" alt="Portfolio name" title="" />
                    <article class="da-animate da-slideFromRight">
                      <a class="zoom" data-pretty="prettyPhoto" href="{{asset('img/dummies/sites/102.jpg')}}">
                            <i class="icon-zoom-in icon-rounded icon-48 active"></i>
                            </a>
                      <div class="hidden-tablet">
                        <p>
                         Ciudad de México,  México
                        </p>
                      </div>
                    </article>
                  </div>
                </div>
              </div>
            </li>
            <li class="portfolio-item" data-id="id-0" data-type="web">
              <div class="span6">
                <div class="thumbnail">
                  <div class="image-wrapp">
                    <img src="{{asset('img/dummies/sites/103.jpg')}}" alt="Portfolio name" title="" />
                    <article class="da-animate da-slideFromRight">
                      <a class="zoom" data-pretty="prettyPhoto" href="{{asset('img/dummies/sites/103.jpg')}}">
                            <i class="icon-zoom-in icon-rounded icon-48 active"></i>
                            </a>
                      <div class="hidden-tablet">
                        <p>
                         Buenos Aires, Argentina
                        </p>
                      </div>
                    </article>
                  </div>
                </div>
              </div>
            </li>
            <li class="portfolio-item" data-id="id-0" data-type="web">
              <div class="span6">
                <div class="thumbnail">
                  <div class="image-wrapp">
                    <img src="{{asset('img/dummies/sites/104.jpg')}}" alt="Portfolio name" title="" />
                    <article class="da-animate da-slideFromRight">
                      <a class="zoom" data-pretty="prettyPhoto" href="{{asset('img/dummies/sites/104.jpg')}}">
                            <i class="icon-zoom-in icon-rounded icon-48 active"></i>
                            </a>
                      <div class="hidden-tablet">
                        <p>
                        Lima, Perú
                        </p>
                      </div>
                    </article>
                  </div>
                </div>
              </div>
            </li>
            <li class="portfolio-item" data-id="id-0" data-type="web">
              <div class="span6">
                <div class="thumbnail">
                  <div class="image-wrapp">
                    <img src="{{asset('img/dummies/sites/105.jpg')}}" alt="Portfolio name" title="" />
                    <article class="da-animate da-slideFromRight">
                      <a class="zoom" data-pretty="prettyPhoto" href="{{asset('img/dummies/sites/105.jpg')}}">
                            <i class="icon-zoom-in icon-rounded icon-48 active"></i>
                            </a>
                      <div class="hidden-tablet">
                        <p>
                         Santiago, Chile
                        </p>
                      </div>
                    </article>
                  </div>
                </div>
              </div>
            </li>
            <li class="portfolio-item" data-id="id-0" data-type="web">
              <div class="span6">
                <div class="thumbnail">
                  <div class="image-wrapp">
                    <img src="{{asset('img/dummies/sites/106.jpg')}}" alt="Portfolio name" title="" />
                    <article class="da-animate da-slideFromRight">
                      <a class="zoom" data-pretty="prettyPhoto" href="{{asset('img/dummies/sites/106.jpg')}}">
                            <i class="icon-zoom-in icon-rounded icon-48 active"></i>
                            </a>
                      <div class="hidden-tablet">
                        <p>
                         Río de Janeiro, Brasil
                        </p>
                      </div>
                    </article>
                  </div>
                </div>
              </div>
            </li>
            <li class="portfolio-item" data-id="id-0" data-type="web">
              <div class="span6">
                <div class="thumbnail">
                  <div class="image-wrapp">
                    <img src="{{asset('img/dummies/sites/107.jpg')}}" alt="Portfolio name" title="" />
                    <article class="da-animate da-slideFromRight">
                      <a class="zoom" data-pretty="prettyPhoto" href="{{asset('img/dummies/sites/107.jpg')}}">
                            <i class="icon-zoom-in icon-rounded icon-48 active"></i>
                            </a>
                      <div class="hidden-tablet">
                        <p>
                         Puerto Vallarta, México
                        </p>
                      </div>
                    </article>
                  </div>
                </div>
              </div>
            </li>
            <li class="portfolio-item" data-id="id-0" data-type="web">
              <div class="span6">
                <div class="thumbnail">
                  <div class="image-wrapp">
                    <img src="{{asset('img/dummies/sites/108.jpg')}}" alt="Portfolio name" title="" />
                    <article class="da-animate da-slideFromRight">
                      <a class="zoom" data-pretty="prettyPhoto" href="{{asset('img/dummies/sites/108.jpg')}}">
                            <i class="icon-zoom-in icon-rounded icon-48 active"></i>
                            </a>
                      <div class="hidden-tablet">
                        <p>
                         Cataratas del Iguazú, Argentina
                        </p>
                      </div>
                    </article>
                  </div>
                </div>
              </div>
            </li>
            <li class="portfolio-item" data-id="id-0" data-type="web">
              <div class="span6">
                <div class="thumbnail">
                  <div class="image-wrapp">
                    <img src="{{asset('img/dummies/sites/109.jpg')}}" alt="Portfolio name" title="" />
                    <article class="da-animate da-slideFromRight">
                      <a class="zoom" data-pretty="prettyPhoto" href="{{asset('img/dummies/sites/109.jpg')}}">
                            <i class="icon-zoom-in icon-rounded icon-48 active"></i>
                            </a>
                      <div class="hidden-tablet">
                        <p>
                         Valparaíso y Viña del Mar, Chile
                        </p>
                      </div>
                    </article>
                  </div>
                </div>
              </div>
            </li>
          </ul>
        </div>
      </div>
@endsection
