<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Reserva de Vuelos') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Reserva de Vuelos') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        @can('permisos.index')
                         <li class="nav-item">
                             <a class="nav-link" href="{{ route('permisos.index') }}">Permisos</a>
                        </li>
                        @endcan
                       @can('marcas.index')
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('marcas.index') }}">Marcas</a>
                        </li>
                      @endcan
                       @can('user.index')
                        
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('user.index') }}">Usuarios</a>
                        </li>
                       
                       @endcan
                       @can('roles.index')
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('roles.index') }}">Roles</a>
                        </li>
                    @endcan
                    @can('vuelos.index')
                  <li class="dropdown">
                    <a href="{{route('vuelos.index')}}"><i class="icon-globe"></i> Vuelos </a>
                  </li>
                  @endcan
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Login</a></li>
                        <li> <a href="{{ route('register') }}">Register</a></li>
                        <li> <a href="{{ route('show.contacto') }}">Contactanos</a></li> 
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>
        <section id="subintro" style="background-color: #3498db;">
      <div class="container" style="background-color: #3498db;">
        <div class="row" style="background-color: #3498db;">
          <div class="span8" style="background-color: #3498db;">
             <div style="float: right; padding-top: 1%;color: white"><label> {{ \Carbon\Carbon::now(new DateTimeZone('America/El_Salvador'))->format('d/m/Y H:i:s')}}</label></div>
            <p style="padding: 2%">

            </p>
          </div>
        </div>
      </div>
    </section>

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
