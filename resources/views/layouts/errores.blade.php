@section('errores')
@if(count($errors))
<div style="align-content: center">
	<div class="alert alert-danger col-md-offset-1 col-md-6" style="margin-top: 2%; margin-left: -10%;" >
		<button type="button" class="close" data-dismiss="alert">
			&times;
		</button>
		<ul>
			@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
			@endforeach

		</ul>
	</div>
</div>
@endif
@endsection