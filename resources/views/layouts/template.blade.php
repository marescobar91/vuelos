<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>Reserva de Vuelos</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="FiveWoman">
  <!-- styles -->
  <link href="{{asset('css/bootstrap.css')}}" rel="stylesheet">
  <link href="{{asset('css/bootstrap-responsive.css')}}" rel="stylesheet">
  <link href="{{asset('css/prettyPhoto.css')}}" rel="stylesheet">
  <link href="{{asset('css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet">
  <link href="{{asset('js/google-code-prettify/prettify.css')}}" rel="stylesheet">
  <link href="{{asset('css/flexslider.css')}}" rel="stylesheet">
  <link href="{{asset('css/style.css')}}" rel="stylesheet">
  <link href="{{asset('color/default.css')}}" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Droid+Serif:400,600,400italic|Open+Sans:400,600,700" rel="stylesheet">
  <link href="{{asset('dataTables.min.css')}}">
  <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

  <!-- fav and touch icons -->
  <link rel="shortcut icon" href="{{asset('ico/favicon.ico')}}">
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="ico/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="ico/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="ico/apple-touch-icon-57-precomposed.png">
  <style type="text/css">
    .contaner{
      width: 1100;
    }
  </style>
  <!-- =======================================================
    Theme Name: Sistema de Vuelos Lumia 
    Author: Five Woman
  ======================================================= -->

</head>


<body>
  <div id="wrapper">
    <header>
      <!-- Navbar
    ================================================== -->
      <div class="navbar navbar-static-top">
        <div class="navbar-inner" >
          <div class="container" style="width: 100%; margin-left: -13%">
            <!-- top menu -->
            <div class="navigation">
              <nav>
                <ul class="nav topnav"> 
                  <li class="active">
                    <a href="{{ url('/') }}"><i class="icon-home"></i> Home </a>
                  </li>
                  <li class="dropdown">
                    <a href="{{ route('show.contacto') }}"><i class="icon-envelope"></i> Contacto </a>
                  </li>
                  @can('user.index')
                  <li class="dropdown">
                    <a href="#"><i class="icon-user"></i> Gestiones <i class="icon-angle-down"></i></a>
                    @endcan
                    
                    <ul class="dropdown-menu">
                    @can('user.index')  <li><a href="{{route('user.index')}}">Usuarios</a></li>@endcan
                    @can('roles.index')<li><a href="{{route('roles.index')}}">Roles</a></li> @endcan
                    @can('permisos.index')<li><a href="{{route('permisos.index')}}">Permisos</a></li> @endcan 
                    </ul>
                  </li> 
                  @can('aeropuertos.index')
                  <li class="dropdown">
                    <a href="{{route('aeropuertos.index')}}"><i class="icon-cloud"></i> Aeropuerto </a>
                  </li>
                  @endcan
                  @can('aviones.index')
                  <li class="dropdown">
                    <a href="{{route('aviones.index')}}"><i class="icon-plane"></i> Avión <i class="icon-angle-down"></i></a>
                    <ul class="dropdown-menu">
                      @can('marcas.index')<li><a href="{{route('marcas.index')}}">Marcas</a></li>@endcan
                      @can('modelos.index')<li><a href="{{route('modelos.index')}}">Modelo</a></li>@endcan
                      @can('clases.index')<li><a href="{{route('clases.index')}}">Clases</a></li>@endcan
                    </ul>
                  </li>
                  @endcan
                  @can('paises.index')
                  <li class="dropdown">
                    <a href="#"><i class="icon-map-marker"></i> Geografia <i class="icon-angle-down"></i> </a>
                    @endcan
                    <ul class="dropdown-menu">
                      @can('paises.index')<li><a href="{{route('paises.index')}}">País</a></li>@endcan
                      @can('departamentos.index')<li><a href="{{route('departamentos.index')}}">Departamento</a></li>@endcan
                      @can('municipios.index')<li><a href="{{route('municipios.index')}}">Municipio</a></li>@endcan
                      @can('ciudades.index')<li><a href="{{route('ciudades.index')}}">Ciudad</a></li>@endcan
                      @can('zonas.index')<li><a href="{{route('zonas.index')}}">Codigo de Area</a></li>@endcan
                    </ul>
                  </li>
                  @can('aerolineas.index')
                  <li class="dropdown">
                    <a href="{{route('aerolineas.index')}}"><i class="icon-map-marker"></i> Aerolineas</a>
                  </li>
                  @endcan
                  @can('aerolineas.index')
                  <li class="dropdown">
                    <a href="{{route('vuelos.index')}}"><i class="icon-globe"></i> Vuelos <i class="icon-angle-down"></i> </a>
                  </li>
                  @endcan
                  <li class="dropdown"><i class="icon-users"></i>
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Iniciar Sesión</a></li>
                        <li> <a href="{{ route('register') }}">Registrar</a></li> 
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="icon-user"></i>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li class="dropdown">
                                      <a href="{{ route('user.password') }}">
                                           Cambiar Contraseña
                                        </a></li>
                                        <li class="dropdown">
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Cerrar Sesión
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                  </li>
                </ul>

                <ul class="nav topnav">
                </ul>
              </nav>
            </div>
            <!-- end menu -->
            <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                       
                    </ul>
            


          </div>
        </div>

      </div>
    </header>
    @yield('slider')
    <!-- Subintro
================================================== -->
    <section id="subintro">
      <div class="container">
        <div class="row">
          <div class="span8">
            
          </div>
          <div class="span4">

            <div class="search">
              <div style="float: right; padding-top: 1%;color: white"><label> {{ \Carbon\Carbon::now(new DateTimeZone('America/El_Salvador'))->format('d/m/Y H:i:s')}}</label></div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section id="maincontent">
      <div class="container">
        <div class="row">
          <div class="span12">
            <div class="centered">
            
              @include('flash::message')
              @yield('content')
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Footer ================================================== -->
    <footer class="footer">
      <div class="container">
        <div class="row">
          <div class="span2"></div>
          <div class="span6">
            <div class="widget">
            <h5 align="center">Aerolineas</h5>
           <!--     <div class="flickr-badge">
                <script type="text/javascript" src="http://www.flickr.com/badge_code_v2.gne?count=9&amp;display=random&amp;size=s&amp;layout=x&amp;source=user&amp;user=34178660@N03"></script>

              </div>-->
              <table cellpadding="10%">
              <tr>
                <td class="span2"><a href="https://aeromexico.com/"><img src="{{asset('img\brands\img1.png')}}"  style="background-color: white;" /></a></td>
                <td class="span3"><a href="https://www.united.com/"><img src="{{asset('img\brands\img2.png')}}"  style="background-color: white;" /></a></td>
              </tr>
              <tr>
                <td class="span2"><a href="https://www.aireuropa.com/"><img src="{{asset('img\brands\img6.png')}}"  style="background-color: white;" /></a></td>
                <td class="span3"><a href="https://www.copaair.com/"><img src="{{asset('img\brands\img3.png')}}"  style="background-color: white;" /></a></td>
              </tr>
              <tr>
               <td class="span2"><a href="https://www.volaris.com/"><img src="{{asset('img\brands\img7.png')}}"  style="background-color: white;" /></a></td>
                <td class="span3"><a href="https://www.avianca.com/"><img src="{{asset('img\brands\img5.png')}}"  style="background-color: white;" /></a></td>
              </tr>
            </table>
              <div class="clear"></div>
            </div>
          </div>
          <div class="span4">
            <div class="widget">
              <a href="{{asset('img\fivewoman.png')}}"><img src="{{asset('img\fivewoman.png')}}" style="width: 25%; padding-left: 10%"></a>
              <h5>Encuentranos en: </h5>
              <address>
                <i class="icon-home"></i> <strong>FiveWoman Company, inc</strong><br>
                Residencial Miralvalle, Avenida Eat<br>
                Milan, Italy
                </address>
              <p>
                <i class="icon-phone"></i> (123) 456-7890 - (123) 555-8890<br>
                <i class="icon-envelope-alt"></i> bussines@fivewoman.com
              </p>
            </div>
            <div class="widget">
              <ul class="social">
                <li><a href="#" data-placement="bottom" title="Twitter"><i class="icon-twitter icon-square icon-32"></i></a></li>
                <li><a href="#" data-placement="bottom" title="Facebook"><i class="icon-facebook icon-square icon-32"></i></a></li>
                <li><a href="#" data-placement="bottom" title="Linkedin"><i class="icon-linkedin icon-square icon-32"></i></a></li>
                <li><a href="#" data-placement="bottom" title="Pinterest"><i class="icon-pinterest icon-square icon-32"></i></a></li>
                <li><a href="#" data-placement="bottom" title="Google plus"><i class="icon-google-plus icon-square icon-32"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="verybottom">
        <div class="container">
          <div class="row">
            <div class="span6">
              <p>
                &copy; FiveWoman - All right reserved
              </p>
            </div>
            <div class="span6">
              <div class="pull-right">
                <div class="credits">
                  Created by <a href="#">FiveWoman</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>

  </div> 
  <!-- end wrapper -->
  <a href="#" class="scrollup"><i class="icon-chevron-up icon-square icon-48 active"></i></a>
  <script src="{{asset('js/jquery.js')}}"></script>
  
  <script src="{{asset('js/jquery-1.8.2.min.js')}}"></script>
  <script src="{{asset('js/raphael-min.js')}}"></script>
  <script src="{{asset('js/jquery.easing.1.3.js')}}"></script>
  <script src="{{asset('js/bootstrap.js')}}"></script>
  <script src="{{asset('js/google-code-prettify/prettify.js')}}"></script>
  <script src="{{asset('js/jquery.elastislide.js')}}"></script>
  <script src="{{asset('js/jquery.prettyPhoto.js')}}"></script>
  <script src="{{asset('js/jquery.flexslider.js')}}"></script>
  <script src="{{asset('js/jquery-hover-effect.js')}}"></script>
  <script src="{{asset('js/animate.js')}}"></script>
  <script src="{{asset('js/bootstrap-datetimepicker.min.js')}}"></script>
  <script src="{{asset('js/locates/bootstrap-datetimepicker.es.js')}}"></script>

 


  <!-- Template Custom JavaScript File -->
  <script src="{{asset('js/custom.js')}}"></script>
  
  <script src="{{asset('js/dataTables.min.js')}}"></script>
  <script>$(document).ready( function () {
    $('#myTable').DataTable();
} );
  </script>

   @yield('js')
</body>

</html>
