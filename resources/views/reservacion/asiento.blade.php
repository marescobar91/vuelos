@extends('layouts.template')
@section('content')
<div class="span8" style="margin-left: 17%">
	<h1>Seleccionar Asientos</h1>
	<div class="row">
		<form action="{{route('itinerario.asientos')}}" method="post" role="form" class="contactForm">
			 {{ csrf_field() }}

<div class="form-group">
                  <input type="hidden" min="1" value="{{$iti1}}" type="number"  name="iti1" class="input-block-level" id="iti1"/>
                </div>





		<div class="span4 form-control" style="float: left;">
			<label class="span3">Asiento de Vuelo de Ida:</label> 
			<!--espacio pal for-->
			@foreach($cla as $clase)
		<div class="span2 form-control"> 
			<label>
				{{$clase->nombre_clase}}
			</label>
		</div>
		@endforeach
		<div class="span21">
			@foreach($asiento1 as $asiento)
			<label>
			<input type="checkbox" value="{{$asiento->id}}" name="ida[]" class="form-control">
			<strong>{{$asiento->asiento}}</strong>
			</label>
			@endforeach
			</div>
			<!--EndFor-->

		</div>
		
		<div class="span4 form-control " style="float: right;">
			<label class="span3">Asiento de Vuelo de Regreso:</label> 

			<div class="form-group">
                  <input type="hidden" min="1" value="{{$iti2}}" type="number"  name="iti2" class="input-block-level" id="iti2"/>
                </div>

			<!--espacio pal for-->
		@foreach($cla as $clase)
		<div class="span2 form-control"> 
			<label>
				{{$clase->nombre_clase}}
			</label>
		</div>
		@endforeach
		<div class="span1">
			@foreach($asiento2 as $asiento)
			<label>
			<input type="checkbox" value="{{$asiento->id}}" name="regreso[]" class="form-control">
			<strong>{{$asiento->asiento}}</strong>
			</label>
			@endforeach
			</div>
			<!--EndFor-->
		</div>
		<div class="span8 form-control" style="padding-top: 5%">
			<div class="text-center">
                    <button name="guardar" class="btn btn-primary" type="submit">Guardar</button>
                    <a href="{{route('itinerario.asientos')}}" class="btn btn-secondary" style="float:right 5%;"> <span>
                            <b>Cancelar</b></span></a>
		</div>
		</form>
	</div>
</div>
@endsection