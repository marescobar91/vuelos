<!DOCTYPE html>

<head>
<title>Vuelos-Boleto</title>
 <link href="style.css" rel="stylesheet">
</head>
<body>
    <div id="wrapper">
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="container" style="margin-left: 30px">
                    <div class="span9">
                        <div class="table-title" style="background-color: #fff; margin-bottom: 1%">

                            <div class="row">
                                <div class="col-sm-2" style="margin: 8px 0px 3px 5px">
                                    <img src="{{public_path().'img/logo.png'}}" width="100px" align="left">
                                </div>
                                <div class="span7" style="text-align: center;" align="center">

                                    <h3 style="color: #1a1a1a;text-align: center;"><b>Boleto</b></h3>
                                    <h4 style="color: #1a1a1a;text-align: center; padding-top: -20px"><b>Sistema de Vuelo</b></h4>
                                    <h4 style="color: #1a1a1a;text-align: center; padding-top: 10px;padding-bottom: 20px"><b>Detalles de Vuelo</b></h4>
                                </div>

                            </div>
                            <hr style="display: block;margin-top: -5px;margin-bottom: 0.5em;margin-left: 5px;margin-right: auto;border-style: inset;border-width: 0.75px; width: 95%; border-color: #1a1a1a"/>
                            <br/>

                            <div class="row">
                                
                                <div style="max-height: 700px;">
             <div>
                <div align="center">
                  
                    <h3 align="center" > <b>Detalles de Reservacion</b></h3>
                    <h4> <b>ID de Reservacion:  {{ $itine }} </b></h4>
                    @foreach($boleto1 as $boleto)
                    <h4> <b> Total de Pago: {{ $boleto->pago }}</b>  
                    @endforeach                 
                    </h4></div>
              </div>
              </div>
            <h3 align="center" > <b>Salida</b></h3>
            <div >
               <table class="table table-striped" style="margin-left: 150px">
                    <tbody>

                       <tr>
                        <th colspan="4" style="text-align: center">Vuelo de Ida</th>
                        
                      </tr>
                     @foreach($boleto1 as $boleto)
                      <tr>
                        <td width="25%">Ciudad de Salida: {{ $boleto->origen_nombre }} , Ciudad de Llegada: {{ $boleto->destino_nombre }}</td></tr>
                        <tr>
                        <td width="25%"> Codigo de Vuelo: {{ $boleto->codigo_vuelo }}</td>
                        <td colspan="2"> {{date('d-m-Y', strtotime($boleto->fecha_salida))}} {{date('g:ia', strtotime($boleto->fecha_salida))}}</td>
                      </tr>
                      @endforeach
                      @foreach($boleto1 as $boleto)
                      <tr>
                        <td >Nombre Completo: {{ $boleto->nombre_cliente }} {{ $boleto->apellido_cliente }}</td>
                      </tr>
                      @endforeach
                      @foreach($persona1 as $persona1)
                      <tr>
                        <td> Acompañante{{ $persona1->nombre }} {{ $persona1->apellido }}</td>
                      </tr>
                      @endforeach
                      @foreach($silla1 as $silla1)
                      <tr>
                        <td>{{ $silla1->asiento }}</td>
                      </tr>
                      @endforeach
                      <tr>
                          <td style="padding-top: 3%;" colspan="4"></td>
                        </tr>
                    </tbody>
                    </table>
                </div>
                <div style="max-height: 700px;">
             <div>
                <div align="center">
                
                    <h3 align="center" > <b>Detalles de Reservacion</b></h3>
                     <h4> <b>ID de Reservacion:  {{ $itine1 }} </b></h4>
                    @foreach($boleto2 as $boleto)
                    <h4> <b> Total de Pago: {{ $boleto->pago }}</b>  
                    @endforeach                 
                    </h4></div>
              </div>
              </div>

            <h3 align="center" > <b>Regreso</b></h3>
            <div >
               <table class="table table-striped" style="margin-left: 150px">
                    <tbody>

                       <tr>
                        <th colspan="4" style="text-align: center">Vuelo de Regreso</th>
                        
                      </tr>
                     
                      @foreach($boleto2 as $boleto)
                      <tr>
                        <td width="25%">Ciudad de Salida: {{ $boleto->origen_nombre }} , Ciudad de Llegada: {{ $boleto->destino_nombre }}</td></tr>
                        <tr>
                        <td width="25%"> Codigo de Vuelo: {{ $boleto->codigo_vuelo }}</td>
                        <td colspan="2"> {{date('d-m-Y', strtotime($boleto->fecha_salida))}} {{date('g:ia', strtotime($boleto->fecha_salida))}}</td>
                      </tr>
                      @endforeach
                      @foreach($boleto2 as $boleto)
                      <tr>
                        <td >Nombre Completo : {{ $boleto->nombre_cliente }} {{ $boleto->apellido_cliente }}</td>
                      </tr>
                      @endforeach
                      @foreach($persona2 as $persona1)
                      <tr>
                        <td> Acompañante{{ $persona1->nombre }} {{ $persona1->apellido }}</td>
                      </tr>
                      @endforeach
                      @foreach($silla2 as $silla1)
                      <tr>
                        <td>{{ $silla1->asiento }}</td>
                      </tr>
                      @endforeach
                      <tr>
                          <td style="padding-top: 3%;" colspan="4"></td>
                        </tr>
                    </tbody>
                    </table>
                </div>
                           </div>
                        </div>

                        <form>
                          
                          
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
