@extends('layouts.template')
@section('content')
<div class="span12">
                <h1 class="rheading">Cliente<span></span></h1>
                <div class="tabbable tabs-top">
                  <ul class="nav nav-tabs">
                    <li class="active"><a href="#one" data-toggle="tab"><i class="icon-briefcase"></i>Natural</a></li>
                    <li><a href="#two" data-toggle="tab"><i class="icon-briefcase"></i>Empresa</a></li>
                  </ul>
                  <div class="tab-content">
                    <div class="tab-pane active span12" id="one"> <!--Natural-->
                      <div class="row">

          <form action="{{route('itinerario.cliente')}}" method="post" role="form" class="contactForm">
             {{ csrf_field() }}


<div class="form-group">
                  <input type="hidden" min="1" value="{{$var1}}" type="number"  name="vuelo" class="input-block-level" id="vuelo"/>
                </div>
                <div class="form-group">
                  <input type="hidden" min="1" value="{{$var2}}" type="number"  name="vuelo2" class="input-block-level" id="vuelo2"/>
                </div>

                <div class="form-group">
                  <input type="hidden" min="1" value="{{$clase}}" type="number"  name="clase" class="input-block-level" id="clase"   />
                </div>

                <div class="form-group">
                  <input type="hidden" min="1" value="{{$sumar}}" type="number"  name="sumar" class="input-block-level" id="sumar"   />
                </div>


            

                      	<div class="span4 form-group">
                  <input type="text" name="nombre_cliente" class="input-block-level" id="nombre_cliente" required placeholder="Nombre" />
                  <div class="validation"></div>       
		                </div>
		                <div class="span4 form-group">
                  <input type="text" name="apellido_cliente" class="input-block-level" id="apellido_cliente" required placeholder="Apellido" />
                  <div class="validation"></div>       
		                </div>

		                <div class="span3">
                    <select name="municipio" class="form-group" required>
                       <option value="">Municipio:</option>
                        @foreach($municipio as $muni)
                          <option value="{{$muni->id}}">{{$muni->nombre_municipio}} </option>
                       @endforeach
                        
                    </select>
                    <div class="validation"></div>
                </div>

                   
		              
		                <h6 class="span12">Direccion</h6>
		                <div class="span4 form-group">
                  <input type="text" name="calle" class="input-block-level" id="calle" required placeholder="Calle o Avenida" />
                  <div class="validation"></div>       
		                </div>
		               
		                <div class="span3 form-group">
                  <input type="text" name="pasaje" class="input-block-level" id="pasaje"  placeholder="Pasaje" />
                  <div class="validation"></div>       
		                </div>
		                <div class="span4 form-group">
                  <input type="text" name="colonia" class="input-block-level" id="colonia" required placeholder="Colonia o Barrio" />
                  <div class="validation"></div>       
		                </div>
		              
		                <div class="span3 form-group">
                  <input type="text" name="telefono_movil" class="input-block-level" id="telefono_movil" required placeholder="Telefono Movil" />
                  <div class="validation"></div>       
		                </div>
		                <div class="span3 form-group">
                  <input type="text" name="telefono_fijo" class="input-block-level" id="telefono_fijo" required placeholder="Telefono Fijo" />
                  <div class="validation"></div>       
		                </div>
		                <div class="span3 form-group">
                  <input type="text" name="num_viejaroFrec" class="input-block-level" id="num_viejaroFrec" required placeholder="Numero de Viajero Frecuente" />
                  <div class="validation"></div>       
		                </div>
		                <div class="span2 form-group">
                  <input type="number" min="1" max="3" name="maleta" class="input-block-level" id="maleta" required placeholder="# Maletas" />
                  <div class="validation"></div>       
                    </div>
		                <div class="span2 form-group">
                  <label>Fecha de Nacimiento:</label>    
		                </div>
		                <div class="span4 form-group">
                  <input type="date" name="fecha_nacimiento" class="input-block-level" id="fecha_nacimiento" required placeholder="Fecha nacimiento" />
                  <div class="validation"></div>       
		                </div>
		                <div class="span3">
                    <select name="genero" class="form-group" required>
                       <option value="">Genero:</option>
                      
                          <option value="F">Femenino </option>
                          <option value="M">Masculino</option>                       
                    </select>
                    <div class="validation"></div>
                </div>

                 <div class="span2 form-group">
                  <input type="text" name="estado_civil" class="input-block-level" id="estado_civil" required placeholder="Estado Civil" />
                  <div class="validation"></div>       
                    </div>
		               
		                <div class="span5 form-group">
                  <input type="text" name="tipo_documento" class="input-block-level" id="tipo_documento" required placeholder="Tipo de Documento" />
                  <div class="validation"></div>       
		                </div>
		                <div class="span6 form-group">
                  <input type="text" name="numero_documento" class="input-block-level" id="numero_documento" required placeholder="Numero de Documento" />
                  <div class="validation"></div>       
		                </div>
		                 
		        <div class="span8" style="margin-left: 15%">
		                 		<h6 >Acompañantes</h6>
		                 	<div class="accordion" id="accordion2">
                  <div class="accordion-group">
                    <div class="accordion-heading">
                      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
									<i class="icon-plus"></i> Acompañante #1 </a>
                    </div>
                    <div id="collapseOne" class="accordion-body collapse">
                      <div class="accordion-inner">
                        <div class="span3 form-group" style="margin-left: 11%">
                  <input type="text" name="nombre1" class="input-block-level" id="nombre1" placeholder="Nombre" />
                  <div class="validation"></div>       
		                </div>
		                <div class="span3 form-group">
                  <input type="text" name="apellido1" class="input-block-level" id="apellido1" placeholder="Apellido" />
                  <div class="validation"></div>       
		                </div>
                      </div>
                    </div>
                  </div>
                  <div class="accordion-group">
                    <div class="accordion-heading">
                      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
									<i class="icon-plus"></i> Acompañante #2 </a>
                    </div>
                    <div id="collapseTwo" class="accordion-body collapse">
                      <div class="accordion-inner">
                        <div class="span3 form-group" style="margin-left: 11%">
                  <input type="text" name="nombre2" class="input-block-level" id="nombre2"  placeholder="Nombre" />
                  <div class="validation"></div>       
		                </div>
		                <div class="span3 form-group">
                  <input type="text" name="apellido2" class="input-block-level" id="apellido2" placeholder="Apellido" />
                  <div class="validation"></div>       
		                </div>
                      </div>
                    </div>
                  </div>
                  <div class="accordion-group">
                    <div class="accordion-heading">
                      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
									<i class="icon-plus"></i> Acompañante #3 </a>
                    </div>
                    <div id="collapseThree" class="accordion-body collapse">
                      <div class="accordion-inner">
                      <div class="span3 form-group" style="margin-left: 11%">
                  <input type="text" name="nombre3" class="input-block-level" id="nombre3"  placeholder="Nombre" />
                  <div class="validation"></div>       
		                </div>
		                <div class="span3 form-group">
                  <input type="text" name="apellido3" class="input-block-level" id="apellido3" placeholder="Apellido" />
                  <div class="validation"></div>       
		                </div>
                      </div>
                    </div>
                  </div>
                  <div class="accordion-group">
                    <div class="accordion-heading">
                      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseFour">
                  <i class="icon-plus"></i> Acompañante #4 </a>
                    </div>
                    <div id="collapseFour" class="accordion-body collapse">
                      <div class="accordion-inner">
                      <div class="span3 form-group" style="margin-left: 11%">
                  <input type="text" name="nombre4" class="input-block-level" id="nombre4"  placeholder="Nombre" />
                  <div class="validation"></div>       
                    </div>
                    <div class="span3 form-group">
                  <input type="text" name="apellido4" class="input-block-level" id="apellido4" placeholder="Apellido" />
                  <div class="validation"></div>       
                    </div>
                      </div>
                    </div>
                  </div>
                </div>
		     </div>
		     <div class="span11 form-group">
                  <div class="text-center" style="padding-top: 1%">
                    <button class="btn btn-primary" type="submit">Guardar</button>
                    <a href="{{route('itinerario.filtro')}}" class="btn btn-secondary" style="float:right 5%;"> <span>
                            <b>Cancelar</b></span></a>


                  </div>
                </div>
		           	 </form>
                      </div>
                    </div><!--Fin Natural-->



                    <div class="tab-pane span12" id="two">
                      <!--Empresa-->
                      <div class="row">
        <form action="{{route('itinerario.cliente')}}" method="post" role="form" class="contactForm">
          {{ csrf_field() }}

          <div class="form-group">
                  <input type="hidden" min="1" value="{{$var1}}" type="number"  name="vuelo" class="input-block-level" id="vuelo"   />
                </div>
                <div class="form-group">
                  <input type="hidden" min="1" value="{{$var2}}" type="number"  name="vuelo2" class="input-block-level" id="vuelo2"   />
                </div>

                <div class="form-group">
                  <input type="hidden" min="1" value="{{$clase}}" type="number"  name="clase" class="input-block-level" id="clase"   />
                </div>

                 <div class="form-group">
                  <input type="hidden" min="1" value="{{$sumar}}" type="number"  name="sumar" class="input-block-level" id="sumar"   />
                </div>

               



                      	<div class="span4 form-group">
                  <input type="text" name="nombre_cliente" class="input-block-level" id="nombre_cliente" required placeholder="Nombre" />
                  <div class="validation"></div>       
		                </div>
		                <div class="span4 form-group">
                  <input type="text" name="apellido_cliente" class="input-block-level" id="apellido_cliente" required placeholder="Apellido" />
                  <div class="validation"></div>       
		                </div>

                    <div class="span3">
                    <select name="municipio" class="form-group" required>
                       <option value="">Municipio:</option>
                        @foreach($municipio as $muni)
                          <option value="{{$muni->id}}">{{$muni->nombre_municipio}} </option>
                       @endforeach
                        
                    </select>
                    <div class="validation"></div>
                </div>
		               
		               

		              
		                <h6 class="span12">Direccion</h6>
		                <div class="span4 form-group">
                  <input type="text" name="calle" class="input-block-level" id="calle" required placeholder="Calle o Avenida" />
                  <div class="validation"></div>       
		                </div>
		                
		                <div class="span3 form-group">
                  <input type="text" name="pasaje" class="input-block-level" id="pasaje"  placeholder="Pasaje" />
                  <div class="validation"></div>       
		                </div>

		                <div class="span4 form-group">
                  <input type="text" name="colonia" class="input-block-level" id="colonia" required placeholder="Colonia o Barrio" />
                  <div class="validation"></div>       
		                </div>
		              
		                <div class="span3 form-group">
                  <input type="number" name="telefono_movil" class="input-block-level" id="telefono_movil" required placeholder="Telefono Movil" />
                  <div class="validation"></div>       
		                </div>
		                <div class="span3 form-group">
                  <input type="number" name="telefono_fijo" class="input-block-level" id="telefono_fijo" required placeholder="Telefono Fijo" />
                  <div class="validation"></div>       
		                </div>
		                <div class="span3 form-group">
                  <input type="text" name="num_viejaroFrec" class="input-block-level" id="num_viejaroFrec" required placeholder="Numero de Viajero Frecuente" />
                  <div class="validation"></div>       
		                </div>
		                <div class="span2 form-group">
                  <input type="number" min="1" max="3" name="maleta" class="input-block-level" id="maleta" required placeholder="# Maletas" />
                  <div class="validation"></div>       
		                </div>

		                <div class="span3 form-group">
                  <input type="text" name="nic" class="input-block-level" id="nic" required placeholder="NIC" />
                  <div class="validation"></div>       
		                </div>
		                <div class="span3 form-group">
                  <input type="text" name="nit" class="input-block-level" id="nit" required placeholder="NIT" />
                  <div class="validation"></div>       
		                </div>
		                <div class="span5 form-group">
                  <input type="text" name="nombre_empresa" class="input-block-level" id="nombre_empresa" required placeholder="Nombre Empresa" />
                  <div class="validation"></div>       
		                </div>
		                 
		        <div class="span8" style="margin-left: 15%">
		                 		<h6 >Acompañantes</h6>
		                 	<div class="accordion" id="accordion2">
                  <div class="accordion-group">
                    <div class="accordion-heading">
                      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseFive">
									<i class="icon-plus"></i> Acompañante #1 </a>
                    </div>
                    <div id="collapseFive" class="accordion-body collapse">
                      <div class="accordion-inner">
                        <div class="span3 form-group" style="margin-left: 11%">
                  <input type="text" name="nombre1" class="input-block-level" id="nombre1"  placeholder="Nombre" />
                  <div class="validation"></div>       
		                </div>
		                <div class="span3 form-group">
                  <input type="text" name="apellido1" class="input-block-level" id="apellido1"  placeholder="Apellido" />
                  <div class="validation"></div>       
		                </div>
                      </div>
                    </div>
                  </div>
                  <div class="accordion-group">
                    <div class="accordion-heading">
                      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseSix">
									<i class="icon-plus"></i> Acompañante #2 </a>
                    </div>
                    <div id="collapseSix" class="accordion-body collapse">
                      <div class="accordion-inner">
                      <div class="span3 form-group" style="margin-left: 11%">
                  <input type="text" name="nombre2" class="input-block-level" id="nombre2"  placeholder="Nombre" />
                  <div class="validation"></div>       
		                </div>
		                <div class="span3 form-group">
                  <input type="text" name="apellido2" class="input-block-level" id="apellido2"  placeholder="Apellido" />
                  <div class="validation"></div>       
		                </div>
                      </div>
                    </div>
                  </div>
                  <div class="accordion-group">
                    <div class="accordion-heading">
                      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseSeven">
                  <i class="icon-plus"></i> Acompañante #3 </a>
                    </div>
                    <div id="collapseSeven" class="accordion-body collapse">
                      <div class="accordion-inner">
                      <div class="span3 form-group" style="margin-left: 11%">
                  <input type="text" name="nombre3" class="input-block-level" id="nombre3"  placeholder="Nombre" />
                  <div class="validation"></div>       
                    </div>
                    <div class="span3 form-group">
                  <input type="text" name="apellido3" class="input-block-level" id="apellido3"  placeholder="Apellido" />
                  <div class="validation"></div>       
                    </div>
                      </div>
                    </div>
                  </div>
                  <div class="accordion-group">
                    <div class="accordion-heading">
                      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseEight">
                  <i class="icon-plus"></i> Acompañante #4 </a>
                    </div>
                    <div id="collapseEight" class="accordion-body collapse">
                      <div class="accordion-inner">
                      <div class="span3 form-group" style="margin-left: 11%">
                  <input type="text" name="nombre4" class="input-block-level" id="nombre4"  placeholder="Nombre" />
                  <div class="validation"></div>       
                    </div>
                    <div class="span3 form-group">
                  <input type="text" name="apellido4" class="input-block-level" id="apellido4"  placeholder="Apellido" />
                  <div class="validation"></div>       
                    </div>
                      </div>
                    </div>
                  </div>
                </div>
		     </div>
		     <div class="span11 form-group">
                  <div class="text-center" style="padding-top: 1%">
                    <button class="btn btn-primary" type="submit">Guardar</button>
                    <a href="{{route('itinerario.filtro')}}"class="btn btn-secondary" style="float:right 5%;"> <span>
                            <b>Cancelar</b></span></a>
                  </div>
                </div>
		           	 </form>
                      </div>
                    </div>
                  </div>
                </div>

@endsection