@extends('layouts.template')
@section('content')
<div class="span12">
	<h2 style="padding-bottom: 2%">Precio de sus Vuelos</h2>
	<div class="row">
		<div class="span8">
       <div class="form-group">
                  

			<table class="table table-striped">
                      <thead>
                     
                    </thead>
                    <tbody>

                       <tr>
                        <th colspan="4" style="text-align: center">Vuelo de Ida</th>
                        
                      </tr>
                      @foreach($compra1 as $compra)
                      <tr>
                        <td width="25%">Hora de Salida:</td>
                        <td width="25%">{{ $compra->fecha_salida }}</td>
                        <td width="25%">Hora de llegada:</td>
                        <td width="25%">{{ $compra->fecha_llegada }}</td>
                      </tr>
                      <tr>
                        <td >Duracion:</td>
                        <td>{{ $compra->tiempo_vuelo}}</td>
                        <td>Clase:</td>
                        @foreach($clases as $clases)
                        <td>{{ $clases->nombre_clase }}</td>
                        @endforeach
                      </tr>
                      <tr>
                        <td>Aerolinea :</td>
                        <td>{{ $compra->nombre_corto }}</td>
                        <td>Precio:</td>
                        <td>{{ $compra->costo }}</td>
                      </tr>
                      @endforeach
                      <tr>
                          <td style="padding-top: 3%;" colspan="4"></td>
                        </tr>
                    </tbody>
                     
                    </table>
		</div>
  </div>
		<div class="span4">
			<label style="padding-top: 15%"> Precio del Vuelo de Ida</label>
      @foreach($compra1 as $compra)
			<label> $ {{ $compra->costo }}</label>
      @endforeach
		</div>
  

    <div class="span8" style="padding-top: 5%">
			<table class="table table-striped">
                      <thead>
                      
                    </thead>
                    <tbody>
                     

                       <tr>
                        <th colspan="4" style="text-align: center">Vuelo Regreso</th>
                        
                      </tr>
                      @foreach($compra2 as $compra2)
                      <tr>
                        <td width="25%">Hora de Salida:</td>
                        <td width="25%">{{ $compra2->fecha_salida }}</td>
                        <td width="25%">Hora de llegada:</td>
                        <td width="25%">{{ $compra2->fecha_llegada }}</td>
                      </tr>
                      <tr>
                        <td >Duracion:</td>
                        <td>{{ $compra2->tiempo_vuelo }}</td>
                        <td>Clase:</td>
                       @foreach($cla as $cla)
                        <td>{{ $cla->nombre_clase }}</td>
                        @endforeach
                      </tr>
                      <tr>
                        <td>Aerolinea :</td>
                        <td>{{ $compra2->nombre_corto }}</td>
                        <td>Precio:</td>
                        <td>{{ $compra2->costo }}</td>
                      </tr>
                      @endforeach
                      <tr>
                          <td style="padding-top: 3%;" colspan="4"></td>
                        </tr>
                    </tbody>
                         
                    </table>
		</div>
		<div class="span4">
			<label style="padding-top: 30%"> Precio del Vuelo de Regreso</label>
		  @foreach($compra3 as $compra3)
      <label> $ {{ $compra3->costo }}</label>
      @endforeach
		</div>
</div>
		<div class="span5" style="margin-left: 64%">
			<div style="background-color: black; border: solid;"></div>
			<h4 style="padding-top: 3%;">Total <b>$ {{ $total }}</b></h4>

		</div>


    
	</div>
          <form action="{{route('itinerario.tarifas')}}" method="post" role="form" class="contactForm">
             {{ csrf_field() }}
             <input type="hidden" min="1" value="{{$iti1}}" type="number"  name="iti1" class="input-block-level" id="iti1"/>
                </div>
                <div class="form-group">
                  <input type="hidden" min="1" value="{{$clase}}" type="number"  name="clase" class="input-block-level" id="clase"/>
                </div>

                 <div class="form-group">
                  <input type="hidden" min="1" value="{{$var1}}" type="number"  name="var1" class="input-block-level" id="var1"   />
                </div>

                <div class="form-group">
                  <input type="hidden" min="1" value="{{$var2}}" type="number"  name="var2" class="input-block-level" id="var2"   />
                </div>

                 <div class="form-group">
                  <input type="hidden" min="1" value="{{$acompañantes}}" type="number"  name="sumar" class="input-block-level" id="sumar"   />
                </div>
                  <div class="form-group">
                  <input type="hidden" min="1" value="{{$iti2}}" type="number"  name="iti2" class="input-block-level" id="iti2"/>
                </div>

          <div class=" span12 text-center" style="padding-top: 1%">
                    <button class="btn btn-primary" type="submit"><b>Pagar</b></button>
                   
                  </div>
                  </form>
@endsection