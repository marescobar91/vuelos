@extends('layouts.template')
@section('content')
<div class="span12">
  @if($m == null)
<h1 style="align-content: left">Busca tu vuelo</h1>

<form action="{{route('itinerario.buscar')}}" method="POST" role="form" class="contactForm">
    {{csrf_field()}}
              <div class="row">
                <div class="span3">
                  <label style="padding-bottom: 20px"></label>
                    <select name="origen" class="form-group" required>
                       <option value="">Origen:</option> 
                       @foreach($ciudad as $destino)
                          <option value="{{$destino->id}}">{{$destino->nombre_ciudad}} </option>
                      @endforeach 
                       
                    </select>
                </div> 
                <div class="span3">
                  <label style="padding-bottom: 20px"></label>
                    <select name="destino" class="form-group" required>
                       <option value="">Destino:</option>
                        @foreach($ciudadDes as $origen)
                          <option value="{{$origen->id}}">{{$origen->nombre_ciudad}} </option>
                      @endforeach 
                        
                       

                    </select>
                </div>


                
                <div class="span3 form-group"> 
                  <label>Fecha de Salida</label>
                  <input required type="date" name="fecha_salida" class="input-block-level" id="fecha_salida" placeholder="Fecha Salida"  />
                </div>
                <div class="span3 form-group">
                  <label>Fecha de Regreso</label>
                  <input  type="date" name="fecha_llegada" class="input-block-level" id="fecha_salida" placeholder="Fecha de Regreso" required />
                </div>
              </div>
              <div class="span12">
                <div class="span1"></div>
                <div class="span3">
                    <select name="clase" class="form-group" required>
                       <option value="">Clase:</option>
                       
                        @foreach($clases as $clase)
                          <option value="{{$clase->id}}">{{$clase->nombre_clase}} </option>
                      @endforeach
                       
                    </select>
                </div>
                <div class="span2 form-group">
                  <input type="number" min="1" name="ninos" class="input-block-level" id="ninos" placeholder="Niños"  />
                </div>



                <div class="span2 form-group">
                  <input required min="1" type="number" name="adultos" class="input-block-level" id="adultos" placeholder="Adultos"  />
                </div>
                <div class="span2 form-group">
                  <input min="1" type="number" name="ancianos" class="input-block-level" id="ancianos" placeholder="Ancianos"  />
                </div>
              </div>
                <div class="span12 form-group">
                  <div class="text-center" style="padding-top: 1%">
                    <button class="btn btn-primary">Continuar</button>
                    <a href="{{route('itinerario.filtro')}}" class="btn btn-secondary"> <span>
                            <b>Cancelar</b></span></a>
                  </div>
                </div>
              </form>
@endif

<form action="{{route('itinerario.guardar')}}" method="POST" role="form" class="contactForm">
    {{csrf_field()}}
@if($m != null)
<div class="span2 form-group">
                  <input type="hidden" min="1" value="{{$suma}}" type="number"  name="suma" class="input-block-level" id="suma"   />
                </div>
<div class="span2 form-group">
                  <input type="hidden" min="1" value="{{$clase}}" type="number"  name="clase" class="input-block-level" id="clase"   />
                </div>

                <div class="form-group">
                  <!--if-->
                  <div style="padding-top: 1%">
                    <label class="span12 ">Listado de Vuelos de Ida</label>
                    <table class="span12 table table-striped">
                      <thead>
                     
                    </thead>
                    <tbody>
                      @foreach($vuelos1 as $vuelo)

                       <tr>
                        <th colspan="3" style="text-align: center">Vuelo de Ida</th>
                        <th><input value="{{$vuelo->id}}" required type="checkbox" name="ida"></th>
                      </tr>
                      <tr>
                        <td width="25%">Hora de Salida:</td>
                        <td width="25%">{{$vuelo->fecha_salida}}</td>
                        <td width="25%">Hora de llegada:</td>
                        <td width="25%">{{$vuelo->fecha_llegada}}</td>
                      </tr>
                      <tr>
                        <td >Duracion:</td>
                        <td>{{$vuelo->tiempo_vuelo}}</td>
                        <td>Clase:</td>
                        <td>{{$vuelo->nombre_clase}}</td>
                      </tr>
                      <tr>
                        <td>Aerolinea :</td>
                        <td>{{$vuelo->nombre_aerolinea}}</td>
                        <td>Precio:</td>
                        <td>{{$vuelo->precio}}</td>
                      </tr>
                      <tr>
                          <td style="padding-top: 3%;" colspan="4"></td>
                        </tr>
                    </tbody>
                      @endforeach   


                     
                    </table>
                  </div>

 @endif


 @if($m != null)

 <div class="span2 form-group">
                  <input type="hidden" min="1" value="{{$suma}}" type="number"  name="suma" class="input-block-level" id="suma"   />
                </div>
<div class="span2 form-group">
                  <input type="hidden" min="1" value="{{$clase}}" type="number"  name="clase" class="input-block-level" id="clase"   />
                </div>
                

                <div class="form-group">
                  <!--if-->
                  <div style="padding-top: 5%">
                    <label class="span12 ">Listado de Vuelos de Regreso</label>
                    <table class="span12 table table-striped">
                      <thead>
                      
                    </thead>
                    <tbody>
                      @foreach($vuelos2 as $vuelo)

                       <tr>
                        <th colspan="3" style="text-align: center">Vuelo Regreso</th>
                        <th><input value="{{$vuelo->id}}" required type="checkbox" name="regreso"></th>
                      </tr>

                      <tr>
                        <td width="25%">Hora de Salida:</td>
                        <td width="25%">{{$vuelo->fecha_salida}}</td>
                        <td width="25%">Hora de llegada:</td>
                        <td width="25%">{{$vuelo->fecha_llegada}}</td>
                      </tr>
                      <tr>
                        <td >Duracion:</td>
                        <td>{{$vuelo->tiempo_vuelo}}</td>
                        <td>Clase:</td>
                        <td>{{$vuelo->nombre_clase}}</td>
                      </tr>
                      <tr>
                        <td>Aerolinea :</td>
                        <td>{{$vuelo->nombre_aerolinea}}</td>
                        <td>Precio:</td>
                        <td>{{$vuelo->precio}}</td>
                      </tr>
                      <tr>
                          <td style="padding-top: 3%;" colspan="4"></td>
                        </tr>
                    </tbody>
                      @endforeach        
                    </table>
                  </div>

                   <div class="span12 form-group">
                  <div class="text-center" style="padding-top: 1%">
                    <button class="btn btn-primary" type="submit">Comprar</button>
                    <a href="{{route('itinerario.filtro')}}"" class="btn btn-secondary" style="float:right 5%;"> <span>
                            <b>Cancelar</b></span></a>
                  </div>
                </div>

  </form>  

 @endif


              
@endsection
