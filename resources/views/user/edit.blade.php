@extends('layouts.template')
@section('content')
<div class="span8" style="margin-left: 25%">
   <!-- Despliega mensajes de error -->
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                              <button type="button" class="close" data-dismiss="alert">
                                &times;</button>
                                <strong>¡ERROR!</strong> Hay errores en la información ingresada.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
@endif
<h1 class="span6">Editar Usuario</h1>
	<form action="{{ route('user.update', $user->id) }}" method="post" role="form" class="contactForm">
    {{csrf_field()}}
    {{ method_field('PUT') }}
              <div class="row">
                <div class="span6 form-group {{ $errors->has('nombre') ? ' has-error' : '' }}">
                  <input type="text" name="nombre" class="input-block-level" id="nombre" data-rule="minlen:4" data-msg="Ingrese al menos cuatro caracteres" value="{{ $user->nombre}}" required />
                  <div class="validation">
                    @if ($errors->has('nombre'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('nombre') }}</strong>
                                    </span>
                    @endif
                  </div>
                </div>
                <div class="span6 form-group {{ $errors->has('apellido') ? ' has-error' : '' }}">
                  <input type="text" class="input-block-level" name="apellido" id="apellido" data-rule="minlen:4" data-msg="Ingrese al menos 4 caracteres" value="{{ $user->apellido }}" required />
                  <div class="validation">
                    @if ($errors->has('apellido'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('apellido') }}</strong>
                                    </span>
                    @endif
                  </div>
                </div>
                <div class="span6 form-group {{ $errors->has('usuario') ? ' has-error' : '' }}">
                  <input type="text" name="usuario" class="input-block-level" id="usuario" value="{{ $user->name }}" data-rule="minlen:4" data-msg="Ingrese al menos cuatro caracteres" required />
                  <div class="validation">
                    @if ($errors->has('usuario'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('usuario') }}</strong>
                                    </span>
                    @endif
                  </div>
                </div>
            <div class="span8 form-group">
              <hr>
              <h3 class="span6">Lista de Roles</h3>
                </div>
              
          <div class="span9 form-group {{ $errors->has('roles') ? ' has-error' : '' }}" align="left">
            <ul>
              @foreach ($roles as $role)
            
                  <label>
                              <input type="checkbox" value="{{$role->id}}"
                                               {{in_array($role->id, $rol) ? "checked" : null}} name="roles[]">
                                          {{$role->name}}</strong><em>({{$role->description ?: 'Sin descripción'}})</em>
                            </label>    
                  
                  @endforeach

          <ul>
             <div class="validation">
                    @if ($errors->has('roles'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('roles') }}</strong>
                                    </span>
                    @endif
                  </div>
          </div>
                  <div class="span6 form-group">
                    <button class="btn btn-primary" type="submit">Guardar</button>
                     <a href="{{route('user.index')}}" class="btn btn-secondary" style="float:right 5%;"> <span>
                            <b>Cancelar</b></span></a>
                  </div>
              
              </div>
            </form>
        </div>
              
@endsection