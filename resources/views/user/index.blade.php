@extends('layouts.template')
@section('content')
 @extends('layouts.errores')
<div class="container" style="margin-top: 3%">
        <div class="col-md-9 col-md-offset-1">
            <div class="table-title" style=" margin-bottom: 1%">
                <div class="row">
                    <div class="col-sm-5" style="margin-left: 2%">

                        <h1 style="align-content: left">Gestion de Usuarios</h1>
                        @can('user.inactivo')
                        <a href="{{ route('user.inactivo')}}" class="btn btn-primary" style="float:right 5%;"> <span>
                            <b>Usuarios Inactivos</b></span></a>
                        @endcan
                     @can('roles.create')
                        <a href="{{ route('roles.create')}}" class="btn btn-primary" style="float:right 5%;"> <span>
                            <b>Agregar Roles</b></span></a>
                            @endcan
                         
                    </div>
                </div>
            </div>
            <table class="table table-striped table-hover" id="myTable" style="background: #fff;width: 100%">
                <thead>
                <tr>
                    <th>N°</td>
                    <th>Nombre Completo</th>
                    <th> Nombre de Usuario</th>
                    
                    <th width="23%">Acciones</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($user as $user)
                <tr>
                    <td>{{ $user->id }}</td>
                    <td>{{ $user->nombre}} {{ $user->apellido }}</td>
                    <td>{{ $user->name }}</td>
                   
                        <td>
                        @can('user.show')
                        <a href="{{ route('user.show', $user->id) }}" class="btn btn-primary"><span class="icon-eye-open"></span> </a>
                        @endcan
                        @can('user.edit')
                        <a href="{{ route('user.edit', $user->id) }}" class="btn btn-warning"><span class=" icon-pencil"></span> </a>
                        
                        @endcan
                        @if($user->id != $id)
                        @can('admin.password')
                        <a href="{{ route('admin.password', $user->id) }}"  class="btn btn-info"><span class="icon-key"></span></a>
                        @endcan
                        @can('user.destroy')
                        <a class="btn btn-danger" onclick="eliminar('{{ $user->id }}')"><span class="icon-trash"></span>
                    </a>
                    @endcan
                    @endif
                    
                      </td>
                </tr>
                @endforeach
                </tbody>
            </table>
<!--Modal-->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-body">
                    <p> ¿Desea Eliminar? </p>
                    <input type="hidden">
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <a class="btn btn-danger" id="aceptar">Sí</a>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
<script>
    
    function eliminar(id){
        var iddentified=id;
        
        $('#exampleModal').modal();
        document.getElementById("aceptar").addEventListener("click", function() {
        var url ='{{ route("user.destroy", ":id") }}';
        url=url.replace(":id",iddentified);
        window.location.href=url;
        }, false);

    }
    </script>
@endsection
