@extends('layouts.template')
@section('content')
<div class="span8" style="margin-left: 20%">
<!-- Despliega mensajes de error -->
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                              <button type="button" class="close" data-dismiss="alert">
                                &times;</button>
                                <strong>¡ERROR!</strong> Hay errores en la información ingresada.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
@endif
<h1 style="align-content: left">Cambiar Contraseña</h1>

	<form action="{{ route('admin.changepassword', $user->id) }}" method="post" role="form" class="contactForm">
    {{csrf_field()}}

    <div class="row">
            <div class="span8 form-group {{ $errors->has('contraseña') ? ' has-error' : '' }}">
            <input id="contraseña" type="password" class="form-control" name="contraseña" required placeholder="Contraseña">
                                <div class="validation">
                    @if ($errors->has('contraseña'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('contraseña') }}</strong>
                                    </span>
                    @endif
                  </div>
                        </div>
                        <div class="span8 form-group {{ $errors->has('confirmar_contraseña') ? ' has-error' : '' }}">
                            
                        <input id="confirmar_contraseña" type="password" class="form-control" name="confirmar_contraseña" required placeholder="Confirmar Contraseña">
                                <div class="validation">
                    @if ($errors->has('confirmar_contraseña'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('confirmar_contraseña') }}</strong>
                                    </span>
                    @endif
                  </div>
                          </div>
                     
                <div class="span8 form-group">
                  <div class="text-center">
                    <button class="btn btn-primary" type="submit">Cambiar Contraseña</button>
                    <a href="{{route('user.index')}}" class="btn btn-secondary" style="float:right 5%;"> <span>
                            <b>Cancelar</b></span></a>
                  </div>
                </div>
              </div>
            </form>
        </div>
              
@endsection