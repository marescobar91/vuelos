@extends('layouts.template')
@section('content')
<div class="span8" style="margin-left: 20%">
    <!-- Despliega mensajes de error -->
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                              <button type="button" class="close" data-dismiss="alert">
                                &times;</button>
                                <strong>¡ERROR!</strong> Hay errores en la información ingresada.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
@endif
<h1 style="align-content: left">Creacion de Permisos</h1>
	<form action="{{ route('permisos.store') }}" method="post" role="form" class="contactForm">
    {{csrf_field()}}
              <div class="row">
                <div class="span8 form-group {{ $errors->has('nombre') ? ' has-error' : '' }}">
                  <input type="text" name="nombre" class="input-block-level" id="nombre" placeholder="Nombre" data-rule="minlen:4" data-msg="Por favor, ingresa al menos cuatro letras" required />
                  <div class="validation">
                    @if ($errors->has('nombre'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('nombre') }}</strong>
                                    </span>
                    @endif
                  </div>
                </div>
                  
                <div class="span8 form-group {{ $errors->has('url') ? ' has-error' : '' }}">
                  <input type="text" name="url" class="input-block-level" id="url" placeholder="Url" data-rule="minlen:4" data-msg="Please enter at least 4 chars" required />
                  <div class="validation">
                    @if ($errors->has('url'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('url') }}</strong>
                                    </span>
                    @endif
                  </div>
                </div>
                <div class="span8 form-group {{ $errors->has('descripcion') ? ' has-error' : '' }}">
                  <input type="text" name="descripcion" class="input-block-level" id="descripcion" placeholder="Descripcion" data-rule="minlen:4" data-msg="Please enter at least 4 chars" required />
                   <div class="validation">
                    @if ($errors->has('descripcion'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('descripcion') }}</strong>
                                    </span>
                    @endif
                  </div>
                </div>
                
                <div class="span8 form-group">
                  <div class="text-center">
                    <button class="btn btn-primary" type="submit">Guardar</button>
                    <a href="{{route('permisos.index')}}" class="btn btn-secondary" style="float:right 5%;"> <span>
                            <b>Cancelar</b></span></a>
                  </div>
                </div>
              </div>
            </form>
        </div>
              
@endsection