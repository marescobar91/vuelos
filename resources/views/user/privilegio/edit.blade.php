@extends('layouts.template')
@section('content')
<div class="span8" style="margin-left: 20%">
    <!-- Despliega mensajes de error -->
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                              <button type="button" class="close" data-dismiss="alert">
                                &times;</button>
                                <strong>¡ERROR!</strong> Hay errores en la información ingresada.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
@endif
<h1 style="align-content: left">Editar Permisos</h1>
	<form class="form-horizontal" method="POST" action="{{route('permisos.update', $permisos->id )}}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}     
              <div class="row">
                <div class="span8 form-group {{ $errors->has('nombre') ? ' has-error' : '' }}">
                  <input type="text" name="nombre" class="input-block-level" id="nombre" value="{{ $permisos->name }}" data-rule="minlen:4" data-msg="Por favor, ingresa al menos cuatro letras" required />
                    <div class="validation">
                    @if ($errors->has('nombre'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('nombre') }}</strong>
                                    </span>
                    @endif
                  </div>
                </div>
                
                <div class="span8 form-group {{ $errors->has('url') ? ' has-error' : '' }}" style="padding-top: 2%"> 
                  <input type="text" name="url" class="input-block-level" id="url" value="{{ $permisos->slug}}" data-rule="minlen:4" data-msg="Please enter at least 4 chars" required />
                    <div class="validation">
                    @if ($errors->has('url'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('url') }}</strong>
                                    </span>
                    @endif
                  </div>
                </div>
                
                <div class="span8 form-group {{ $errors->has('descripcion') ? ' has-error' : '' }}" style="padding-top: 2%">
                  <input type="text" name="descripcion" class="input-block-level" id="descripcion" value="{{ $permisos->description }}" data-rule="minlen:4" data-msg="Please enter at least 4 chars" required />
                    <div class="validation">
                    @if ($errors->has('descripcion'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('descripcion') }}</strong>
                                    </span>
                    @endif
                  </div>
                </div>
                  
                
                <div class="span8 form-group" style="padding-top: 3%">
                  <div class="text-center">
                    <button class="btn btn-primary" type="submit">Guardar</button>
                    <a href="{{route('permisos.index')}}" class="btn btn-secondary" style="float:right 5%;"> <span>
                            <b>Cancelar</b></span></a>
                  </div>
                </div>
              </div>
            </form>
        </div>
              
@endsection