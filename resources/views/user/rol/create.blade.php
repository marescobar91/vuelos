@extends('layouts.template')
@section('content')
 @extends('layouts.errores')
<div class="span8" style="margin-left: 20%">
 <!-- Despliega mensajes de error -->
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                              <button type="button" class="close" data-dismiss="alert">
                                &times;</button>
                                <strong>¡ERROR!</strong> Hay errores en la información ingresada.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
@endif

<h1 style="align-content: left">Crear Roles</h1>
	<form action="{{ route('roles.store') }}" method="post" role="form" class="contactForm">
    {{csrf_field()}}
              <div class="row">
                <div class="span8 form-group {{ $errors->has('nombre') ? ' has-error' : '' }}">
                  <input type="text" name="nombre" class="input-block-level" id="nombre" placeholder="Nombre" data-rule="minlen:4" data-msg="Please enter at least 4 chars" required />
                     <div class="validation">
                    @if ($errors->has('nombre'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('nombre') }}</strong>
                                    </span>
                    @endif
                  </div>
                </div>
                <div class="span8 form-group {{ $errors->has('url') ? ' has-error' : '' }}">
                  <input type="text" name="url" class="input-block-level" id="url" placeholder="Url" data-rule="minlen:4" data-msg="Please enter at least 4 chars" required />
                    <div class="validation">
                    @if ($errors->has('url'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('url') }}</strong>
                                    </span>
                    @endif
                  </div>
                </div>
                <div class="span8 form-group">
                                    <textarea rows="4" cols="10" name="descripcion" id="descripcion" class="form-control" required></textarea>
                  </div>

                  <div align="left">
                    <h3>Permiso Especial</h3>
                  </div>
                   <div class="validation" align="left"></div>


                  <div class="span9 form-group ">
                    
                    <select class="span4 form-group" name="permiso_especial">
                      <option value="">Seleccionar Permiso Especial</option>
                    <option value="all-access">Acceso Total</option>
                    <option value="no-access">No acceso</option>
                    </select>
                    
                  </div>


                <div class="form-group" align="left">
                <h3>Asignar Privilegios</h3>
                </div>
                <div style="overflow: scroll; height: 300px">
                  <div class="span9 form-group {{ $errors->has('permisos') ? ' has-error' : '' }}" align="left">
                    <ul>
                      @foreach ($permisos as $permisos)
                    
                          <label>
                            <input  type="checkbox" value="{{$permisos->id }}" name="permisos[]">
                                         <strong>{{$permisos->name}}</strong><em>({{$permisos->description ?: 'Sin descripción'}})</em>
                            </label>
                          
                          @endforeach

                  </ul>
                  <div class="validation">
                    @if ($errors->has('permisos'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('permisos') }}</strong>
                                    </span>
                    @endif
                  </div>
                  </div>
                </div>

                  <div class="text-center span12" style="padding-top: 5%">
                    <button class="btn btn-primary" type="submit">Guardar</button>
                     <a href="{{route('roles.index')}}" class="btn btn-secondary" style="float:right 5%;"> <span>
                            <b>Cancelar</b></span></a>
                  </div>
                </div>
              </div>
            </form>
        </div>
              
@endsection