@extends('layouts.template')
@section('content')
<div class="span8" style="margin-left: 20%">
 <!-- Despliega mensajes de error -->
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                              <button type="button" class="close" data-dismiss="alert">
                                &times;</button>
                                <strong>¡ERROR!</strong> Hay errores en la información ingresada.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
@endif

<h1 style="align-content: left">Editar Roles</h1>
  <form class="form-horizontal" method="POST" action="{{route('roles.update', $roles->id )}}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
              <div class="row">
                <div class="span8 form-group {{ $errors->has('nombre') ? ' has-error' : '' }}">
                  <input type="text" name="nombre" class="input-block-level" id="nombre" data-rule="minlen:4" value="{{ $roles->name}}" required />
                    <div class="validation">
                    @if ($errors->has('nombre'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('nombre') }}</strong>
                                    </span>
                    @endif
                  </div>
                </div>

                <div class="span8 form-group {{ $errors->has('url') ? ' has-error' : '' }}" style="padding-top: 2%">
                  <input type="text" name="url" class="input-block-level" id="url"  data-rule="minlen:4" value="{{ $roles->slug}}" required />
                  <div class="validation">
                    @if ($errors->has('url'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('url') }}</strong>
                                    </span>
                    @endif
                  </div>
                </div>
                <div class="span8 form-group {{ $errors->has('descripcion') ? ' has-error' : '' }}" style="padding-top: 2%">
                                    <textarea rows="4" cols="5" name="descripcion" id="descripcion" class="form-control" required>{{ $roles->description }}</textarea>
                                     <div class="validation">
                    @if ($errors->has('descripcion'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('descripcion') }}</strong>
                                    </span>
                    @endif
                  </div>
                  </div>
                  <div align="left">
                    <h3>Permiso Especial</h3>
                  </div>
                   <div class="validation" align="left"></div>

                  <br/>
                  <div class="span8 form-group">
                    
                      <select name="permiso_especial" class="span4 form-group">
                        <option @if(old('permiso_especial', $roles->special)== null) selected @endif>Selecciona Permiso Especial</option>
                        <option @if(old('permiso_especial', $roles->special)=='all-access') selected @endif>Acceso Total</option>
                        <option @if(old('permiso_especial', $roles->special)=='no-access') selected @endif>No acesso</option>
                      </select>
                  </div>
                   <div class="validation"></div>



                 <div class="form-group" align="left" style="padding-top: 7%">
                  <h3>Asignar Privilegios</h3>
                </div>

                <div style="overflow: scroll; height: 300px;" >
                  <div class="span9 form-group {{ $errors->has('permissions') ? ' has-error' : '' }}" align="left">
                <ul>
                                        @foreach($permisos as $permisos)
                                        <label>
                              <input type="checkbox" value="{{$permisos->id}}"
                                               {{in_array($permisos->id, $permission) ? "checked" : null}} name="permissions[]">
                                          {{$permisos->name}}</strong><em>({{$permisos->description ?: 'Sin descripción'}})</em>
                            </label>    
                                @endforeach
                                      </ul>
                                    </div>
                                    <div class="validation">
                    @if ($errors->has('permissions'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('permissions') }}</strong>
                                    </span>
                    @endif
                  </div>

                  <div class="validation"></div>
                  <div class="text-center">
                    <button class="btn btn-primary" type="submit">Guardar</button>
                     <a href="{{route('roles.index')}}" class="btn btn-secondary" style="float:right 5%;"> <span>
                            <b>Cancelar</b></span></a>
                  </div>
                </div>
              </div>
            </form>
        </div>
              
@endsection