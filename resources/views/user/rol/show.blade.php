@extends('layouts.template')
@section('content')
<div class="span8">
<h1 style="align-content: left">Información del Rol</h1>
	
              <div class="row">
                <div class="panel-heading">Información del Rol {{ $role->name }}</div>

                    <div class="panel-body">
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Description</label>
                            <p>{{ $role->description}}</p>
                        </div>
                        <div class="span6 form-group">
                            <label for="name" class="col-md-4 control-label">Permisos</label>
                            @if(!empty($permission))
                                <div class="col-md-8 control-label">
                                
                                  @foreach($permission as $permission)
                              
                                       <p class="label label-info" style="float: left; margin-right: 5px">{{ $permission->name }}</p>

                                  @endforeach

                                </div>
                            @endif
                        </div>
</div>
              </div>
            </form>
        </div>
              
@endsection