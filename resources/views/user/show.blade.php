@extends('layouts.template')
@section('content')
<div class="span8">
<h1 style="align-content: left">Detalle de Usuario</h1>
	<form action="" method="post" role="form" class="contactForm">
                <div class="row span12">
                </div>
                <div class="span12 form-group">
                 <p style="float: left; padding-left: 5%">Nombre Completo</p>
                  <div class="validation"></div>
                  <p style="float: left; padding-left: 5%" ><strong>{{ $user->nombre }} {{ $user->apellido }}</strong></p>
                </div>
                <div class="span12 form-group">
                 <p style="float: left; padding-left: 5%">Nombre de Usuario</p>
                  <div class="validation"></div>
                 <p style="float: left; padding-left: 5%" ><strong>{{ $user->name }} </strong></p>
                </div>
                <div class="span12 form-group">
                  <p style="float: left; padding-left: 5%">Correo Electrónico</p>
                  <div class="validation"></div>
                   <p style="float: left; padding-left: 5%" ><strong>{{ $user->email }} </strong></p>
                </div>
               <div class="span8 form-group">
                            <label for="name" class="span2 control-label">Permisos</label>
                            @if(!empty($roles))
                                <div class="col-md-8 control-label">
                                
                                  @foreach($roles as $role)
                              
                                       <p class="label label-info" style="float: left; margin-right: 5px">{{ $role->name }}</p>

                                  @endforeach

                                </div>
                            @endif
                        </div>
                </div>
              
            </form>
        </div>
              
@endsection