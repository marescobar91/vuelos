@extends('layouts.template')
@section('content')
<div class="span12">
<!-- Despliega mensajes de error -->
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                              <button type="button" class="close" data-dismiss="alert">
                                &times;</button>
                                <strong>¡ERROR!</strong> Hay errores en la información ingresada.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
@endif
<h1>Crear Vuelo</h1>
  <form action="{{route('vuelos.store')}}" method="post" role="form" class="contactForm">
    {{csrf_field()}}
            <div class="span12">
                    <label class="span3 "style="float: left"> Detalles de Ida:</label>
                <div class="span4 input-append date form_datetime {{ $errors->has('fecha_salida') ? ' has-error' : '' }}">
                  <input size="16" type="text" value="" readonly name="fecha_salida">
                  <span class="add-on"><i class="icon-remove"></i></span>
                  <span class="add-on"><i class="icon-th"></i></span>
                  <div class="validation">
                    @if ($errors->has('fecha_salida'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('fecha_salida') }}</strong>
                                    </span>
                    @endif
                  </div>
              </div>
                <div class="span2 form-group {{ $errors->has('origen') ? ' has-error' : '' }}">
                  <select name="origen" id="origen" class="form-group" required>
                    <option value="">Seleccione Ciudad Origen</option>
                  @foreach($ciudades as $ciudad)
                  <option value="{{$ciudad->id}}">{{$ciudad->nombre_ciudad}}
                  </option>
                  @endforeach
                </select>
                <div class="validation">
                    @if ($errors->has('origen'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('origen') }}</strong>
                                    </span>
                    @endif
                  </div>
                </div>
            </div>
            <div class="span12">
                <label style="float: left" class="span3">Detalles de Regreso:</label>
                <div class="span4 input-append date form_datetime {{ $errors->has('fecha_llegada') ? ' has-error' : '' }}">
                  <input size="16" type="text" value="" readonly name="fecha_llegada">
                  <span class="add-on"><i class="icon-remove"></i></span>
                  <span class="add-on"><i class="icon-th"></i></span>

                  <div class="validation">
                    @if ($errors->has('fecha_llegada'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('fecha_llegada') }}</strong>
                                    </span>
                    @endif
                  </div>
              </div>
                <div class="span2 form-group {{ $errors->has('destino') ? ' has-error' : '' }}">
                  <select name="destino" id="destino" class="form-group" required>
                    <option value="">Seleccione Ciudad Destino</option>
                  @foreach($ciudades as $ciudades)
                  <option value="{{$ciudades->id}}">{{$ciudades->nombre_ciudad}}
                  </option>
                  @endforeach
                </select>

                <div class="validation">
                    @if ($errors->has('destino'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('destino') }}</strong>
                                    </span>
                    @endif
                  </div>
                </div>
            </div>
            <div class="span12" style="margin-left: 7%">
                <div class="span3 form-group {{ $errors->has('aerolinea') ? ' has-error' : '' }}">
                  <select name="aerolinea" id="aerolinea" class="form-group" required>
                    <option value="">Seleccione Aerolinea</option>
                  @foreach($aerolinea as $aerolinea)
                  <option value="{{$aerolinea->id}}">{{$aerolinea->nombre_aerolinea}}
                  </option>
                  @endforeach
                </select>
                <div class="validation">
                    @if ($errors->has('aerolinea'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('aerolinea') }}</strong>
                                    </span>
                    @endif
                  </div>
                </div>

                <div class="span3 form-group  {{ $errors->has('gateway') ? ' has-error' : '' }}">
                  <select name="gateway" id="gateway" class="form-group" required>
                    <option value="">Seleccione Gateway</option>
                  @foreach($gateway as $gateway)
                  <option value="{{$gateway->id}}">{{$gateway->nombre_gateway}}
                  </option>
                  @endforeach
                </select>
                <div class="validation">
                    @if ($errors->has('gateway'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('gateway') }}</strong>
                                    </span>
                    @endif
                  </div>
                </div>
               <div class="span3 form-group {{ $errors->has('aviones') ? ' has-error' : '' }}">
                  <select name="aviones" id="aviones" class="form-group" required>
                    <option value="">Seleccione el Avion</option>
                  @foreach($aviones as $aviones)
                  <option value="{{$aviones->id}}">{{$aviones->tipo_avion}}
                  </option>
                  @endforeach
                </select>
                  <div class="validation">
                    @if ($errors->has('aviones'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('aviones') }}</strong>
                                    </span>
                    @endif
                  </div>
                </div>
              </div>
                <div class="span7" style="margin-left: 20%;padding-top: 2%">
                <div class="span3 form-group {{ $errors->has('distancia_recorrida') ? ' has-error' : '' }}">
                  <input required type="number" name="distancia_recorrida" class="input-block-level" id="distancia_recorrida" placeholder="Kilometros" required />
                  <div class="validation">
                    @if ($errors->has('distancia_recorrida'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('distancia_recorrida') }}</strong>
                                    </span>
                    @endif
                  </div>
                </div>
            
                <div class="span3 form-group {{ $errors->has('costos') ? ' has-error' : '' }}">
                  <input required type="number" name="costos" class="input-block-level" id="costos" placeholder="Costos" required />
                  <div class="validation">
                    @if ($errors->has('costos'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('costos') }}</strong>
                                    </span>
                    @endif
                </div>
              </div>
            </div>
            </div>  

                 <div class="span12 form-group">
                  <div class="text-center" style="padding-top: 1%">
                    <button class="btn btn-primary" type="submit">Guardar</button>
                    <a href="{{route('vuelos.index')}}" class="btn btn-secondary" style="float:right 5%;"> <span>
                            <b>Cancelar</b></span></a>
                  </div>
                </div>
              </div>
            </form>
        </div>
      
@endsection
@section('js')
<script type="text/javascript">
    $(".form_datetime").datetimepicker({format:'yyyy-mm-dd hh:ii'
    });
</script>
@endsection