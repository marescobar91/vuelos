@extends('layouts.template')
@section('content')
<div class="span12">
  <!-- Despliega mensajes de error -->
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                              <button type="button" class="close" data-dismiss="alert">
                                &times;</button>
                                <strong>¡ERROR!</strong> Hay errores en la información ingresada.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
@endif
<h1>Editar Vuelo</h1>
  <form action="{{route('vuelos.update', $vuelos->id)}}" method="post" role="form" class="contactForm">
    {{csrf_field()}}
    {{ method_field('PUT') }}
            <div class="span12">
                    <label class="span3 "style="float: left"> Detalles de Ida:</label>
                <div class="span4 input-append date form_datetime {{ $errors->has('fecha_salida') ? ' has-error' : '' }}">
                  <input size="16" type="text" value="{{ $vuelos->fecha_salida }}" readonly name="fecha_salida">
                  <span class="add-on"><i class="icon-remove"></i></span>
                  <span class="add-on"><i class="icon-th"></i></span>

                   <div class="validation">
                    @if ($errors->has('fecha_salida'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('fecha_salida') }}</strong>
                                    </span>
                    @endif
                  </div>
              </div>
                <div class="span2 form-group {{ $errors->has('origen') ? ' has-error' : '' }}">
                  <select name="origen" id="origen" class="form-group" required>
                    <option value="">Seleccione Ciudad Origen</option>
                     @foreach($origen as $origen)
                  <option value="{{ $origen->id }}" @if($origen->id==$vuelos->origen_id) selected='selected' @endif> {{ $origen->nombre_ciudad }}
                  </option>
                  @endforeach
                </select>
                <div class="validation">
                    @if ($errors->has('origen'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('origen') }}</strong>
                                    </span>
                    @endif
                  </div>
                </div>
            </div>
            <div class="span12">
                <label style="float: left" class="span3">Detalles de Regreso:</label>
                <div class="span4 input-append date form_datetime {{ $errors->has('fecha_llegada') ? ' has-error' : '' }}">
                  <input size="16" type="text" value="{{ $vuelos->fecha_llegada }}" readonly name="fecha_llegada">
                  <span class="add-on"><i class="icon-remove"></i></span>
                  <span class="add-on"><i class="icon-th"></i></span>
                   <div class="validation">
                    @if ($errors->has('fecha_llegada'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('fecha_llegada') }}</strong>
                                    </span>
                    @endif
                  </div>
              </div>
                <div class="span2 form-group {{ $errors->has('destino') ? ' has-error' : '' }}">
                  <select name="destino" id="destino" class="form-group" required>
                    <option value="">Seleccione Ciudad Destino</option>
              @foreach($destino as $destino)
                  <option value="{{ $destino->id }}" @if($destino->id==$vuelos->destino_id) selected='selected' @endif>{{ $destino->nombre_ciudad }}
                  </option>
                 @endforeach
                </select>
                 <div class="validation">
                    @if ($errors->has('destino'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('destino') }}</strong>
                                    </span>
                    @endif
                  </div>
                </div>
            </div>

            <div class="span7" style="margin-left: 22%">
                <div class="span3 form-group {{ $errors->has('aerolinea') ? ' has-error' : '' }}">
                  <select name="aerolinea" id="aerolinea" class="form-group" required>
                    <option value="">Seleccione Aerolinea</option>
                    @foreach($aerolinea as $aerolinea)
                  <option value="{{ $aerolinea->id }}" @if($aerolinea->id==$vuelos->aerolinea_id) selected='selected' @endif>{{ $aerolinea->nombre_aerolinea }}
                  </option>
                  @endforeach
                </select>
                                 <div class="validation">
                    @if ($errors->has('aerolinea'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('aerolinea') }}</strong>
                                    </span>
                    @endif
                  </div>
                </div>
                <div class="span3 form-group {{ $errors->has('gateway') ? ' has-error' : '' }}">
                  <select name="gateway" id="gateway" class="form-group" required>
                    <option value="">Seleccione el Gateway</option>
                    @foreach($gateway as $gateway)
                  <option value="{{ $gateway->id }}" @if($gateway->id==$vuelos->gateway_id) selected='selected' @endif>{{ $gateway->nombre_gateway }}
                  </option>
                @endforeach
                </select>
                <div class="validation">
                    @if ($errors->has('gateway'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('gateway') }}</strong>
                                    </span>
                    @endif
                  </div>
                </div>
              </div>
                <div class="span7" style="margin-left: 22%">
                <div class="span3 form-group {{ $errors->has('distancia_recorrida') ? ' has-error' : '' }}">
                  <label>Kilometros:</label>
                  <input required type="number" name="distancia_recorrida" class="input-block-level" value="{{ $vuelos->kilometros_vuelo  }}" id="distancia_recorrida" placeholder="Kilometros" required />
                  <div class="validation">
                    @if ($errors->has('distancia_recorrida'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('distancia_recorrida') }}</strong>
                                    </span>
                    @endif
                  </div>
                </div>
            
            <div class="span3 form-group {{ $errors->has('costos') ? ' has-error' : '' }}">
              <label>Costo:</label>
                  <input required type="number" name="costos" class="input-block-level" id="costos" value="{{ $vuelos->costo }}" placeholder="Costos" required />
                </div>
                <div class="validation">
                    @if ($errors->has('costos'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('costos') }}</strong>
                                    </span>
                    @endif
                  </div>
              </div>
            </div>
          </div>
          <div class="span9" style="margin-left: 16%">
            @foreach($vuelos->clases as $clase)
            <label class="span8">La clase de {{ $clase->nombre_clase }}</label>
              <div class="span4 form-group {{ $errors->has('precio') ? ' has-error' : '' }}">
                <label>Precio:</label>
                  <input type="hidden" value="{{ $clase->id }}" name="clase[]" class="input-block-level" id="clase[]" />
                  <input type="number" name="precio[]" class="input-block-level" id="precio[]" placeholder="Precio" required value="{{ $clase->pivot->precio  }}" />
                   <div class="validation">
                    @if ($errors->has('precio'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('precio') }}</strong>
                                    </span>
                    @endif
                  </div>
                </div>
          
                <div class="span4 form-group">
                  <label>Promoción</label>
                  <input type="number" name="promocion[]" class="input-block-level" id="promocion[]" placeholder="Promocion" value="{{ $clase->pivot->promocion }}" />
                  <div class="validation"></div>
                </div>
                @endforeach
              </div>
              <div class="span12 form-group">
                  <div class="text-center" style="padding-top: 1%">
                    <button class="btn btn-primary" type="submit">Guardar</button>
                    <a href="{{route('vuelos.index')}}" class="btn btn-secondary" style="float:right 5%;"> <span>
                            <b>Cancelar</b></span></a>
                  </div>
                </div>
              </div>
            </form>
        </div>
        
@section('js')
<script>
    function available(value){
        if(value=='1'){
            document.getElementById("forma").disabled=true;
        }
        else{
            document.getElementById("forma").disabled=false;
        }
    }
</script>
<script type="text/javascript">
    $(".form_datetime").datetimepicker({format:'yyyy-mm-dd hh:ii'
    });
</script>
@endsection
@endsection