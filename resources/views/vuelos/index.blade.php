@extends('layouts.template')
@section('content')
<div class="container" style="margin-top: 3%">
        <div class="col-md-9 col-md-offset-0">
            <div class="table-title" style=" margin-bottom: 1%">
                <div class="row">
                    <div class="col-sm-5" style="margin-left: 2%">
                        <h1 style="align-content: left">Gestion de Vuelos</h1>
                        <a href="{{route('vuelos.create')}}" class="btn btn-primary" style="float:right 5%;"> <span><b>Agregar Vuelo</b></span></a>
                    </div>
                </div>
            </div>
            <table class="table table-striped table-hover" id="myTable" style="background: #fff;width:100%">
                <thead>
                <tr>
                    <th>Codigo de Vuelo</th>
                    <th>Lugar de Ida</th>
                    <th>Fecha de Ida</th>
                    <th>Lugar de Regreso</th>
                    <th>Fecha de Regreso</th>
                    <th>Aerolinea</th>
                    <th>Gateway</th>
                    <th width="11%">Acciones</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($vuelo as $vuelo)
                <tr>
                   <td>{{ $vuelo->codigo_vuelo}}</td> 
                    <td>{{ $vuelo->ciudadOrigen->nombre_ciudad }}</td>  
                    <td>{{ $vuelo->fecha_salida }}</td>
                    <td>{{ $vuelo->ciudadDestino->nombre_ciudad }}</td>
                    <td>{{ $vuelo->fecha_llegada }}</td> 
                    <td>{{ $vuelo->aerolineas->nombre_corto}}</td>  
                    <td>{{ $vuelo->getaways->nombre_gateway }}</td>
                        <td>
                        <a href="{{ route('vuelos.edit', $vuelo->id) }}" class="btn btn-warning"><span class=" icon-pencil"></span> </a>
                        <a class="btn btn-danger" onclick="eliminar('{{ $vuelo->id }}')"><span class="icon-trash" ></span></a>
                      </td>
                </tr>
                @endforeach
                
                </tbody>
            </table>
            <!--Modal-->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-body">
                    <p> ¿Desea Eliminar? </p>
                    <input type="hidden">
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <a class="btn btn-danger" id="aceptar">Sí</a>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
<script>
    
    function eliminar(id){
        var iddentified=id;
        
        $('#exampleModal').modal();
        document.getElementById("aceptar").addEventListener("click", function() {
        var url ='{{ route("vuelos.destroy", ":id") }}'; //agregar ruta del destroy de vuelos
        url=url.replace(":id",iddentified);
        window.location.href=url;
        }, false);

    }
    </script>
@endsection
