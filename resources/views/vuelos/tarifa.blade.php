@extends('layouts.template')
@section('content')
<style type="text/css">
  .btn-dolars{
    background-color: #5ccd72;
  }
</style>
<div class="container" style="margin-top: 3%">
  @if($vacio==null)
        <div class="col-md-9 col-md-offset-0">
          <h3>Ultimo Vuelo</h3>
            <table class="table table-striped table-hover" id="vuelo" style="background: #fff;width:100%">
          <div class="table-title" style=" margin-bottom: 1%">
                <div class="row">
                </div>
            </div>
                <thead>
                <tr>
                    <th>Codigo de Vuelo</th>
                    <th>Lugar de Ida</th>
                    <th>Fecha de Ida</th>
                    <th>Lugar de Regreso</th>
                    <th>Fecha de Regreso</th>
                    <th>Aerolinea</th>
                    <th>Gateway</th>
                    <th width="11%">Acciones</th>
                </tr>
                </thead>
                <tbody>
                  
                <tr>
                    <td>{{ $vuelo->codigo_vuelo}}</td> 
                    <td>{{ $vuelo->ciudadOrigen->nombre_ciudad }}</td>  
                    <td>{{ $vuelo->fecha_salida }}</td>
                    <td>{{ $vuelo->ciudadDestino->nombre_ciudad }}</td>
                    <td>{{ $vuelo->fecha_llegada }}</td> 
                    <td>{{ $vuelo->aerolineas->nombre_corto}}</td>  
                    <td>{{ $vuelo->getaways->nombre_gateway }}</td>
                    <td>
                        <a href="{{ route('tarifas.create') }}" class="btn btn-dolars" style="background-color: #5ccd72"><b>$</b> </a>
                      </td>
                </tr>
           
                </tbody>
            </table>
        </div>
    </div>
@endif

@if($vacio !=null)
<div class="container" style="margin-top: 3%">
   <!-- Despliega mensajes de error -->
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                              <button type="button" class="close" data-dismiss="alert">
                                &times;</button>
                                <strong>¡ERROR!</strong> Hay errores en la información ingresada.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
@endif
<h1>Agregar Tarifa</h1>

<form action="{{ route('tarifas.store') }}" method="post" role="form" class="contactForm">
    {{csrf_field()}}
  <div class="span7" style="padding-left: 23%">
              <div>
                <h4>Asignar tarifas al vuelo {{ $vuelo->codigo_vuelo }} 
                  que sale de {{ $vuelo->ciudadOrigen->nombre_ciudad }}
                a {{ $vuelo->ciudadDestino->nombre_ciudad }}</h4>
                 </div>
                  @foreach($clases as $clase)
                  <label class="span6">{{ $clase->nombre_clase}}</label>
                <div class="span3 form-group {{ $errors->has('precio') ? ' has-error' : '' }}">
                  
                  <input type="hidden" value="{{ $clase->id }}" name="clase[]" class="input-block-level" id="clase[]" />
                  <input type="number" name="precio[]" class="input-block-level" id="precio[]" placeholder="Precio" required />
                   <div class="validation">
                    @if ($errors->has(''))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('precio') }}</strong>
                                    </span>
                    @endif
                  </div>

                </div>
          
                <div class="span3 form-group">
                  <input type="number" name="promocion[]" class="input-block-level" id="promocion[]" placeholder="Promocion" />
                  <div class="validation"></div>
                </div>
              @endforeach
           
              </div>
                <div class="span12 form-group">
                  <div class="text-center">
                    <button class="btn btn-primary" type="submit">Guardar</button>
                  <a href="{{ route('vuelos.tarifas') }}" class="btn btn-secondary"> <span>
                            <b>Cancelar</b></span></a>
                  </div>
                </div>             
            </form>
        </div>
@endif
@endsection