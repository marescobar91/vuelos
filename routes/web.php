<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//sitios publicos
Route::get('/', function () {
    return view('index');   //view('index');

})->name('index'); 

Route::get('/top10', function() {
	return view('information.top10');
})->name('top10');

Route::get('/compra', function() {
	return view('reservacion.view');
})->name('preview.buy');

Route::get('/contacto/', function(){
	return view('information.contact'); 
})->name('show.contacto');


Route::get('itinerario/add','ItinerariosController@consulta')->name('itinerario.filtro');
Route::post('itinerario/buscar','ItinerariosController@busqueda')->name('itinerario.buscar');
Route::post('itinerario/captura','ItinerariosController@guardar')->name('itinerario.guardar');
Route::post('itinerario/guardar/cliente','ItinerariosController@cliente')->name('itinerario.cliente');
Route::post('asientos/reserva', 'ItinerariosController@asiento')->name('itinerario.asientos');
Route::post('tarifas/nuevo', 'ItinerariosController@mostrar')->name('itinerario.tarifas');

Route::post('/enviar', [
	'uses'	=> 	'MailController@store',
	'as'	=>	'mail.store'
]);
 
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

   //ayuda :(

Route::middleware(['auth'])->group(function(){


//Aqui iran todas las rutas una vez que haya finalizado todo el proceso de creación del menu de usuarios, para ver el tipo de acceso. 
	//ADMINISTRADOR DE APLICACION
Route::resource('user', 'UsersController');
Route::get('user/{id}/destroy', [
		'uses'	=>	'UsersController@destroy',
		'as'	=>	'user.destroy'
	]);


Route::resource('roles', 'RolesController');
Route::get('roles/{id}/destroy', [
		'uses'	=>	'RolesController@destroy',
		'as'	=>	'roles.destroy'
	]);
Route::resource('permisos', 'PermissionsController');
Route::get('permisos/{id}/destroy', [
		'uses'	=>	'PermissionsController@destroy',
		'as'	=>	'permisos.destroy'
	]);

route::resource('paises','PaisesController');
Route::get('paises/{id}/destroy', [
		'uses'	=>	'PaisesController@destroy',
		'as'	=>	'paises.destroy'
	]);
route::resource('departamentos','DepartamentosController');
Route::get('departamentos/{id}/destroy', [
		'uses'	=>	'DepartamentosController@destroy',
		'as'	=>	'departamentos.destroy'
	]);


route::resource('municipios','MunicipiosController');
Route::get('municipios/{id}/destroy', [
		'uses'	=>	'MunicipiosController@destroy',
		'as'	=>	'municipios.destroy'
	]);


route::resource('ciudades','CiudadesController');
Route::get('ciudades/{id}/destroy', [
		'uses'	=>	'CiudadesController@destroy',
		'as'	=>	'ciudades.destroy'
	]);

//RUTAS de Administrador de aeropuertos
route::resource('aerolineas','AerolineasController');
Route::get('aerolineas/{id}/destroy', [
		'uses'	=>	'AerolineasController@destroy',
		'as'	=>	'aerolineas.destroy'
	]);
Route::resource('aeropuertos', 'AeropuertosController');
Route::get('aeropuertos/{id}/destroy', [
		'uses'	=>	'AeropuertosController@destroy',
		'as'	=>	'aeropuertos.destroy'	
	]);

Route::resource('zonas', 'ZonasController');
Route::get('zonas/{id}/destroy', [
		'uses'	=>	'ZonasController@destroy',
		'as'	=>	'zonas.destroy'	
	]);



//USUARIOS INACTIVO
//Administrador de la Aplicacion

//muesta los usuarios eliminados
Route::get('/deshabilitado', [
		'uses'	=>	'UsersController@inactivo',
		'as'	=>	'user.inactivo'
	]);

Route::get('{id}/habilitar',[
		'uses'	=> 'UsersController@habilitar',
		'as'	=>	'user.habilitar'
	]);
//Cambio de contraseña por parte del administrador
Route::get('/{id}/password', [
		'uses'	=>	'UsersController@password',
		'as'	=>	'admin.password']);
Route::post('/{id}/change', [
        'uses'	=>	'UsersController@changepassword',
        'as'	=>	'admin.changepassword']);

//Cambiar de contraseña por parte del usuario
Route::get('/changepassword', [
		'uses'	=> 'UsersController@passwordchange',
		'as' 	=>	'user.password'
	]);
//guarda el cambio de contraseña
Route::post('/updatepassword', [
		'uses'	=>	'UsersController@updatepassword',
		'as'	=>	'user.updatepassword'
	]);	
 Route::resource('aviones', 'AvionesController');
 Route::get('aviones/{id}/destroy', [
		'uses'	=>	'AvionesController@destroy',
		'as'	=>	'aviones.destroy'	
	]);

 Route::resource('marcas', 'MarcasController');
 Route::get('marcas/{id}/destroy', [
		'uses'	=>	'MarcasController@destroy',
		'as'	=>	'marcas.destroy'	
	]);

 Route::resource('modelos', 'ModelosController');
 Route::get('modelos/{id}/destroy', [
		'uses'	=>	'ModelosController@destroy',
		'as'	=>	'modelos.destroy'	
	]);

 Route::resource('clases', 'ClasesController');
 Route::get('clases/{id}/destroy', [
		'uses'	=>	'ClasesController@destroy',
		'as'	=>	'clases.destroy'	
	]);

//vuelos
Route::resource('vuelos','VuelosController');
Route::get('vuelo/{id}/destroy', [
		'uses'	=>	'VuelosController@destroy',
		'as'	=>	'vuelos.destroy'
	]);
Route::get('/vuelos/create/tarifas',[
		'uses'	=>	'VuelosController@tarifas',
		'as'	=>	'vuelos.tarifas'
	]);
Route::get('/tarifas/create', [
		'uses'	=>	'VuelosController@asignacion',
		'as'	=>	'tarifas.create'
	]);
Route::post('/tarifas', [
		'uses'	=>	'VuelosController@guardar',
		'as'	=>	'tarifas.store'
	]);
});


 

